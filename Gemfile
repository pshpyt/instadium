source 'https://rubygems.org'
ruby '2.0.0'
gem 'rails', '4.1.1'
gem 'sass-rails', '~> 4.0.2'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.0.0'
gem 'jquery-rails'
gem 'turbolinks'
gem 'jbuilder', '~> 1.2'
gem 'bootstrap-sass'
gem 'cancan'
gem 'devise'
gem 'omniauth'
gem 'squeel', github: 'kiela/squeel'

# gem 'squeel', '~> 1.1'
# gem "polyamorous", :github => "activerecord-hackery/polyamorous"

gem 'omniauth-facebook'
gem 'omniauth-google-oauth2'
gem 'omniauth-twitter'
gem 'omniauth-instagram'
gem 'acts_as_follower', '~> 0.2'

gem 'will_paginate'
gem 'bootstrap-will_paginate'
gem 'bootstrap-typeahead-rails'

gem 'intercom-rails', '~> 0.2.21'
gem 'newrelic_rpm'

gem 'pg'
gem 'rolify'
gem 'simple_form'
gem 'slim-rails'

gem 'compass'
gem 'compass-rails'
gem 'nested-hstore'

gem 'grape', '~> 0.7'
gem 'grape-entity'
gem 'grape-swagger'
gem 'grape-swagger-ui'
gem 'tilt', '1.4.1'

gem 'nested_form', :git => "git://github.com/ryanb/nested_form.git"
group :development do
  gem 'better_errors'
  gem 'binding_of_caller', :platforms=>[:mri_20]
  gem 'quiet_assets'
  gem 'rails_layout'
end
group :development, :test do
  gem 'factory_girl_rails'
  gem 'pry-rails'
  gem 'pry-nav', '~> 0.2'
  gem 'pry-rescue'
  gem 'rspec-rails'
  gem 'spring', '~> 1.0'
  gem 'spring-commands-rspec', '~> 1.0'
  gem 'annotate'
end
group :production do
  gem 'unicorn'
end
group :test do
  gem 'capybara'
  gem 'capybara-screenshot'
  gem 'database_cleaner', '1.0.1'
  gem 'email_spec'
  gem 'shoulda-matchers', '~> 2.6', require: false
  gem 'grape-entity-matchers'
  gem 'poltergeist', '~> 1.5'
  gem 'simplecov', :require => false
end

gem 'koala'
gem 'google-api-client', require: 'google/api_client'
gem 'twitter'
gem 'apns'
gem 'clock'

gem 'geocoder'

gem 'fog', '~> 1.6'
gem 'carrierwave'
gem 'carrierwave_backgrounder'
gem 'rmagick', :require => 'RMagick'
gem 's3'

gem 'mechanize'
gem 'cached_web'
gem 'rapidfire', github: 'projectlounge/rapidfire'

gem 'twilio-ruby', '~> 3.11'

gem 'rack-google-analytics'

# Security
gem 'rack-attack', '4.1.0'

group :production do
  gem "lograge"
  gem 'heroku-deflater', '~> 0.5' 
  gem 'rails_12factor', '~> 0.0'
end
