$ ->
  $("a.sms").click (a)->
  	phone = prompt("Please enter the phone number (US only)","")
  	url = "/home/text?url=#{$(this).attr('url')}&phone=#{phone}"
  	$.ajax(url: url).done (html) ->
  		alert html
paneHeights = ->
	windowHeight = $(window).height();
	$(".pane, #intro-pane:not(.survey), #answer-pane").css "height", windowHeight
play = ->
	$("#intro-pane .btn").click ->
		$("body").scrollTo("#pane-1", 1000, {axis:'y'})
previous = ->
	$(".pane a.previous").click ->
		paneID = $(this).closest(".pane").attr('number');
		paneINT = parseInt(paneID) - 1
		paneLink = '#pane-' + paneINT
		if paneINT >= 1
			$("body").scrollTo(paneLink, 1000, {axis:'y'});
		else
			$("body").scrollTo("#intro-pane", 1000, {axis:'y'});			
answerReceived = ->
	$(".answer input").change ->
		paneID = $(this).closest(".pane").attr('number');
		paneINT = parseInt(paneID) + 1
		paneLink = '#pane-' + paneINT
		paneHeights()
		$("body").scrollTo(paneLink, 1000, {axis:'y'});
docready = ->
	paneHeights()
	play()
	previous()
	syncLabelForAttr()
	answerReceived()
syncLabelForAttr = ->
	$("#new_answer_group .pane").each ->
		i = 0
		while i < $(this).find(".answer label").length
			$(this).find(".answer label:nth-child(" + (i * 2 + 2) + ")").attr "for", $(this).find(".answer input:nth-child(" + (i * 2 + 1) + ")").attr("id")
			i++
$(document).ready ->
	docready()
$(window).resize ->
	paneHeights()
$(document).on('page:load', docready)