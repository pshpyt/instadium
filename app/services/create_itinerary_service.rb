class CreateItineraryService < Struct.new(:user, :product, :start_time, :end_time)
  def execute
    raise ArgumentError.new('You must specify a user a product a start_time an end_time') unless user.present? && product.present? && start_time.present? && end_time.present?
    user.itinerary_items.create product: product,
                                  starttime: start_time,
                                  endtime: end_time
    product.usage +=1
    product.save
  end
end