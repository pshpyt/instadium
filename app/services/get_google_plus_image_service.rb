class GetGooglePlusImageService
  attr_accessor :uid

  def initialize(user)
    @uid = user.uid
  end

  def execute(size)
    "https://plus.google.com/s2/photos/profile/#{uid}#{(size == 'large') ? '?s=300' : '?s=50'}"
  end

end