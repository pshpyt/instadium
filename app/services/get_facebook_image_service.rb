class GetFacebookImageService < Struct.new(:id)

  def execute(size = 'normal')
    size = 'small' unless allowed_sizes.include? size
   "https://graph.facebook.com/#{self.id}/picture?type=#{size}"
  end

private
  def allowed_sizes
    %w{small normal large square}
  end
end