class GetFacebookFriends < Struct.new(:token)

  def execute
    secret = GetFacebookEntry.new.execute[1]
    graph = Koala::Facebook::API.new(token, secret)
    friends = graph.get_connections("me", "friends", fields: "first_name, last_name, id")

    friends.map do |x|
      x.merge provider: :facebook,
              avatar_url: GetFacebookImageService.new(x['id']).execute,
              user_id: x['id']
    end
  rescue
    Rails.logger.warn "couldn't retrieve friends from facebook: #{$!}"
    Rails.logger.warn " backtrace: #{$!.backtrace}"
    []
  end

end
