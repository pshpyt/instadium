class GetGoogleFriends < Struct.new(:token)

  def execute
    id, secret = GetGoogleEntry.new.execute
    client = Google::APIClient.new :application_name => 'InStadium',
                                   :application_version => '1.0.0'

    client.authorization.client_id = id
    client.authorization.client_secret = secret
    client.authorization.scope = 'www.googleapis.com/auth/plus.login'

    client.authorization.access_token = token

    plus = client.discovered_api('plus', 'v1')

    response = client.execute(plus.people.list,
        :collection => 'visible',
        :userId => 'me',
        :headers => {'Content-Type' => 'application/json'}).body

    response = JSON.parse(response)
    response['items'].map do |x|
      fn, ln =  x['displayName'].split
      {
        id: x['id'],
        avatar_url: x['image']['url'],
        provider: :google,
        first_name: fn,
        last_name: ln
      }
    end
  rescue
    Rails.logger.warn "couldn't retrieve friends from google: #{$!}"
    Rails.logger.warn " backtrace: #{$!.backtrace}"
    []
  end

end


    # client = Google::APIClient.new(
    #   :application_name => 'NIA Encore application',
    #   :application_version => '1.0.0'
    # )
    # calendar_api = client.discovered_api('calendar', 'v3')

    # # Initialize OAuth 2.0 client    
    # client.authorization.client_id = '198829887409-na3tcgqit4pndp7i7r7j6mjboocga5ih.apps.googleusercontent.com'
    # client.authorization.client_secret = 'iEUoHeD7gFxFI09TT73J74n7'
    
    # client.authorization.scope = 'https://www.googleapis.com/auth/calendar'

    # client.authorization.access_token = oauth_access_token
