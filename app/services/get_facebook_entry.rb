class GetFacebookEntry
  def execute
    args = Devise.omniauth_configs[:facebook].args
    [args[0], args[1]]
  end
end