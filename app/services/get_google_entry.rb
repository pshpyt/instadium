class GetGoogleEntry
  def execute
    args = Devise.omniauth_configs[:google_oauth2].args
    [args[0], args[1]]
  end
end