class DetailsTransformer
  attr_accessor :chain

  def initialize
    self.chain = [PartitionByLocation, PartitionByLocation2, PartitionByLocation3, ClosedForSeason1, ClosedForSeason2, MissingHours, Regular]
  end

  def transform(hash)
    partial = hash
    chain.each do |e|
      begin
        instance = e.send :new
        return instance.execute(partial) if e.applicable?(partial)
      rescue
        Rails.logger.debug "Error transforming the hash: #{$!}"
        Rails.logger.debug $!.backtrace
      end
    end
    partial
  end

  class << self
    def transform(hash)
      DetailsTransformer.new.transform(hash)
    end
  end
end
