# This class recognizes if hours are missing
#


class MissingHours
  def self.applicable?(hash)
    hash['hours'].blank?
  end
  def execute(hash)
    hash
  end
end