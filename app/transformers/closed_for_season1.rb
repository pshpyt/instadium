# This class recognizes and transforms this pattern:
#
# "hours": [{
#   "day": "CLOSED FOR THE SEASON",
#   "time": " "
# }]

class ClosedForSeason1
  def self.applicable?(hash)
    hours = hash['hours']
    return false unless hours.present?
    hours.is_a?(Array) && hours.first.is_a?(Hash) && hours.first['day'].upcase == "CLOSED FOR THE SEASON" && hours.first['time'].blank?
  end
  def execute(hash)
    hours = hash['hours']
    new_hours = [{
      'name' => 'Closed for the season',
      'hours' => [{'day' => 'closed', 'time' => 'closed'}]
      }]
    hash.merge('hours' => new_hours)
  end
end