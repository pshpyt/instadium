# This class recognizes and transforms this pattern:
#
# "hours": [
#   {
#     "day": "Bar",
#     "time": " "
#   },
#   {
#     "day": "Daily",
#     "time": "5:00PM - 3:00AM"
#   },
#   {
#     "day": "Lounge",
#     "time": " "
#   },
#   {
#     "day": "Sun - Thurs",
#     "time": "8:00PM - 3:00AM"
#   },
#   {
#     "day": "Fri-Sat",
#     "time": "11:00PM - 3:00AM"
#   }
# ]

class PartitionByLocation
  def self.applicable?(hash)
    hours = hash['hours']
    return false unless hours.present?
    state = :starting
    hours.each do |segment|
      state = :checking_if_day_means_location_segment if segment.is_a?(Hash) && segment.values.all?{|x|x.present?} && ((state == :checking_if_location_timing_present) || (state == :checking_if_day_means_location_segment))
      state = :checking_if_location_timing_present if segment.is_a?(Hash) && segment['time'].blank? && segment['day'].present? && ((state == :checking_if_location_timing_present) || (state == :starting))
    end
    state == :checking_if_day_means_location_segment
  end
  def execute(hash)
    hours = hash['hours']
    new_hours = []
    hours_for_segment = []
    new_segment = {}
    hours.each do |segment|
      if segment.values.all?{|x|x.present?}
        hours_for_segment << segment
      end
      if ((segment['time'].blank? && segment['day'].present?) || (segment == hours.last))
        if new_segment['hours'].blank? && hours_for_segment.present?
          new_segment['hours'] = hours_for_segment
          hours_for_segment = []
          new_hours << new_segment
          new_segment = {}
        end
        new_segment['name'] = segment['day']
      end
    end
    hash.merge('hours' => new_hours)
  end
end