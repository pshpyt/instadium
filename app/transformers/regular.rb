# This class recognizes and transforms this pattern:
#
# "hours":  [{
#     "day": "Mon-Wed",
#     "time": "12:00PM - 10:00PM"
#   },{
#     "day": "Thu-Sun",
#     "time": "12:00PM - 12:00AM"
# }]

# "hours": [{
#   "day": "Thu-Sun",
#   "time": "Open at 1:00AM"
# }]

class Regular
  def self.applicable?(hash)
    true
  end
  def execute(hash)
    hours = hash['hours']
    new_hours = [{
      'name' => 'Open Hours',
      'hours' => hours
      }]
    hash.merge('hours' => new_hours)
  end
end