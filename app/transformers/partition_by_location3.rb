# This class recognizes and transforms this pattern:
#
# "hours":       [
#         "Restaurant",
#                 {
#           "day": "Sun-Thu   ",
#           "time": "11:00AM - 10:00PM"
#         },
#                 {
#           "day": "Fri &amp; Sat ",
#           "time": "11:30AM - 12:00AM"
#         },
#         "Bar",
#                 {
#           "day": "Sun &amp; Mon ",
#           "time": "11:30AM - 12:00AM"
#         },
#                 {
#           "day": "Tue-Thu ",
#           "time": "11:30AM - 1:00AM"
#         },
#                 {
#           "day": "Fri-Sat ",
#           "time": "11:30AM - 4:00AM"
#         }
#       ]

class PartitionByLocation3
  def self.applicable?(hash)
    hours = hash['hours']
    return false unless hours.present?
    state = :starting
    hours.each do |segment|
      state = :checking_if_first_means_location_segment if segment.is_a?(Hash) && segment.values.any?{|x|x.present?} && ((state == :checking_if_location_timing_present) || (state == :checking_if_first_means_location_segment))
      state = :checking_if_location_timing_present if segment.is_a?(String) && ((state == :checking_if_location_timing_present) || (state == :starting))
    end
    state == :checking_if_first_means_location_segment
  end
  def execute(hash)
    hours = hash['hours']
    new_hours = []
    hours_for_segment = []
    new_segment = {}
    hours.each do |segment|
      if segment.is_a?(Hash) && segment.values.any?{|x|x.present?}
        hours_for_segment << segment
      end
      if segment.is_a?(String) || (segment == hours.last)
        if new_segment['hours'].blank? && hours_for_segment.present?
          new_segment['hours'] = hours_for_segment
          hours_for_segment = []
          new_hours << new_segment
          new_segment = {}
        end
        new_segment['name'] = segment
      end
    end
    hash.merge('hours' => new_hours)
  end
end