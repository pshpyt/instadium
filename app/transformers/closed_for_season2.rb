# This class recognizes and transforms this pattern:
#
# "hours": [{
#   "day": " ",
#   "time": "CLOSED FOR THE SEASON"
# }]

class ClosedForSeason2
  def self.applicable?(hash)
    hours = hash['hours']
    return false unless hours.present?
    hours.is_a?(Array) && hours.first.is_a?(Hash) && hours.first['day'].blank? && hours.first['time'].upcase == "CLOSED FOR THE SEASON"
  end
  def execute(hash)
    hours = hash['hours']
    new_hours = [{
      'name' => 'Closed for the season',
      'hours' => [{'day' => 'closed', 'time' => 'closed'}]
      }]
    hash.merge('hours' => new_hours)
  end
end