class Admin::TriviaConfigsController < ApplicationController
  layout 'admin'
  before_filter :authenticate_user!
  before_action :set_trivia_config, only: [:show, :edit, :update, :destroy]

  # GET /trivia_configs
  # GET /trivia_configs.json
  def index
    @trivia_configs = TriviaConfig.all
  end

  # GET /trivia_configs/1
  # GET /trivia_configs/1.json
  def show
  end

  # GET /trivia_configs/new
  def new
    @trivia_config = TriviaConfig.new
  end

  # GET /trivia_configs/1/edit
  def edit
  end

  # POST /trivia_configs
  # POST /trivia_configs.json
  def create
    @trivia_config = TriviaConfig.new(trivia_config_params)

    respond_to do |format|
      if @trivia_config.save
        format.html { redirect_to admin_trivia_config_path(@trivia_config), notice: 'Trivia config was successfully created.' }
        format.json { render action: 'show', status: :created, location: @trivia_config }
      else
        format.html { render action: 'new' }
        format.json { render json: @trivia_config.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /trivia_configs/1
  # PATCH/PUT /trivia_configs/1.json
  def update
    respond_to do |format|
      if @trivia_config.update(trivia_config_params)
        format.html { redirect_to admin_trivia_config_path(@trivia_config), notice: 'Trivia config was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @trivia_config.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /trivia_configs/1
  # DELETE /trivia_configs/1.json
  def destroy
    @trivia_config.destroy
    respond_to do |format|
      format.html { redirect_to admin_trivia_configs_url }
      format.json { head :no_content }
    end
  end

  def delete_image
    tc = TriviaConfig.find(params[:id])
    case params[:image_type]
    when "presented_image"
      tc.remove_presented_image!
      tc.save
    when "ad_image"
      tc.remove_ad_image!
      tc.save
    end
    respond_to do |format|
      format.js { render :text => "ok" }
    end

  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trivia_config
      @trivia_config = TriviaConfig.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def trivia_config_params
      params.require(:trivia_config).permit(:url, :presented_image, :ad_image, :share_message, :trivia_number,
                          trivia_attributes: [:id, :title, :url, :question_number, :question, :answers, :answer, :_destroy])
    end
end
