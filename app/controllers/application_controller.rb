class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_path, :alert => exception.message
  end

  ADMIN_AUTH_COOKIE_NAME = :auth_token

  def can_administer?
    current_user.try(:has_role?, :admin)
  end

  def access_denied(exception)
    redirect_to new_user_session_path, :alert => exception.message
  end

  def set_auth_cookie(new_token, remember_me = false)
    if remember_me
      cookies[ADMIN_AUTH_COOKIE_NAME] = {
          :value => new_token,
          :expires => Clock.now + 60.days,
          :path => '/'
      }
    else
      cookies[ADMIN_AUTH_COOKIE_NAME] = {
          :value => new_token,
          :path => '/'
      }
    end
  end
end
