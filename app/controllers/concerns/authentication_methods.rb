require 'active_support/concern'

module AuthenticationMethods
  extend ActiveSupport::Concern

  included do

    def process_omni_auth_login(oauth_provider, email=nil)
      begin
        token = login_with_token(oauth_provider, email) if load_omniauth_data(oauth_provider, params["#{oauth_provider}_token"])
        if token
          status 200
          present token.user, :with => Presenters::DetailedUserWithTokenPresenter
        else
          error!({ "error_type" => (signed_in? ? "#{oauth_provider}_account_already_linked" : "#{oauth_provider}_not_authenticated") }, 401)
        end
      rescue
        error!({ "error_type" => $!.to_s, "backtrace" =>  $!.backtrace })
      end
    end

private

    def load_omniauth_data(provider, token)
      if :facebook == provider
        load_facebook_data token
      elsif :twitter == provider
        load_twitter_data token
      elsif :google_oauth2 == provider
        load_google_oauth2_data token
      elsif :instagram == provider
        load_instagram_data token
      end
    end

    def build_user_from_token(oauth_provider, email)
      user = User.find_by_social_identity_uid(oauth_provider, @uid)
      return user if user.present?

      use_social_identity_avatar = true
      user = User.find_by_email(@email) if @email.present?
      if user.blank?
        user_params = {
          :username => @provider_username,
          :email => @email || email,
          :first_name => @first_name,
          :last_name => @last_name,
          :full_name => @full_name,
          :password => User.newpass(3),
          :display_location => @display_location,
          :use_social_identity_avatar => use_social_identity_avatar
        }

        user, error = User.create_user_from_params(user_params)
        if user.present? && [:facebook, :google_oauth2].include?(oauth_provider)
          # user.confirm!
        else
          # The spec detects that an email is already send.
          # user.send_confirmation_instructions
        end
      end
      user.create_or_update_identity(oauth_provider, @uid, @oauth_token, @provider_username, @social_identity_avatar_url)
      user
    end

    def associate_with_current_user(provider)
      # TODO: PMG - Determine if we want to update full name, other data during this process
      user = User.find_by_social_identity_uid(provider, @uid)

      if user.nil? || current_user == user
        current_user.create_or_update_identity(provider, @uid, @oauth_token, @provider_username, @social_identity_avatar_url)
        if @email.present?
          current_user.email = @email 
          current_user.save
        end
        current_user
      else
        # Problem - Facebook account is already linked with another user
        # TODO: PMG - Consult with product on resolution.  For the moment, return nil
        nil
      end
    end

    def login_with_token(oauth_provider, email)
      user = if signed_in?
        associate_with_current_user(oauth_provider)
      else
        build_user_from_token(oauth_provider, email)
      end
      return nil if user.nil?
      token = user.get_authentication_token
    end

    def load_twitter_data(oauth_token)
      twitter_data = Social::TwitterFacade.get_identity_data(params[:twitter_token], params[:twitter_secret])
      return nil unless twitter_data.present?
      @uid = twitter_data[:screen_name]
      return nil unless @uid.present?
      @provider_username = twitter_data[:screen_name]
      @email = params[:email]
      @oauth_token = oauth_token
      @display_location = twitter_data[:time_zone][:name]
      true
    end

    def load_facebook_data(oauth_token)
      facebook_data = Social::FacebookFacade.get_identity_data(oauth_token)
      return nil unless facebook_data.present? # Facebook didn't validate the OAuth token
      @uid = facebook_data["id"]
      return nil unless @uid.present?
      @provider_username = facebook_data["username"]
      @email = facebook_data["email"]
      @first_name = facebook_data["first_name"]
      @last_name = facebook_data["last_name"]
      @full_name = facebook_data["name"]
      @social_identity_avatar_url = "http://graph.facebook.com/#{@uid}/picture"
      @oauth_token = oauth_token
      @display_location = facebook_data["location"]["name"] unless facebook_data["location"].nil? || facebook_data["location"]["name"].blank?
      true
    end

    def load_google_oauth2_data(oauth_token)
      google_data = Social::GoogleFacade.get_identity_data(oauth_token)
      return nil unless google_data.present? # Google didn't validate the OAuth token
      @uid = google_data["id"]
      return nil unless @uid.present?
      @provider_username = google_data["username"]
      if google_data["emails"].present?
        @email = google_data["emails"].first['value']
      else
        throw "Email not found"
      end
      @first_name = google_data['name']["givenName"]
      @last_name = google_data['name']["familyName"]
      @full_name = google_data["displayName"]
      @oauth_token = oauth_token
      @display_location = google_data["placesLived"].first["value"] unless google_data["placesLived"].nil?
      true
    end

  end

end
