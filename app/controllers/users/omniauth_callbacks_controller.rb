class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController

  def all
    auth = request.env["omniauth.auth"]
    redirect_to root_url, :alert => "Authentication error" and return if auth.nil?

    @identity = SocialIdentity.find_with_omniauth(auth) ||
                SocialIdentity.build_with_omniauth(auth)
    token = auth.credentials.token rescue ''
    @identity.update_token_and_secret token if @identity.persisted?

    if signed_in?
      if @identity.user == current_user
        redirect_to root_url, notice: "Already linked that account!"
      else
        @identity.user = current_user
        @identity.save
        redirect_to root_url, notice: "Successfully linked that account!"
      end
    else
      if @identity.user.present?
        # The identity we found had a user associated with it so let's 
        # just log them in here
        token = @identity.user.get_authentication_token
        set_auth_cookie(token.token)
        sign_in @identity.user

        redirect_to root_url, notice: "Signed in!"
      else
        email = if auth['info']
          auth['info']['email']
        else
          nil
        end
        @user = User.find_by_email(email)
        if @user.present?
          @identity.user = @user
          @identity.save
          sign_in @user
          redirect_to root_url, notice: "Signed in!"
        else
          @user = User.create_with_omniauth(auth)
          @identity.user = @user
          @identity.save
          session["devise.facebook_data"] = auth
          sign_in @user
          redirect_to root_url, notice: "New user created"
        end
      end
    end
  end

  alias_method :facebook, :all
  alias_method :twitter, :all
  alias_method :google_oauth2, :all
end
