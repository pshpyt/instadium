class HomeController < ApplicationController
  def index
    cookies[:originator_code] = params[:originator_code] if params[:originator_code]
    cookies[:source] = params[:source] if params[:source]
    if params[:url]
      @trivia_config = TriviaConfig.find_by_url(params[:url])
    else
      @trivia_config = TriviaConfig.find_by_url("first") rescue nil
      @trivia_config = TriviaConfig.first if !@trivia_config
    end
    @trivia = @trivia_config.trivia.order(:question_number) if @trivia_config
    #@trivia = Trivium.where(url: params[:url]).order(:question_number)
    render 'trivia' if @trivia
  end

  def answers
    result = Result.new
    unless params[:url]
      params[:url] = TriviaConfig.first.url
    end
    if @trivia_config = TriviaConfig.find_by_url(params[:url])
      @share_message = @trivia_config.share_message
      @next_trivia = TriviaConfig.find_by_trivia_number(@trivia_config.trivia_number+1) if @trivia_config.trivia_number
    else
      @share_message = "Play this sports trivia game: https://instadium.encore.io/"
    end

    @message = "Share this trivia game with your friends!" 

    @count = @trivia_config.trivia.size
    @right_count = 0
    right_answers = []
    if params[:answer]
      params[:answer].each_pair do |question_number, answer|
        trivium = @trivia_config.trivia.where(question_number: question_number).first
        if answer == trivium.answer
          @right_count += 1
          right_answers << question_number
        end
      end
    end
    @trivia = @trivia_config.trivia.order(:question_number)
     
    result.ip = request.remote_ip
    result.url = params[:url]
    result.right_answers = right_answers
    result.score = @right_count 
    result.started_at = params[:started_at]
    result.finished_at = Time.now
    result.user_code = request.session_options[:id]
    result.originator_code = cookies[:originator_code] if cookies[:originator_code]
    result.source = cookies[:source] if cookies[:source]
    result.save 
  end
  
  def text

    if @trivia_config = TriviaConfig.find_by_url(params[:url])
      @share_message = @trivia_config.share_message
    else
      @share_message = "Play this sports trivia game: https://instadium.encore.io/"
    end


    # put your own credentials here
    account_sid = 'ACa2da4f4e7abc4516e086f633d6cbee26' 
    auth_token = '3e626cf6c6141c321ddd319198fa2de3'
    @client = Twilio::REST::Client.new account_sid, auth_token 
    phone = params[:phone].gsub(/[\D]/,'')
    @client.account.messages.create({
      :from => '+16174010555',
      :to => "+1#{phone}",
      :body => @share_message    
    })
    render text: "Message sent"
  end
end
