# == Schema Information
#
# Table name: itinerary_items
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  product_id :integer
#  starttime  :datetime
#  endtime    :datetime
#  created_at :datetime
#  updated_at :datetime
#

class ItineraryItem < ActiveRecord::Base
  belongs_to :user
  belongs_to :product
end
