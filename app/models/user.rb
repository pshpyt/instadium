# == Schema Information
#
# Table name: users
#
#  id                         :integer          not null, primary key
#  email                      :string(255)      default(""), not null
#  encrypted_password         :string(255)      default(""), not null
#  reset_password_token       :string(255)
#  reset_password_sent_at     :datetime
#  remember_created_at        :datetime
#  sign_in_count              :integer          default(0), not null
#  current_sign_in_at         :datetime
#  last_sign_in_at            :datetime
#  current_sign_in_ip         :string(255)
#  last_sign_in_ip            :string(255)
#  created_at                 :datetime
#  updated_at                 :datetime
#  name                       :string(255)
#  username                   :string(255)
#  full_name                  :string(255)
#  first_name                 :string(255)
#  last_name                  :string(255)
#  confirmation_token         :string(255)
#  confirmed_at               :datetime
#  confirmation_sent_at       :datetime
#  unconfirmed_email          :datetime
#  latitude                   :string(255)
#  longitude                  :string(255)
#  avatar                     :string(255)
#  use_social_identity_avatar :boolean          default(FALSE)
#  gender                     :string(255)
#  bio                        :text
#  location_last_update_time  :datetime
#  disabled                   :boolean
#  zip                        :string(255)
#  dob                        :datetime
#

class User < ActiveRecord::Base
  rolify
  mount_uploader :avatar, ImageUploader

  def non_social_identity_avatar(size=:large)
    if self.avatar.present?
      self.avatar.url(size)
    elsif self.email.present?
      hash = Digest::MD5.hexdigest(self.email)
      "http://www.gravatar.com/avatar/#{hash}#{(size == 'large') ? '?s=300' : '?s=50'}"
    else
      #TODO this should go to the view layer
      image_url("profile_silhouette.png")
    end
  end

  concerning :Friends do
    included do
      acts_as_followable
      acts_as_follower
    end
  end

  concerning :Login do
    included do
      # Include default devise modules. Others available are:
      # :confirmable, :lockable, :timeoutable and :omniauthable

      devise :database_authenticatable, :registerable,
             :recoverable, :rememberable, :trackable,
             # :validatable, :confirmable,
             :validatable,
             :omniauthable, :omniauth_providers => [:facebook, :google_oauth2, :twitter, :instagram]

    end

  end

  concerning :SocialLogin do
    included do
      has_many :social_identities
      has_many :authentication_tokens
      has_many :itinerary_items

      DISALLOWED_CHARACTERS_REGEXP = /[\$\s&\+,\/:;=\?\@"'><#%\{\}\|\\\^~\]\[`\!\)\()]/
      RESERVED_USERNAMES = %w(admin)

      class << self
        def create_with_omniauth(auth = {})
          User.create! do |user|
            if auth['info']
               user.email  = auth['info']['email']
               user.password = newpass(3)
               # TODO except for twitter.
               user.confirmed_at = Time.now
               username_candidate = auth['info']['name']
            end
            user.username = create_unique_username(username_candidate, user.email)
          end
        end

        def reserved_username?(username)
          RESERVED_USERNAMES.include?(username)
        end

        def remove_disallowed_characters(username)
          return "" if username.nil?
          username.gsub(DISALLOWED_CHARACTERS_REGEXP, '')
        end

        def build_full_name_from_first_and_last(first_name, last_name, full_name)
          return full_name unless full_name.blank?
          names = [first_name, last_name].delete_if(&:blank?)
          names.empty? ? nil : names.join(" ")
        end

        def create_username_from_email(email)
          return "" if email.nil? || email.blank?
          email.match(/(.+)@/)[1] rescue nil
        end

        def username_prefix(suggested_username, email = nil)
          username_prefix = User.remove_disallowed_characters(suggested_username)
          username_prefix = create_username_from_email(email) if username_prefix.blank?
          username_prefix = "newuser" if username_prefix.blank?
          username_prefix = "newuser#{username_prefix}" if username_prefix.match(/^[0-9]*/)[0] == username_prefix
          username_prefix
        end

        def create_unique_username(username, email = nil)
          base_name = self.username_prefix(username, email)
          candidate = base_name

          10.times do
            return candidate if not User.find_by_username(candidate) || User.reserved_username?(candidate)
            candidate = "#{base_name}#{SecureRandom.hex(2)}"
          end
          raise "couldn't find unique username"
        end

        def find_by_social_identity_uid(oauth_provider, uid)
          si = SocialIdentity.where(:provider => oauth_provider.to_s, :uid => uid.to_s).first
          return nil if si.nil?
          si.user
        end
      end

    end

    def cover_image_url(size = :large)
      begin
        if cover_image.present?
          if cover_image_processing
            cover_image.url
          else
            cover_image.url(size)
          end
        else
          ActionController::Base.helpers.asset_path("default-cover.jpg")
        end
      rescue
        ActionController::Base.helpers.asset_path("default-cover.jpg")
      end
    end

    def image_url(size="large")
      begin
        if self.image?
          if self.image_processing
            self.image.url
          else
            self.image.url(size.to_sym)
          end
        # elsif self.social_image.present?
        #   self.social_image
        elsif self.facebook_identities.size > 0 
            #used for facebook accounts (we have social, but this does sizes)
            GetFacebookImageService.new(self.facebook_identities.first.uid).image(size)
        elsif self.twitter_identities.size > 0 
          self.twitter_identities.first.social_image_url
        elsif self.instagram_identities.size > 0 
          self.instagram_identities.first.social_image_url
        elsif self.google_oauth2_identities.size > 0
          self.google_oauth2_identities.first.social_image_url
        elsif self.email.present?
          GetGravatarImageService.new(self).image(size)
        else
          ActionController::Base.helpers.asset_path("profile_silhouette.png")
        end
      rescue
        ActionController::Base.helpers.asset_path("profile_silhouette.png")
        # Base.helpers.image_path
      end
    end

    def avatar_url(size=:large)
      if use_social_identity_avatar &&
          social_identities.present? &&
          (url = social_identities.find{|x|x.avatar_url.present?})
        url.avatar_url
      else
        non_social_identity_avatar(size)
      end
    end

    def the_identity(provider)
      return nil if self.send("#{provider}_identities").empty?
      self.send("#{provider}_identities").first
    end

    def create_or_update_identity(provider, uid, oauth_token, provider_username, social_identity_avatar_url)
      return nil if uid.nil?
      si = self.send("#{provider}_identities").where(:uid => uid).first
      if si.present?
        si.update_token_and_secret(oauth_token) if oauth_token.present?
      else
        si =  self.social_identities.create! do |si|
          si.provider = provider.to_s
          si.uid = uid
          si.token = oauth_token
          si.provider_username = provider_username
          si.last_verified_at = Clock.now
          si.avatar_url = social_identity_avatar_url
        end
      end
      si
    end
  end

  concerning :ApiLogin do
    included do
      # the :google_identity is :google_oauth2
      def self.has_many_identities *args
        args.each do |a|
          class_eval do
            has_many "#{a}_identities".to_sym, -> { where provider: a.to_s}, :class_name => "SocialIdentity"
          end
        end
      end

      has_many_identities :instagram, :google_oauth2, :twitter, :facebook
      # alias_method :verified, :confirmed?
      def verified
        true
      end

      def confirmed?
        true
      end

      alias_method :guid, :id

      class << self
        @@sportwords = %w{ 2minutedrill 3pointer allstar amateur arena athlete athletics award ballpark baseball baseballbat basketball blitz bunt captain catch champion cheer chipshot coach comeback compete contest course court crosscheck cup curveball deepthreat defeat defend defense doubleplay draftpick draw dribble fan field fieldgoal finalfour finalscore fitness football gameday goal goalie golf grandslam gymnasium hailmary half-time halfcourt hockeystick homerun instantreplay interception judge lacrosse league linedrive lob manager medal mvp offside opponent pass penalty penaltyshot pitch playbook player playoffs practice professional punt quaterback racquet rally rallymonkey record referee replay rules runningback score scouting serve shoot skill slamdunk soccer southpaw spectator stadium strategy swish tackle tactics teammate teamwork teeoff tennis tie titans touchdown tournament train trainer tripleplay trophy umpire veteran victory volleyball whistle }

        def newpass( len )
          newpass = SecureRandom.random_number(10000)
          return "#{@@sportwords[rand(@@sportwords.size-1)]}#{newpass}"
        end

        def create_user_from_params(params = {})
          username = params[:username]
          email = params[:email]
          user = self.create! do |user|
            user.username = self.create_unique_username(username, email)
            user.email = email
            user.password = params[:password]
            first_name = params[:first_name].strip! || params[:first_name] unless params[:first_name].nil?
            user.first_name = first_name unless first_name.blank?
            last_name = params[:last_name].strip! || params[:last_name] unless params[:last_name].nil?
            user.last_name = params[:last_name] unless params[:last_name].blank?
            full_name = params[:full_name] || params[:name]
            user.full_name = User.build_full_name_from_first_and_last(user.first_name, user.last_name, full_name)
            user.avatar = params[:avatar] unless params[:avatar].blank?
            user.display_location = params[:display_location] unless params[:display_location].blank?
            user.bio = params[:bio] unless params[:bio].blank?
            user.use_social_identity_avatar = params[:use_social_identity_avatar] unless params[:use_social_identity_avatar].nil?
            user.disabled = true
          end
          [user, nil]
        rescue Exception => e
          [nil, e.to_s]
        end

        def login_with_email_or_username_and_password(email_or_username, password)
          return nil if email_or_username.blank? || password.blank?
          user = self.find_by_email(email_or_username)
          user = self.find_by_username(email_or_username) unless user
          user = nil unless user.valid_password?(password) if user
          return nil unless user
          token = user.get_authentication_token
        end

      end
    end

    def password_required?
      super if confirmed?
    end

    def get_authentication_token
      token = self.authentication_tokens.valid.latest_first.first
      return token if token.present?
      AuthenticationToken.create_token_for_user(self)
    rescue Exception => e
      nil
    end

  end

end
