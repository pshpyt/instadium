# == Schema Information
#
# Table name: groups
#
#  id                :integer          not null, primary key
#  name              :string(255)
#  description       :text
#  users             :integer          is an Array
#  created_at        :datetime
#  updated_at        :datetime
#  unconfirmed_users :integer          is an Array
#

class Group < ActiveRecord::Base
  has_many :messages
end
