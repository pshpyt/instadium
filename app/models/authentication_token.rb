# == Schema Information
#
# Table name: authentication_tokens
#
#  id         :integer          not null, primary key
#  token      :string(255)
#  user_id    :integer
#  expires_at :datetime
#  created_at :datetime
#  updated_at :datetime
#

class AuthenticationToken < ActiveRecord::Base
  belongs_to :user

  before_validation :generate_token, :on => :create
  validates :user, :presence => true

  scope :valid,  -> { where('authentication_tokens.expires_at >= ?', Clock.now) }
  scope :by_value,  ->(v) { where(:token => v) }
  scope :latest_first, -> { order('authentication_tokens.expires_at DESC') }

  def self.find_user_by_token_value(token_value)
    token = AuthenticationToken.valid.by_value(token_value).includes(:user).first
    return nil if token.nil?
    return nil if (token.user and (token.user.disabled == true))
    token.user
  end

  def generate_token
    self.token = SecureRandom.hex(16)
  end

  DEFAULT_EXPIRES_INTERVAL = 60.days

  def self.create_token_for_user(user, expires_at = (Clock.now + DEFAULT_EXPIRES_INTERVAL))
    user.authentication_tokens.create! do |t|
      t.expires_at = expires_at
    end
  end
end
