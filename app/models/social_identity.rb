# == Schema Information
#
# Table name: social_identities
#
#  id                :integer          not null, primary key
#  user_id           :integer          not null
#  provider          :string(255)
#  uid               :string(255)
#  provider_username :string(255)
#  token             :string(255)
#  secret            :string(255)
#  last_verified_at  :datetime
#  expires_at        :datetime
#  created_at        :datetime
#  updated_at        :datetime
#  avatar_url        :string(255)
#

class SocialIdentity < ActiveRecord::Base
  belongs_to :user

  ALLOWED_PROVIDERS = %w(facebook twitter google_oauth2 instagram)

  validates :provider, :inclusion => {:in => ALLOWED_PROVIDERS}

  def self.find_with_omniauth(auth)
    find_by_provider_and_uid(auth['provider'], auth['uid'])
  end

  def self.build_with_omniauth(auth)
    token = auth.credentials.token rescue nil
    params = {uid: auth['uid'], provider: auth['provider'], token: token}
    if auth['provider'] == 'facebook'
      params.merge! avatar_url: auth['info']['image']
    elsif auth['provider'] == 'google_oauth2'
      params.merge! avatar_url: auth['info']['image']
    end
    self.new(params)
  end

  def update_token_and_secret(oauth_token, oauth_secret = nil)
    self.token = oauth_token
    self.secret = oauth_secret
    self.last_verified_at = Clock.now
    self.save
  end

  def needs_verification?
    return true if last_verified_at.nil?
    return last_verified_at < (Clock.now - 1.day)
  end

# #TODO put these to some other place...
#   def refresh_facebook_token_if_needed
#     return false unless needs_verification?
#     return false unless self.token.present?
#     @graph = Koala::Facebook::API.new(oauth_access_token)
#     resp = @graph.get_object("me")
#     if resp == nil
#       # Current token is invalid, so wipe it out
#       self.token = nil
#       self.last_verified_at = nil
#     else
#       self.uid = resp["id"]
#       self.provider_username = resp["username"]
#       self.last_verified_at = Clock.now
#     end   
#     self.save
#   end

# #TODO put these to some other place...
  # def update_with_new_facebook_token(new_token)
  #   return nil if new_token.nil?
  #   @graph = Koala::Facebook::API.new(oauth_access_token)
  #   resp = @graph.get_object("me")
  #   if (resp == nil)
  #     # Invalid token
  #     return
  #   elsif resp["id"] != self.uid
  #     # Token is not for this user
  #     # TODO: PMG - Log error for this case
  #     return
  #   else
  #     self.provider_username = resp["username"]
  #     self.token = new_token
  #     self.last_verified_at = Clock.now
  #     self.save
  #   end
  #   new_token
  # end
end
