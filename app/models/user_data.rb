# == Schema Information
#
# Table name: user_data
#
#  id         :integer          not null, primary key
#  key        :string(255)
#  value      :hstore
#  created_at :datetime
#  updated_at :datetime
#

class UserData < ActiveRecord::Base
  serialize :value, ActiveRecord::Coders::NestedHstore
end
