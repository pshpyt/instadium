# == Schema Information
#
# Table name: trivia
#
#  id              :integer          not null, primary key
#  title           :string(255)
#  url             :string(255)
#  question_number :integer
#  question        :string(255)
#  answers         :string(255)      is an Array
#  answer          :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#

class Trivium < ActiveRecord::Base
  belongs_to :trivia_config
end
