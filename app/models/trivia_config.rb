class TriviaConfig < ActiveRecord::Base
  has_many :trivia, -> { order(:question_number) }, :dependent => :destroy
  mount_uploader :presented_image, ImageUploader
  mount_uploader :ad_image, ImageUploader
  accepts_nested_attributes_for :trivia,  :allow_destroy => true, :reject_if => :all_blank
end
