# == Schema Information
#
# Table name: api_keys
#
#  id         :integer          not null, primary key
#  token      :string(255)
#  expires_at :datetime
#  created_at :datetime
#  updated_at :datetime
#

class ApiKey < ActiveRecord::Base
  before_create :generate_access_token

private

  def generate_access_token
    begin
      self.token = SecureRandom.hex
    end while self.class.exists?(token: token)
  end

end
