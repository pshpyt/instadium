class Result < ActiveRecord::Base
  def self.answer_statistic
    total = Result.count.to_f
    q1, q5, q10 = 0, 0, 0
    Result.all.each do |result|
     q1 += 1 if result.right_answers.include?("1") 
     q5 += 1 if result.right_answers.include?("5")
     q10 += 1 if result.right_answers.include?("10")
    end
    

    return [(q1/total)*100, (q5/total)*100, (q10/total)*100] 
  end
end
