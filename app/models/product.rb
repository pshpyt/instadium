# == Schema Information
#
# Table name: products
#
#  id                :integer          not null, primary key
#  brand             :string(255)
#  entity_path       :string(255)
#  market            :string(255)
#  market_rank       :integer
#  name              :string(255)
#  property_code     :string(255)
#  property_rank     :integer
#  category          :string(255)
#  created_at        :datetime
#  updated_at        :datetime
#  feed_id           :string(255)
#  expired           :boolean
#  coordinates       :hstore
#  mass_relevance    :hstore
#  short_description :text
#  details           :hstore
#  usage             :integer          default(0)
#

class Product < ActiveRecord::Base
  serialize :details, ActiveRecord::Coders::NestedHstore
  serialize :mass_relevance, ActiveRecord::Coders::NestedHstore
  serialize :coordinates, ActiveRecord::Coders::NestedHstore
  CATEGORY_BLACKLIST = ['promotions', 'property-feature', 'Property Feature']

  scope :actual, -> {where('((expired = false) OR (expired IS NULL)) AND (category NOT IN (:list))', {list: CATEGORY_BLACKLIST})}
end
