json.array!(@results) do |result|
  json.extract! result, :id, :ip, :url, :right_answers, :started_at, :finished_at, :score
  json.url result_url(result, format: :json)
end
