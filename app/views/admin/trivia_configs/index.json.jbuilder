json.array!(@trivia_configs) do |trivia_config|
  json.extract! trivia_config, :id, :url, :presented_image, :ad_image, :share_message
  json.url trivia_config_url(trivia_config, format: :json)
end
