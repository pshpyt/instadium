json.array!(@trivia) do |trivium|
  json.extract! trivium, :id, :title, :question, :answers, :answer
  json.url trivium_url(trivium, format: :json)
end
