class String
  def int?
    true if Integer(self) rescue false
  end
  def float?
    true if Float(self) rescue false
  end
  def boolean?
    true if 'true' == self.downcase || 'false' == self.downcase
  end
  def remove_html_tags
    re = /<("[^"]*"|'[^']*'|[^'">])*>/
    self.gsub(re, '')
  end
end

class Hash
  def arrayize_series
    deep_transform(self, deep: true) do |h, key, value|
      if value.is_a? Hash
        if ['dateranges'].include? key.to_s
          h[key] = value.values
        else
          keys = value.keys
          pattern = /.*_(\d*)/
          if keys.all?{|x|x.to_s.match pattern} && keys.map{|x|x.to_s.match pattern; $1.to_i}.sort.each_cons(2).to_a.map{|x|x[1] -x[0]}.uniq.size == 1
            h[key] = value.values
          else
            h[key] = value
          end
        end
      else
        h[key] = value
      end
    end
  end
  def booleanize_values(deep=true)
    deep_transform(self, deep: deep) do |h, key, value|
      if value.is_a?( String) && value.boolean?
        h[key] = true if value.downcase == 'true'
        h[key] = false if value.downcase == 'false'
      else
        h[key] = value
      end
    end
  end
  def remove_tags(deep=true)
    deep_transform(self, deep: deep) do |h, key, value|
      if value.is_a?( String)
        h[key] = value.remove_html_tags
      else
        h[key] = value
      end
    end
  end
  def underscore_values(deep=true)
    deep_transform(self, deep: deep) do |h, key, value|
      if value.is_a?( String)
        h[key] = value.underscore
      else
        h[key] = value
      end
    end
  end
  def underscore_keys(deep=true)
    deep_transform(self, deep: deep) do |h, key, value|
      h[key.to_s.underscore] = value
    end
  end
  def numericalize_values(deep=true)
    deep_transform(self, deep: deep) do |h, key, value|
      if value.is_a? String
        if value.int?
          h[key] = value.to_i
        elsif value.float?
          h[key] = value.to_f
        else
          h[key] = value
        end
      else
        h[key] = value
      end
    end
  end

private

  def deep_transform(original, options={}, &block)
    original.inject({}){|result, (key,value)|
      value = if (options[:deep] && Hash === value) 
                deep_transform(value, options, &block)
              else 
                value
              end
      block.call(result,key,value)
      result
    }
  end
end
