module Presenters
  class DetailedUserWithTokenPresenter < Grape::Entity
    expose :me, using: Presenters::DetailedUserPresenter do |user, options|
      user
    end
    expose :token do |user, options|
      user.get_authentication_token.token
    end
  end
end