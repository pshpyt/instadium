module Presenters
  class GroupPresenter < Grape::Entity
    expose :id
    expose :name
    expose :description
    expose :members do |group, options|
      if group.users.present?
        group.users.map do |id|
          u = User.find id
        end
      else
        []
      end
    end
    expose :pending_members do |group, options|
      if group.unconfirmed_users.present?
        group.unconfirmed_users.map do |id|
          u = User.find id
        end
      else
        []
      end
    end
    expose :users
    expose :unconfirmed_users
  end
end
