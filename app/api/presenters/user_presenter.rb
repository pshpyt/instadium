module Presenters
  class UserPresenter < Grape::Entity
    expose :id
    expose :name
    expose :username
    expose :first_name
    expose :last_name
    expose :full_name
  end
end