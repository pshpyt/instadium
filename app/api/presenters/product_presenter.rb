module Presenters
  class ProductPresenter < Grape::Entity
    expose :id
    expose :usage
    expose :brand
    expose :entity_path
    expose :market
    expose :market_rank
    expose :name
    expose :property_code
    expose :property_rank
    expose :category
    expose :created_at
    expose :updated_at
    expose :short_description
    expose :mass_relevance
    expose :details do |product, options|
      original_details = product.details
      if original_details.present?
        original_details.numericalize_values(false).booleanize_values(false)
      else
        {}
      end
    end
    expose :coordinates do |product, options|
      if product.coordinates.present?
        latitude = product.coordinates.fetch('latitude'){0}
        longitude = product.coordinates.fetch('longitude'){0}
        {latitude: latitude.to_f, longitude: longitude.to_f}
      else
        {}
      end
    end
  end
end
