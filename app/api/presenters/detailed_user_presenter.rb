module Presenters
  class DetailedUserPresenter < UserPresenter
    format_with(:iso_timestamp) { |dt| dt.iso8601 }
    expose :image_url
    expose :cover_image_url
    expose :email
    expose :disabled
    expose :zip
    expose :dob
    expose :created_at
    expose :agree_on_email_marketing
    expose :gender
    expose :phone_number
    expose :friends do |user, options|
      invited = user.following_by_type('User').map{|x|x.id}
      invitations = user.followers_by_type('User').map{|x|x.id}
      mutual = invited & invitations
      { mutual:      mutual ,
        invited:     invited - mutual,
        invitations: invitations - mutual}
    end
    expose :social_identities do |user, options|
      user.social_identities.reduce({}){|memo,x|memo[x.provider] = x.token; memo}
    end
  end
end
