module InStadium
  class GroupApi < Grape::API
    format :json
    helpers AuthenticationHelpers
    helpers UserHelpers

    before { restrict_access_to_developers }

    desc "End-points for the currently authenticated user."
    params do
      requires :token, type: String, desc: "Auth token"
    end
    namespace :groups do
      before { authenticate! }

      desc "Group Data"
      params do
        requires :ids, type: String, desc: 'Array of group ids in JSON format.'
      end
      get do
        groups = Group.where('id IN (:ids)', {ids: JSON.parse(params[:ids])})
        present groups, with: Presenters::GroupPresenter
      end

      desc "Query group invitation requests"
      get :check_request do
        groups = Group.where("#{current_user.id} = ANY (groups.unconfirmed_users)")
        present groups, with: Presenters::GroupPresenter
      end

      desc "Query group memberships"
      get :check_memberships do
        groups = Group.where("#{current_user.id} = ANY (groups.users)")
        present groups, with: Presenters::GroupPresenter
      end

      desc "Accept group invitation requst"
      route_param :id do

        desc "Add user to a group"
        params do
          requires :user_ids, type: String, desc: "The id of the user to be added to the group"
        end
        post :users do
          group = Group.find_by_id params[:id]
          if group.present?
            if group.users.include? current_user.id
              confirmable_user_ids = JSON.parse(params[:user_ids]) - group.users
              users_to_add = User.where('id IN (:ids)', {ids: confirmable_user_ids}).pluck(:id)
              if users_to_add.present?
                group.unconfirmed_users = [group.unconfirmed_users, users_to_add].flatten.uniq.compact
                group.save
                present group, with: Presenters::GroupPresenter
              else
                status 400
              end
            else
              status 401
            end
          else
            status 400
          end
        end

        desc "Delete user from a group"
        params do
          requires :user_ids, desc: "The id of the user to be deleted either from the users or the unconfirmed users"
        end
        delete :users do
          group = Group.find_by_id params[:id]
          if group.present?
            if group.users.present? && group.users.include?(current_user.id) || group.unconfirmed_users.present? && group.unconfirmed_users.include?( current_user.id)
              removable_user_ids = JSON.parse(params[:user_ids])
              users_to_remove = User.where('id IN (:ids)', {ids: removable_user_ids}).pluck(:id)

              group.users = group.users - users_to_remove if group.users.present?
              group.unconfirmed_users = group.unconfirmed_users - users_to_remove if group.unconfirmed_users.present?

              group.save
              present group, with: Presenters::GroupPresenter
            else
              status 401
            end
          else
            status 400
          end
        end

        post :accept_request do
          group = Group.find params[:id]
          users = group.users || []
          group.users = users + [current_user.id]
          unconfirmed_ones = group.unconfirmed_users - [current_user.id]
          group.unconfirmed_users = unconfirmed_ones
          group.save
          present group, with: Presenters::GroupPresenter
        end
      end

      desc "Create a group"
      params do
        optional :name, type: String, desc: 'Name of a group'
        optional :description, type: String, desc: 'Description of a group'
        requires :user_ids, type: String, desc: 'An array of users separated. JSON format.'
      end
      post do
        creation_params = {users: [current_user.id]}
        creation_params.merge!({description: params[:description]}) if params[:description].present?
        creation_params.merge!({name: params[:name]}) if params[:name].present?
        unconfirmed = []
        if params[:user_ids].present?
          unconfirmed = JSON.parse(params[:user_ids]).map{|x|x.to_i}.uniq - [current_user.id]
        end
        creation_params.merge!({unconfirmed_users: unconfirmed})

        group = Group.create creation_params
        present group, with: Presenters::GroupPresenter
      end
    end
  end
end