module InStadium
  class UserDataApi < Grape::API
    format :json
    prefix 'api'

    helpers AuthenticationHelpers
    before { restrict_access_to_developers }

    desc "Retrieve user data object for a given key"
    params do
      requires :key, type: String, desc: "key for the user data"
    end
    get :user_data do
      @user_data = UserData.find_by_key(params[:key])
      present @user_data.value
    end
  end
end