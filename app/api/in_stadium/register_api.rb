module InStadium
  class RegisterApi < Grape::API
    helpers AuthenticationMethods

    format :json
    prefix 'api'

    helpers AuthenticationHelpers
    before { restrict_access_to_developers }

    desc "Registering a new user with an email address. The response code will be a 400 if the registration can't be processed."
    params do
      requires :email, type: String, desc: "Email for login"
      requires :password, type: String, desc: "Password for login"
      optional :first_name, type: String, desc: "User first name"
      optional :last_name, type: String, desc: "User last name"
      optional :avatar, desc: "JPG, PNG, or GIF file that will serve as the user’s avatar image and uploaded in a multipart-form"
    end
    post 'register/email' do
      process_registration
    end

  end
end
