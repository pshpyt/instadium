module InStadium
  class UsersApi < Grape::API
    format :json
    prefix 'api'

    helpers AuthenticationHelpers
    helpers UserHelpers
    before { restrict_access_to_developers }

    resources :users do
      route_param :id do
        desc "Resend confirmation email."
        post "resend_confirmation" do
          user = User.find_by_id params[:id]
          if user.present?
            if user.confirmed?
              status 403
            else
              user.send_confirmation_instructions
              status 200
            end
          else
            status 404
          end
        end

        desc "Get the detailed view of this user"
        get "" do
          @user = get_user_for_guid_or_me(params[:id])
          present @user,
                  with: Presenters::DetailedUserPresenter,
                  current_user: current_user
        end
      end
    end
  end
end