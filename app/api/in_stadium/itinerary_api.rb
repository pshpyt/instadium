module InStadium
  class ItineraryApi < Grape::API
    format :json
    helpers AuthenticationHelpers
    helpers UserHelpers

    before { restrict_access_to_developers }

    desc "End-points for the currently authenticated user."
    params do
      requires :token, type: String, desc: "Auth token"
    end
    namespace :itinerary do
      before { authenticate! }

      desc "Create Itinerary"
      params do
        requires :product_id, desc: "The id of a product"
        optional :start_time, desc: "products start time"
        optional :end_time, desc: "products end time"
      end
      post '' do
        product = Product.find params[:product_id]
        CreateItineraryService.new(current_user, product, params[:start_time], params[:end_time]).execute
      end

      desc "Delete Itinerary"
      params do
        requires :product_id, desc: "The id of a product"
      end
      delete '' do
        items = current_user.itinerary_items.where(product_id: params[:product_id])
        if items.present?
          current_user.itinerary_items.where(product_id: params[:product_id]).destroy_all
        else
          status 404
        end
      end

      desc "Itinerary retrieval"
      params do
        requires :users, desc: "The ids of a users (comma separated)"
      end
      get '' do
        JSON.parse(params[:users]).map do |id|
          { user_id: id,
            products: User.find(id).itinerary_items.map do |itinerary|
              {
                product_id: itinerary.product_id,
                start_time: itinerary.starttime,
                end_time: itinerary.endtime
              }
            end
          }
        end
      end
    end
  end
end