module InStadium
  class UserLocationApi < Grape::API
    format :json
    helpers AuthenticationHelpers
    helpers UserHelpers

    before { restrict_access_to_developers }
    before { authenticate! }

    desc "End-points for the currently authenticated user."
    params do
      requires :token, type: String, desc: "Auth token"
      requires :latitude, type: Float, desc: "The latitude of a user"
      requires :longitude, type: Float, desc: "The longitude of a user"
    end
    post 'userlocation' do
      current_user.latitude = params[:latitude]
      current_user.longitude = params[:longitude]
      current_user.location_last_update_time = Time.now
      current_user.save
    end
  end
end
