module InStadium
  class FriendsApi < Grape::API
    format :json
    helpers AuthenticationHelpers
    helpers UserHelpers

    before { restrict_access_to_developers }
    before { authenticate! }

    desc "End-points for the currently authenticated user."
    params do
      requires :token, type: String, desc: "Auth token"
      optional :emails, type: String, desc: "A comma separated list of emails to check if they are friends or not."
    end
    get 'friends' do
      friends = []

      fb_friends = current_user.facebook_identities.present? ? GetFacebookFriends.new( current_user.facebook_identities.first.token).execute : []
      g_friends = current_user.google_oauth2_identities.present? ? GetGoogleFriends.new( current_user.google_oauth2_identities.first.token).execute : []
      email_friends = params[:emails].present? ? JSON.parse(params[:emails]).map{|x| User.find_by_email x}.compact : []

      friends_in_the_system = current_user.following_by_type('User')

      fb_friends_present_in_the_system = fb_friends.map{|x| User.find_by_social_identity_uid :facebook, x['id']}.compact
      g_friends_present_in_the_system = g_friends.map{|x| User.find_by_social_identity_uid :google_oauth2, x['id']}.compact

      friend_ids =  (friends_in_the_system.map(&:id) & fb_friends_present_in_the_system.map(&:id)) +
                    (friends_in_the_system.map(&:id) & g_friends_present_in_the_system.map(&:id)) +
                    (friends_in_the_system.map(&:id) & email_friends.map(&:id))

      friend_ids.map{|x|User.find x}.map do |x|
        {
          user_id: x.id,
          avatar_url: x.avatar_url,
          provider: 'system user',
          first_name: x.first_name,
          last_name: x.last_name
        }
      end
    end
  end
end
