require 'securerandom'

module InStadium
  class MeApi < Grape::API
    format :json
    prefix 'api'

    helpers AuthenticationHelpers
    helpers UserHelpers
    before { restrict_access_to_developers }
    
    desc "End-points for the currently authenticated user. The gender is encoded as described here: http://en.wikipedia.org/wiki/ISO_5218"
    params do
      requires :token, type: String, desc: "Auth token"
    end
    namespace :me do
      before { authenticate! }

      desc "User information"
      get do
        present current_user,
          with: Presenters::DetailedUserPresenter
      end

      desc "Update password. This is deprecated!. Please use the update."
      params do
        requires :new_password, desc: "New password"
      end
      post :set_password do
        current_user.password = params[:new_password]
        current_user.save!
        status 200
        present current_user,
          with: Presenters::DetailedUserPresenter
      end

      desc "Invite/accept friend. If I have an invitation from a User then it is going to be accepted otherwise I will invite the specified user and he needs to accept me."
      params do
        requires :friend, desc: "The id or email of a friend. If no user with the specified email is found then an unconfirmed user is created."
      end
      post "friend" do
        friend_identifier = params[:friend]
        potential_friend = User.find_by_id(friend_identifier) || User.find_by_email(friend_identifier)
        if potential_friend
          current_user.follow potential_friend
          present current_user,
            with: Presenters::DetailedUserPresenter
        elsif friend_identifier.include?('@') && friend_identifier.include?('.')
          pwd = SecureRandom.hex(8)
          unconfirmed_friend = User.create email: friend_identifier, username: friend_identifier.split('@').first, password: 'testPwd001', password_confirmation: 'testPwd001'
          current_user.follow unconfirmed_friend
        else
          status 404
        end
      end

      desc "Destroy friendship. If I have a mutual friendship then it will be one sided. If it is only an invitation then the invitation will be revoked."
      params do
        requires :friend_id, desc: "The id of a friend"
      end
      delete "unfriend" do
        potential_friend = User.find_by_id params[:friend_id]
        if potential_friend
          current_user.stop_following potential_friend
          present current_user,
            with: Presenters::DetailedUserPresenter
        else
          status 404
        end
      end

      desc "Update ZIP. This is deprecated!. Please use the update."
      params do
        requires :zip, desc: "ZIP code for the user"
      end
      post :set_zip do
        current_user.zip = params[:zip]
        current_user.save!
        status 200
        
        present current_user,
          with: Presenters::DetailedUserPresenter
      end

      desc "Update DOB. This is deprecated!. Please use the update."
      params do
        requires :dob, desc: "Date of birth format YYYY/MM/DD"
      end
      post :set_dob do
        current_user.dob = params[:dob]
        current_user.save!
        status 200
        
        present current_user,
          with: Presenters::DetailedUserPresenter
      end

      desc 'Update user. The gender is encoded as described here: http://en.wikipedia.org/wiki/ISO_5218"
'
      params do
        optional :dob, desc: "Date of birth format YYYY/MM/DD"
        optional :zip, desc: "Zip code for the user"
        optional :new_password, desc: "New password"
        optional :name, desc: "Name"
        optional :username, desc: "username"
        optional :bio, desc: "bio"
        optional :first_name, desc: "First name"
        optional :last_name, desc: "Last name"
        optional :display_location, desc: "Display Location"
        optional :gender, desc: "Gender. example:male, female"
        optional :phone_number, desc: "Phone number"
        optional :registrationConfirmed, desc: "Phone number"
      end
      post :update do
        optional_params = [:dob,
                :zip,
                :new_password,
                :name,
                :username,
                :bio,
                :first_name,
                :last_name,
                :display_location,
                :gender,
                :phone_number]

        optional_params.each do |e|
          current_user.send("#{e}=", params[e]) if params[e].present?
        end

        if current_user.save
          status 200
        else
          status 404
        end
        
        present current_user,
          with: Presenters::DetailedUserPresenter
      end

      desc "Update avatar."
      params do
        requires :use_social_identity_avatar, desc: "Use FB avatar"
        optional :avatar, desc: "New user avatar"
      end
      post :avatar do
        if ["true", "yes"].include?(params[:use_social_identity_avatar].strip.downcase)
          current_user.use_social_identity_avatar = true
        elsif params[:avatar].present?
          current_user.use_social_identity_avatar = false
          current_user.avatar = params[:avatar]
        else
          error!({error_type: :invalid_image}, 400)
        end
        current_user.save!
        status 200
      end
    end
  end
end