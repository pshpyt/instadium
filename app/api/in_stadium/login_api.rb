module InStadium
  class LoginApi < Grape::API
    helpers AuthenticationMethods
    helpers AuthenticationHelpers

    format :json
    prefix 'api'
    before { restrict_access_to_developers }

    desc "End-points for the Login"
    namespace :login do

      desc "Reset password for user"
      params do
        requires :login, type: String, desc: "Login to reset"
      end
      get "forgot" do
        if u = User.find_by_email(params[:login])
          u.send_reset_password_instructions
          status 200
          "The pasword reset instructions are sent it to: '#{u.email}'"
        else
          status 404
          nil
        end
      end

      desc "Checking for an existing account w/email address"
      params do
        requires :login, type: String, desc: "Email or login"
      end
      get 'email' do
        login = params[:login]
        u = User.where(:email => login).first
        if u.nil?
          status 404
          'User can not be authenticated'
        else
          status 200
          u
        end
      end

      desc "Login via username or email and password"
      params do
        requires :login, type: String, desc: "Email or login"
        requires :password, type: String, desc: "Password for login"
      end
      post 'email' do
        process_login_via_password
      end

      desc "Registering/Logging In with Facebook. Authenticate the user, creating a new user account if necessary. Return a 401 if no token is provided or the token is invalid. For a valid token the endpoint will return a 200 and the body will be the 'me' entry for the current user"
      params do
        requires :facebook_token, type: String, desc: "Facebook OAuth token"
        optional :email, type: String, desc: "Facebook linked email address (can be provided via token in some cases but not all)"
        optional :token, type: String, desc: "Previously assigned API token. If provided we will link the user accounts."
      end
      post 'facebook' do
        process_omni_auth_login(:facebook, params[:email])
      end

      desc "Unlink Facebook for the current logged in user."
      delete 'facebook' do
        authenticate!
        error!({ :error_type => "no_linked_facebook_account" }, 404) unless current_user.the_identity(:facebook).present?
        if current_user.the_identity(:facebook).destroy
          status 200
          nil
        else
          status 400
          nil
        end
      end

      desc "Registering/Logging In with Google. Authenticate the user, creating a new user account if necessary. Return a 401 if no token is provided or the token is invalid. For a valid token the endpoint will return a 200 and the body will be the 'me' entry for the current user"
      params do
        requires :google_oauth2_token, type: String, desc: "Google OAuth token"
        optional :email, type: String, desc: "Google email address (can be provided via token in some cases but not all)"
        optional :token, type: String, desc: "Previously assigned API token. If provided we will link the user accounts."
      end
      post 'google' do
        process_omni_auth_login(:google_oauth2, params[:email])
      end

      desc "Registering/Logging In with Twitter. Authenticate the user, creating a new user account if necessary. Return a 401 if no token is provided or the token is invalid. For a valid token the endpoint will return a 200 and the body will be the 'me' entry for the current user"
      params do
        requires :twitter_token, type: String, desc: "Twitter OAuth token"
        requires :twitter_secret, type: String, desc: "Twitter OAuth token"
        requires :email, type: String, desc: "Unfortunatelly twitter doesn'not retrieves our email."
        optional :token, type: String, desc: "Previously assigned API token. If provided we will link the user accounts."
      end
      post 'twitter' do
        process_omni_auth_login(:twitter, params[:email])
      end

      desc "Unlink Twitter for the current logged in user."
      params do
        requires :token, type: String, desc: "The authentication token"
      end
      delete 'twitter' do
        authenticate!
        error!({ :error_type => "no_linked_facebook_account" }, 404) unless current_user.the_identity(:twitter).present?
        if current_user.the_identity(:twitter).destroy
          status 200
          nil
        else
          status 400
          nil
        end
      end

      desc "Temporarily this does not work"
      params do
        requires :instagram_token, type: String, desc: "Instagram OAuth token"
        optional :email, type: String, desc: "Instagram email address (can be provided via token in some cases but not all)"
      end
      post 'instagram' do
        process_omni_auth_login(:instagram, params[:email])
      end

      desc "Unlink Google for the current logged in user."
      delete 'google' do
        authenticate!
        error!({ :error_type => "no_linked_google_account" }, 404) unless current_user.the_identity(:google_oauth2).present?
        if current_user.the_identity(:google_oauth2).destroy
          status 200
          nil
        else
          status 400
          nil
        end
      end

    end
  end
end
