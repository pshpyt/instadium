module InStadium
  class MessagesApi < Grape::API
    format :json
    helpers AuthenticationHelpers
    helpers UserHelpers

    before { restrict_access_to_developers }

    desc "End-points for the currently authenticated user."
    params do
      requires :token, type: String, desc: "Auth token"
    end
    namespace :messages do
      before { authenticate! }

      desc "Create Message"
      params do
        requires :group_id, desc: "The id of a group"
        requires :message, desc: "The message itselt"
        optional :media, desc: "Image", type: :file
        optional :timelimit, desc: "Timelimit for image view in seconds", type: Integer
      end
      post do
        group = Group.find params[:group_id]
        if group.users.include? current_user.id
          group.messages.create message: params[:message], timelimit: params[:timelimit], media: params[:media]
        else
          status 401
        end
      end

      desc "Retrieve message thread"
      params do
        requires :group_id, desc: "The id of a group"
        optional :count, desc: "The number of messages", default: 50
        optional :offset, desc: "The n-th message"
      end
      get do
        group = Group.find params[:group_id]
        if group.users.include? current_user.id
          rel = group.messages.order(created_at: :asc)
          rel = rel.limit(params[:count]).offset(params[:offset]) if params[:offset].present?
          rel
        else
          status 401
        end
      end

    end
  end
end
