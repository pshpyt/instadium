module InStadium
  class ProductsApi < Grape::API
    format :json
    prefix 'api'

    namespace :feed do
      helpers AuthenticationHelpers
      helpers UserHelpers
      before { restrict_access_to_developers }
      
      desc "End-points for the currently available products"
      namespace :products do

        desc "Product information"
        params do
          optional :page, type: Integer, desc: "The page number for pagination"
          optional :per_page, type: Integer, desc: "The number of items displayed per page. Only used if 'page' is present. The default is 10."
          optional :sort_by, type: String, desc: "The sorting column"
          optional :sort_direction, type: String, desc: "Could be ASC or DESC. Only used if 'sort_by' is present. The default is DESC."
          optional :sort_by_1, type: String, desc: "The sorting column"
          optional :sort_direction_1, type: String, desc: "Could be ASC or DESC. Only used if 'sort_by' is present. The default is DESC."
          optional :sort_by_2, type: String, desc: "The sorting column"
          optional :sort_direction_2, type: String, desc: "Could be ASC or DESC. Only used if 'sort_by' is present. The default is DESC."
          optional :sort_by_3, type: String, desc: "The sorting column"
          optional :sort_direction_3, type: String, desc: "Could be ASC or DESC. Only used if 'sort_by' is present. The default is DESC."
          optional :sort_by_4, type: String, desc: "The sorting column"
          optional :sort_direction_4, type: String, desc: "Could be ASC or DESC. Only used if 'sort_by' is present. The default is DESC."
          optional :product_ids, type: String, desc: 'JSON array of ids'
        end
        get do
          begin
            relation = Product.actual
            relation = relation.where('id IN (:ids)', {ids: JSON.parse(params[:product_ids])}) if params[:product_ids].present?
            [[:sort_by, :sort_direction],[:sort_by_1, :sort_direction_1],[:sort_by_2, :sort_direction_2],[:sort_by_3, :sort_direction_3],[:sort_by_4, :sort_direction_4]].reverse.each do |sort|
              sort_column = params[sort[0]]
              sort_direction = params[sort[1]]
              if sort_column.present? &&
                 Product.columns.map{|x|x.name}.include?(sort_column)
                direction = ['ASC', 'DESC'].include?(sort_direction) ? sort_direction : 'DESC'
                relation = relation.order("#{sort_column} #{direction}")
              end
            end
            if params[:page].present?
              relation = relation.paginate(:page => params[:page], :per_page => params[:per_page] || 10)
            end
            present relation, with: Presenters::ProductPresenter
          rescue
          end
        end
      end
    end

  end
end