module InStadium
  class PopularProductsApi < Grape::API
    format :json
    helpers AuthenticationHelpers

    before { restrict_access_to_developers }

    desc "End-points for retrieving the most popular products."
    get 'popular_products' do
      present Product.actual.order(usage: :desc), with: Presenters::ProductPresenter
    end
  end
end