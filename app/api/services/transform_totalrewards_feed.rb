module Services
  class TransformTotalrewardsFeed
    attr_reader :json

    def initialize(json)
      @json = json
    end

    def transform(root)
      common_fields = ['brand','latitude','longitude','entityPath','market','marketRank','name','title','type','propCode','propertyRank','massRelevanceStreamID','massRelevanceHashTags','shortDescription','id']

      json[root].map do |x|
        x = x.numericalize_values.booleanize_values.arrayize_series
        details = x.except(*common_fields)
        details = DetailsTransformer.transform(x.except(*common_fields)).underscore_values.underscore_keys.remove_tags
        {
          brand:         x['brand'],
          coordinates: {
            latitude:      x['latitude'].to_f,
            longitude:     x['longitude'].to_f
          },
          entity_path:   x['entityPath'],
          market:        x['market'],
          market_rank:   x['marketRank'],
          name:          x['name'] || x['title'],
          category: if root == 'thingstodo'
            x['type']
          elsif root == 'event'
            'event'
          else root == 'restaurant'
            'restaurant'
          end,
          property_code: x['propCode'],
          property_rank: x['propertyRank'],
          mass_relevance: {
            stream_id: x['massRelevanceStreamID'],
            hash_tags: x['massRelevanceHashTags']
          },
          short_description: x['shortDescription'],
          feed_id:       x['id'],
          details:       details
        }
      end
    end

  end
end