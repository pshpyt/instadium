module Services
  class ProductLoader
    attr_reader :data

    def initialize(data = {})
      @data = data
    end

    def load_data
      data.each do |row|
        updatable = Product.find_by_feed_id row[:feed_id]
        if updatable
          updatable.update_attributes row.merge({expired: false})
        else
          Product.create row.merge({expired: false})
        end
      end
    end
  end
end
