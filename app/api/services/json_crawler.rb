require "rubygems"
require "json"
require "net/http"
require "uri"
require "cgi"

def unescape_value(value)
  unescaped_value = if value.is_a? Array
    value.map {|x| unescape_value(x) }
  elsif value.is_a? String
    coder = HTMLEntities.new
    coder.decode(CGI.unescapeHTML( value))
  elsif value.is_a? Hash
    unescape_hash(value)
  else
    value
  end
  unescaped_value
end

def unescape_hash(hash)
  transform_hash(hash, deep: true) do |h, key, value|
    h[key] = unescape_value(value)
  end
end

module Services
  class JsonCrawler

    attr_reader :url

    def initialize(url)
      @url = url
    end

    def json
      uri = URI.parse(url)
      http = Net::HTTP.new(uri.host, uri.port)
      request = Net::HTTP::Get.new(uri.request_uri)
      response = http.request(request)
       
      json = if response.code == "200"
        json = JSON.parse(response.body)
        unescape_hash(json)
      else
        {}
      end
    end

  end
end