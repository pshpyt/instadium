module Etl
  module EtlCore
    def run
      begin
        data = extract
        transformed = transform data
        load transformed
      rescue
        Rails.logger.debug "An error occured during the ETL process"
        Rails.logger.debug $!
        Rails.logger.debug $!.backtrace
      end
    end
  end
end
