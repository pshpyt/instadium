module Etl
  class FeedCrawlerEtl
    include Etl::EtlCore

    attr_reader :url, :type

    def initialize url, type
      @url = url
      @type = type
    end

    def extract
      Services::JsonCrawler.new(url).json
    end

    def transform(data)
      Services::TransformTotalrewardsFeed.new(data).transform(type)
    end

    def load(data)
      Services::ProductLoader.new(data).load_data
    end

  end
end