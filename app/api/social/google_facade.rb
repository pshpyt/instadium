require 'google/api_client'

module Social
  class GoogleFacade
    def self.get_identity_data(oauth_access_token)
      #TODO
       # token="ya29.1.AADtN_UfKlbgaToLdzWX40n-VtU6ATuQLKrfcILsMS0QC02s-doxcuPjvLqtaw"
        
        client = Google::APIClient.new
        plus = client.discovered_api('plus')

        # Initialize OAuth 2.0 client
        client.authorization.client_id = Devise.omniauth_configs[:google_oauth2].args[0]
        client.authorization.client_secret = Devise.omniauth_configs[:google_oauth2].args[1]
        client.authorization.scope = 'https://www.googleapis.com/auth/plus.me'
        client.authorization.access_token = oauth_access_token

        # Make an API call.
        result = client.execute(
          :api_method => plus.people.get,
          :parameters => {'collection' => 'public', 'userId' => 'me'}
        )

        JSON.parse(result.response.body)
    rescue
      nil
    end
  end
end
