module Social
  class FacebookFacade

    def self.get_identity_data(oauth_access_token)
      @graph = Koala::Facebook::API.new(oauth_access_token)
      @graph.get_object("me")
    rescue ::Koala::KoalaError => apie
      nil
    end

    def self.publish_to_feed(oauth_access_token, message = nil, image_uri = nil)
      @graph = Koala::Facebook::API.new(oauth_access_token)
      feed_item_hash = {}
      feed_item_hash[:message] = message unless message.nil?
      feed_item_hash[:image] = image_uri unless image_uri.nil?
      retval = @graph.put_connections("me", "feed", feed_item_hash)
      retval.nil? ? nil : retval["id"]
    rescue ::Koala::KoalaError => apie
      nil
    end

    def self.get_friend_uids(oauth_access_token)
      @graph = Koala::Facebook::API.new(oauth_access_token)
      @graph.get_connections("me", "friends").map { |f| f["id"] }
    rescue ::Koala::KoalaError => apie
      []
    end
  end
end
