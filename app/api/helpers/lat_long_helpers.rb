module LatLongHelpers
  LAT_LONG_REGEXP = /(-?\d{1,3}\.?\d{0,8}),(-?\d{1,3}\.?\d{0,8})/

  def extract_lat_long
    location_param = params[:latlng]
    if !location_param.blank?
      matchers = LAT_LONG_REGEXP.match(location_param)
      unless matchers.nil? || matchers.size < 2
        @latitude = matchers[1]
        @longitude = matchers[2]
      end
    end
  rescue
    @latitude = nil
    @longitude = nil
  end

  def latitude
    @latitude
  end

  def longitude
    @longitude
  end

end
