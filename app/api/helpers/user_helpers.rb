module UserHelpers

  def get_user_for_guid_or_me(guid)
    if signed_in? && (guid == 'me')
      current_user
    else
      User.find_by_id!(guid)
    end
  end

end
