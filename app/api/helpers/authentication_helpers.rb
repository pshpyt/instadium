module AuthenticationHelpers
  include ActionController::HttpAuthentication::Token::ControllerMethods
  TOKEN_PARAM_NAME = :token

  def restrict_access_to_developers
    header_token = headers['Authorization']
    key = ApiKey.where('token = :token', {token: header_token}).
          where('(expires_at IS NULL) OR ((expires_at IS NOT NULL) AND (expires_at > :time))', {time:Time.now})
    error!({ "error_type" => "please aquire a developer key" }, 401) unless key.present?
  end

  def current_user
    @current_user ||= AuthenticationToken.find_user_by_token_value(token_value_from_request)
  end

  def signed_in?
    !!current_user
  end

  def token_value_from_request(token_param = TOKEN_PARAM_NAME)
    params[token_param]
  end

  def authenticate!
    error!({ "error_type" => "authentication_error" }, 401) unless signed_in?
  end

  def process_login_via_password
    token = User.login_with_email_or_username_and_password(params[:login], params[:password])
    if token
      status 200
      present token.user, :with => Presenters::DetailedUserWithTokenPresenter
    else
      u = User.where(:email => params[:login]).first || User.where(:username => params[:login]).first
      error_type = u.nil? ? "no_user" : "bad_password"
      error!({ "error_type" => error_type }, 401)
    end
  end

  def valid_registration_params?
    !(params[:email].nil? || params[:email].blank?  ||
      params[:password].nil?   || params[:password].blank?)
  end

  def process_registration
    error!({ "error_type" => "missing_required_parameters" }, 400) unless valid_registration_params?
    new_user, error = User.create_user_from_params(params)
    if new_user
      token = new_user.get_authentication_token
      status 201
      present new_user, :with => Presenters::DetailedUserWithTokenPresenter
    elsif error
      if error =~ /Email has already been taken/
        error!({ "error_type" => "account_exists" }, 400)
      else
        error!({ "error_type" => error }, 400)
      end
    else
      user_already_exists = User.where(:email => params[:email]).exists?
      if user_already_exists
        error!({ "error_type" => "account_exists" }, 400)
      else
        error!({ "error_type" => "missing_required_parameters" }, 400)
      end
    end
  end

end
