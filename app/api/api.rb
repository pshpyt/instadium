class API < Grape::API
  prefix 'api'

  rescue_from ActiveRecord::RecordNotFound do |e|
    rack_response({ error_type: "not_found" }.to_json, 404)
  end

  rescue_from Grape::Exceptions::ValidationErrors do |e|
    rack_response({
      status: e.status,
      message: e.message,
      errors: e.errors
    #TODO this should be 400
    }.to_json, 500)
  end

  rescue_from :all do |e|
    binding.pry
    Rails.logger.error e.message
    Rails.logger.error e.backtrace
    rack_response({ error_type: "invalid_request" }.to_json, 500)
  end

  mount InStadium::Ping
  mount InStadium::PingProtected
  mount InStadium::LoginApi
  mount InStadium::RegisterApi
  mount InStadium::MeApi
  mount InStadium::GroupApi
  mount InStadium::UsersApi
  mount InStadium::UserDataApi
  mount InStadium::ProductsApi
  mount InStadium::ItineraryApi
  mount InStadium::UserLocationApi
  mount InStadium::MessagesApi
  mount InStadium::FriendsApi
  mount InStadium::PopularProductsApi

  add_swagger_documentation
end
