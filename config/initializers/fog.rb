AWS_ACCESS_KEY_ID = ENV['AWS_ACCESS_KEY_ID'] || 'AKIAJ6LBNBEW24U4FJDQ'
AWS_SEC_ACCESS_KEY = ENV['AWS_SEC_ACCESS_KEY'] || '5vFgVFPRasdURrgcUEcbhz86FcHhzmI/15/Kx7cx'
BUCKET_ENV_SUFFIX = ENV['BUCKET_ENV_SUFFIX'] || 'test'


BUCKET_ENV_CLDFRNT = ENV['BUCKET_ENV_CLDFRNT'] 

bucket_name = "instadium-images-#{BUCKET_ENV_SUFFIX}"

CarrierWave.configure do |config|
  config.fog_credentials = {
    :provider               => 'AWS',                                  # required
    :aws_access_key_id      => AWS_ACCESS_KEY_ID,                             # required
    :aws_secret_access_key  => AWS_SEC_ACCESS_KEY,                             # required
  }

  #needed for site map
  config.storage = :fog

  if BUCKET_ENV_CLDFRNT.present? #only use CDN when set
    config.asset_host     = BUCKET_ENV_CLDFRNT #'https://d3c6unn11937mj.cloudfront.net'
  end
  config.fog_directory  = bucket_name                              # required
  config.fog_public     = true                                     # optional, defaults to true
  config.fog_attributes = {'Cache-Control'=>'max-age=315576000'}   # optional, defaults to {}
end

#sitemap
SITEMAP_HOST_URL =  ENV['BUCKET_ENV_CLDFRNT']  || "https://s3.amazonaws.com/instadium-images-#{BUCKET_ENV_SUFFIX}"
