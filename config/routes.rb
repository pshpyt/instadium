Instadium::Application.routes.draw do
  namespace :admin do
    get 'dashboard/index'
    root :to => "results#index"
    resources :results
    resources :trivia_configs
    resources :trivia
    match "delete_image", :to => "trivia_configs#delete_image", via: [:get, :post]
  end




  root :to => "home#index"
  match "answers", :to => "home#answers", via: [:get, :post]
  get "home/text", :to => "home#text"
  devise_for :users, :controllers => { :confirmations => "confirmations", :registrations => "users/registrations", omniauth_callbacks: "users/omniauth_callbacks" }
  resources :users

  match 'auth/:provider/callback', to: 'sessions#create', via: [:get, :post]
  match 'auth/failure', to: redirect('/'), via: [:get, :post]
  match 'signout', to: 'sessions#destroy', as: 'signout', via: [:get, :post]

  mount Rapidfire::Engine => "/survey"

  mount API => '/'

end
