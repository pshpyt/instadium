# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140710230230) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "api_keys", force: true do |t|
    t.string   "token"
    t.datetime "expires_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "authentication_tokens", force: true do |t|
    t.string   "token"
    t.integer  "user_id"
    t.datetime "expires_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "authentication_tokens", ["token"], name: "index_authentication_tokens_on_token", unique: true, using: :btree
  add_index "authentication_tokens", ["user_id", "expires_at"], name: "index_authentication_tokens_on_user_id_and_expires_at", using: :btree

  create_table "follows", force: true do |t|
    t.integer  "followable_id",                   null: false
    t.string   "followable_type",                 null: false
    t.integer  "follower_id",                     null: false
    t.string   "follower_type",                   null: false
    t.boolean  "blocked",         default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "follows", ["followable_id", "followable_type"], name: "fk_followables", using: :btree
  add_index "follows", ["follower_id", "follower_type"], name: "fk_follows", using: :btree

  create_table "groups", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "users",             array: true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "unconfirmed_users", array: true
  end

  create_table "itinerary_items", force: true do |t|
    t.integer  "user_id"
    t.integer  "product_id"
    t.datetime "starttime"
    t.datetime "endtime"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "itinerary_items", ["product_id"], name: "index_itinerary_items_on_product_id", using: :btree
  add_index "itinerary_items", ["user_id", "product_id"], name: "index_itinerary_items_on_user_id_and_product_id", using: :btree
  add_index "itinerary_items", ["user_id"], name: "index_itinerary_items_on_user_id", using: :btree

  create_table "messages", force: true do |t|
    t.string   "message"
    t.integer  "group_id"
    t.string   "media"
    t.integer  "timelimit"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "messages", ["group_id"], name: "index_messages_on_group_id", using: :btree

  create_table "products", force: true do |t|
    t.string   "brand"
    t.string   "entity_path"
    t.string   "market"
    t.integer  "market_rank"
    t.string   "name"
    t.string   "property_code"
    t.integer  "property_rank"
    t.string   "category"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "feed_id"
    t.boolean  "expired"
    t.hstore   "coordinates"
    t.hstore   "mass_relevance"
    t.text     "short_description"
    t.hstore   "details"
    t.integer  "usage",             default: 0
  end

  add_index "products", ["category"], name: "index_products_on_category", using: :btree
  add_index "products", ["expired"], name: "index_products_on_expired", using: :btree
  add_index "products", ["feed_id"], name: "index_products_on_feed_id", unique: true, using: :btree

  create_table "rapidfire_answer_groups", force: true do |t|
    t.integer  "question_group_id"
    t.integer  "user_id"
    t.string   "user_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rapidfire_answer_groups", ["question_group_id"], name: "index_rapidfire_answer_groups_on_question_group_id", using: :btree
  add_index "rapidfire_answer_groups", ["user_id", "user_type"], name: "index_rapidfire_answer_groups_on_user_id_and_user_type", using: :btree

  create_table "rapidfire_answers", force: true do |t|
    t.integer  "answer_group_id"
    t.integer  "question_id"
    t.text     "answer_text"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rapidfire_answers", ["answer_group_id"], name: "index_rapidfire_answers_on_answer_group_id", using: :btree
  add_index "rapidfire_answers", ["question_id"], name: "index_rapidfire_answers_on_question_id", using: :btree

  create_table "rapidfire_question_groups", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "rapidfire_questions", force: true do |t|
    t.integer  "question_group_id"
    t.string   "type"
    t.string   "question_text"
    t.integer  "position"
    t.text     "answer_options"
    t.text     "validation_rules"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rapidfire_questions", ["question_group_id"], name: "index_rapidfire_questions_on_question_group_id", using: :btree

  create_table "results", force: true do |t|
    t.string   "ip"
    t.string   "url"
    t.string   "right_answers",   array: true
    t.datetime "started_at"
    t.datetime "finished_at"
    t.integer  "score"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "user_code"
    t.string   "originator_code"
    t.string   "source"
  end

  create_table "roles", force: true do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "social_identities", force: true do |t|
    t.integer  "user_id",           null: false
    t.string   "provider"
    t.string   "uid"
    t.string   "provider_username"
    t.string   "token"
    t.string   "secret"
    t.datetime "last_verified_at"
    t.datetime "expires_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_url"
  end

  add_index "social_identities", ["provider", "uid"], name: "index_social_identities_on_provider_and_uid", using: :btree
  add_index "social_identities", ["token"], name: "index_social_identities_on_token", using: :btree
  add_index "social_identities", ["user_id", "provider"], name: "index_social_identities_on_user_id_and_provider", using: :btree

  create_table "trivia", force: true do |t|
    t.string   "title"
    t.string   "url"
    t.integer  "question_number"
    t.string   "question"
    t.string   "answers",          array: true
    t.string   "answer"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "trivia_config_id"
  end

  create_table "trivia_configs", force: true do |t|
    t.string   "url"
    t.string   "presented_image"
    t.string   "ad_image"
    t.string   "share_message"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "trivia_number"
  end

  create_table "user_data", force: true do |t|
    t.string   "key"
    t.hstore   "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                      default: "",    null: false
    t.string   "encrypted_password",         default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",              default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "username"
    t.string   "full_name"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.datetime "unconfirmed_email"
    t.string   "latitude"
    t.string   "longitude"
    t.string   "avatar"
    t.boolean  "use_social_identity_avatar", default: false
    t.string   "gender"
    t.text     "bio"
    t.datetime "location_last_update_time"
    t.boolean  "disabled"
    t.string   "zip"
    t.datetime "dob"
    t.boolean  "agree_on_email_marketing"
    t.string   "phone_number"
    t.string   "display_location"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users_roles", id: false, force: true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

end
