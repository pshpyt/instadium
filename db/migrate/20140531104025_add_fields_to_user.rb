class AddFieldsToUser < ActiveRecord::Migration
  def up
    add_column :users, :username, :string
    add_column :users, :full_name, :string
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :confirmation_token, :string
    add_column :users, :confirmed_at, :datetime
    add_column :users, :confirmation_sent_at, :datetime
    add_column :users, :unconfirmed_email, :datetime

# [:provider,
#  :uid,
#  :first_name,
#  :last_name,
#  :location,
#  :description,
#  :phone,
#  :urls,
#  :social_image,
#  :display_name,
#  :display_number,
#  :image,
#  :youtube,
#  :dob,
#  :gender,
#  :school,
#  :city,
#  :home_town,
#  :relationship,
#  :about,
#  :cover_image,
#  :goalie,
#  :username,
#  :full_name,
#  :level,
#  :team_position,
#  :team_year,
#  :school_year,
#  :facebook_handle,
#  :twitter_handle,
#  :vine_handle,
#  :instagram_handle,
#  :featured,
#  :team_id,
#  :team_logo,
#  :team_name,
#  :team_location,
#  :team_year_end,
#  :show_steps,
#  :deleted_at,
#  :custom_url,
#  :image_processing,
#  :cover_image_processing,
#  :social_image_processing,
#  :confirmed_at,
#  :confirmation_token,
#  :confirmation_sent_at,
#  :unconfirmed_email,
#  :status,
#  :use_social_identity_avatar,
#  :zip,
#  :avatar,
#  :agree_on_email_marketing,
#  :cache_followers_count,
#  :cache_followings_count

  end

  def down
    remove_column :users, :username, :string
    remove_column :users, :full_name, :string
    remove_column :users, :first_name, :string
    remove_column :users, :last_name, :string
  end
end
