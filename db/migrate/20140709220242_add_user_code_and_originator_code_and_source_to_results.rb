class AddUserCodeAndOriginatorCodeAndSourceToResults < ActiveRecord::Migration
  def change
    add_column :results, :user_code, :string
    add_column :results, :originator_code, :string
    add_column :results, :source, :string
  end
end
