class AddTiviaNumberToTriviaConfig < ActiveRecord::Migration
  def change
    add_column :trivia_configs, :trivia_number, :integer
  end
end
