class CreateUserData < ActiveRecord::Migration
  def change
    create_table "user_data", force: true do |t|
      t.string   "key"
      t.hstore   "value"
      t.datetime "created_at"
      t.datetime "updated_at"
    end
  end
end
