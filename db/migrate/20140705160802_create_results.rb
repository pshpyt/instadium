class CreateResults < ActiveRecord::Migration
  def change
    enable_extension "hstore"
    create_table :results do |t|
      t.string :ip
      t.string :url
      t.string :right_answers , array: true
      t.datetime :started_at
      t.datetime :finished_at
      t.integer :score

      t.timestamps
    end
  end
end
