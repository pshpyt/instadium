class CreateTrivia < ActiveRecord::Migration
  def change
    enable_extension "hstore"
    
    create_table :trivia do |t|
      t.string :title
      t.string :url
      t.integer :question_number
      t.string :question
      t.string :answers, array: true
      t.string :answer

      t.timestamps
    end
  end
end
