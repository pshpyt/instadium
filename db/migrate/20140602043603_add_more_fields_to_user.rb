class AddMoreFieldsToUser < ActiveRecord::Migration
  def up
    add_column :users, :latitude, :string
    add_column :users, :longitude, :string
    add_column :users, :avatar, :string
    add_column :users, :use_social_identity_avatar, :boolean, default: false
    add_column :users, :gender, :string
    add_column :users, :bio, :text
    add_column :users, :location_last_update_time, :datetime
    add_column :users, :disabled, :boolean
    add_column :users, :zip, :string
    add_column :users, :dob, :datetime
    add_column :users, :agree_on_email_marketing, :boolean
    add_column :users, :phone_number, :string
    add_column :users, :display_location, :string
  end
  def down
    remove_column :users, :latitude, :string
    remove_column :users, :longitude, :string
    remove_column :users, :avatar, :string
    remove_column :users, :use_social_identity_avatar
    remove_column :users, :gender
    remove_column :users, :bio
    remove_column :users, :location_last_update_time
    remove_column :users, :disabled
    remove_column :users, :zip
    remove_column :users, :dob
    remove_column :users, :agree_on_email_marketing
    remove_column :users, :phone_number
    # remove_column :users, :display_location
  end
end
