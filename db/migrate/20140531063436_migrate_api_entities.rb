class MigrateApiEntities < ActiveRecord::Migration
  def change
    enable_extension 'plpgsql'
    enable_extension 'hstore'

    create_table "api_keys", force: true do |t|
      t.string   "token"
      t.datetime "expires_at"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "authentication_tokens", force: true do |t|
      t.string   "token"
      t.integer  "user_id"
      t.datetime "expires_at"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "authentication_tokens", ["token"], :name => "index_authentication_tokens_on_token", :unique => true
    add_index "authentication_tokens", ["user_id", "expires_at"], :name => "index_authentication_tokens_on_user_id_and_expires_at"

    create_table "follows", force: true do |t|
      t.integer  "followable_id",                   null: false
      t.string   "followable_type",                 null: false
      t.integer  "follower_id",                     null: false
      t.string   "follower_type",                   null: false
      t.boolean  "blocked",         default: false, null: false
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "follows", ["followable_id", "followable_type"], :name => "fk_followables"
    add_index "follows", ["follower_id", "follower_type"], :name => "fk_follows"

    create_table "groups", force: true do |t|
      t.string   "name"
      t.text     "description"
      t.integer  "users",             array: true
      t.datetime "created_at"
      t.datetime "updated_at"
      t.integer  "unconfirmed_users", array: true
    end

    create_table "itinerary_items", force: true do |t|
      t.integer  "user_id"
      t.integer  "product_id"
      t.datetime "starttime"
      t.datetime "endtime"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "itinerary_items", ["product_id"], :name => "index_itinerary_items_on_product_id"
    add_index "itinerary_items", ["user_id", "product_id"], :name => "index_itinerary_items_on_user_id_and_product_id"
    add_index "itinerary_items", ["user_id"], :name => "index_itinerary_items_on_user_id"

    create_table "messages", force: true do |t|
      t.string   "message"
      t.integer  "group_id"
      t.string   "media"
      t.integer  "timelimit"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "messages", ["group_id"], :name => "index_messages_on_group_id"

    create_table "products", force: true do |t|
      t.string   "brand"
      t.string   "entity_path"
      t.string   "market"
      t.integer  "market_rank"
      t.string   "name"
      t.string   "property_code"
      t.integer  "property_rank"
      t.string   "category"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   "feed_id"
      t.boolean  "expired"
      t.hstore   "coordinates"
      t.hstore   "mass_relevance"
      t.text     "short_description"
      t.hstore   "details"
      t.integer  "usage",             default: 0
    end

    add_index "products", ["category"], :name => "index_products_on_category"
    add_index "products", ["expired"], :name => "index_products_on_expired"
    add_index "products", ["feed_id"], :name => "index_products_on_feed_id", :unique => true

    # create_table "social_identities", force: true do |t|
    #   t.integer  "user_id",           null: false
    #   t.string   "provider"
    #   t.string   "uid"
    #   t.string   "provider_username"
    #   t.string   "token"
    #   t.string   "secret"
    #   t.datetime "last_verified_at"
    #   t.datetime "expires_at"
    #   t.datetime "created_at"
    #   t.datetime "updated_at"
    #   t.string   "avatar_url"
    # end

    # add_index "social_identities", ["provider", "uid"], :name => "index_social_identities_on_provider_and_uid"
    # add_index "social_identities", ["token"], :name => "index_social_identities_on_token"
    # add_index "social_identities", ["user_id", "provider"], :name => "index_social_identities_on_user_id_and_provider"

    # create_table "user_data", force: true do |t|
    #   t.string   "key"
    #   t.hstore   "value"
    #   t.datetime "created_at"
    #   t.datetime "updated_at"
    # end

    # create_table "users", force: true do |t|
    #   t.string   "email",                      default: "",    null: false
    #   t.string   "encrypted_password",         default: "",    null: false
    #   t.string   "reset_password_token"
    #   t.datetime "reset_password_sent_at"
    #   t.datetime "remember_created_at"
    #   t.integer  "sign_in_count",              default: 0,     null: false
    #   t.datetime "current_sign_in_at"
    #   t.datetime "last_sign_in_at"
    #   t.string   "current_sign_in_ip"
    #   t.string   "last_sign_in_ip"
    #   t.datetime "created_at"
    #   t.datetime "updated_at"
    #   t.string   "name"
    #   t.string   "username"
    #   t.string   "bio"
    #   t.string   "first_name"
    #   t.string   "last_name"
    #   t.string   "full_name"
    #   t.string   "avatar"
    #   t.string   "display_location"
    #   t.string   "use_social_identity_avatar"
    #   t.string   "status"
    #   t.string   "zip"
    #   t.date     "dob"
    #   t.string   "confirmation_token"
    #   t.datetime "confirmed_at"
    #   t.datetime "confirmation_sent_at"
    #   t.boolean  "agree_on_email_marketing",   default: false
    #   t.string   "gender"
    #   t.string   "phone_number"
    #   t.boolean  "registrationConfirmed"
    #   t.float    "latitude"
    #   t.float    "longitude"
    #   t.datetime "location_last_update_time"
    # end

    # add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
    # add_index "users", ["email"], :name => "index_users_on_email", :unique => true
    # add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  end
end
