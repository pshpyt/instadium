class CreateTriviaConfigs < ActiveRecord::Migration
  def change
    create_table :trivia_configs do |t|
      t.string :url
      t.string :presented_image
      t.string :ad_image
      t.string :share_message

      t.timestamps
    end
  end
end
