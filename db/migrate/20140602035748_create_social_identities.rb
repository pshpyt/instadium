class CreateSocialIdentities < ActiveRecord::Migration
  def change
    create_table "social_identities", force: true do |t|
      t.integer  "user_id",           null: false
      t.string   "provider"
      t.string   "uid"
      t.string   "provider_username"
      t.string   "token"
      t.string   "secret"
      t.datetime "last_verified_at"
      t.datetime "expires_at"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   "avatar_url"
    end

    add_index "social_identities", ["provider", "uid"], :name => "index_social_identities_on_provider_and_uid"
    add_index "social_identities", ["token"], :name => "index_social_identities_on_token"
    add_index "social_identities", ["user_id", "provider"], :name => "index_social_identities_on_user_id_and_provider"
  end
end
