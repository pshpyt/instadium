# == Schema Information
#
# Table name: users
#
#  id                         :integer          not null, primary key
#  email                      :string(255)      default(""), not null
#  encrypted_password         :string(255)      default(""), not null
#  reset_password_token       :string(255)
#  reset_password_sent_at     :datetime
#  remember_created_at        :datetime
#  sign_in_count              :integer          default(0), not null
#  current_sign_in_at         :datetime
#  last_sign_in_at            :datetime
#  current_sign_in_ip         :string(255)
#  last_sign_in_ip            :string(255)
#  created_at                 :datetime
#  updated_at                 :datetime
#  name                       :string(255)
#  username                   :string(255)
#  full_name                  :string(255)
#  first_name                 :string(255)
#  last_name                  :string(255)
#  confirmation_token         :string(255)
#  confirmed_at               :datetime
#  confirmation_sent_at       :datetime
#  unconfirmed_email          :datetime
#  latitude                   :string(255)
#  longitude                  :string(255)
#  avatar                     :string(255)
#  use_social_identity_avatar :boolean          default(FALSE)
#  gender                     :string(255)
#  bio                        :text
#  location_last_update_time  :datetime
#  disabled                   :boolean
#  zip                        :string(255)
#  dob                        :datetime
#

require 'spec_helper'

describe User do
  context 'assotiations' do
    specify { expect(subject).to have_many(:itinerary_items)}
    specify { expect(subject).to have_many(:authentication_tokens)}
    specify { expect(subject).to have_many(:social_identities)}

    specify { expect(subject).to have_many(:instagram_identities)}
    specify { expect(subject).to have_many(:google_oauth2_identities)}
    specify { expect(subject).to have_many(:twitter_identities)}
    specify { expect(subject).to have_many(:facebook_identities)}
  end
  context 'fields' do
    specify { expect(subject).to respond_to(:id)}
    specify { expect(subject).to respond_to(:latitude)}
    specify { expect(subject).to respond_to(:longitude)}
    specify { expect(subject).to respond_to(:email)}
    specify { expect(subject).to respond_to(:encrypted_password)}
    specify { expect(subject).to respond_to(:reset_password_token)}
    specify { expect(subject).to respond_to(:reset_password_sent_at)}
    specify { expect(subject).to respond_to(:remember_created_at)}
    specify { expect(subject).to respond_to(:sign_in_count)}
    specify { expect(subject).to respond_to(:current_sign_in_at)}
    specify { expect(subject).to respond_to(:last_sign_in_at)}
    specify { expect(subject).to respond_to(:current_sign_in_ip)}
    specify { expect(subject).to respond_to(:last_sign_in_ip)}
    specify { expect(subject).to respond_to(:created_at)}
    specify { expect(subject).to respond_to(:updated_at)}
    specify { expect(subject).to respond_to(:name)}
    specify { expect(subject).to respond_to(:username)}
    specify { expect(subject).to respond_to(:bio)}
    specify { expect(subject).to respond_to(:first_name)}
    specify { expect(subject).to respond_to(:last_name)}
    specify { expect(subject).to respond_to(:full_name)}
    specify { expect(subject).to respond_to(:avatar)}
    specify { expect(subject).to respond_to(:display_location)}
    specify { expect(subject).to respond_to(:use_social_identity_avatar)}
    specify { expect(subject).to respond_to(:disabled)}
    specify { expect(subject).to respond_to(:zip)}
    specify { expect(subject).to respond_to(:dob)}
    specify { expect(subject).to respond_to(:confirmation_token)}
    specify { expect(subject).to respond_to(:confirmed_at)}
    specify { expect(subject).to respond_to(:confirmation_sent_at)}
    specify { expect(subject).to respond_to(:agree_on_email_marketing)}
    specify { expect(subject).to respond_to(:gender)}
    specify { expect(subject).to respond_to(:phone_number)}
  end
  context 'virtual attributes' do
    context 'verified' do
      specify { expect(subject).to respond_to(:verified)}
      specify do
        u = User.new confirmed_at: nil
        expect(u.verified).to be_true
      end
      specify do
        u = User.new confirmed_at: Time.now
        expect(u.verified).to be_true
      end
    end
    context 'guid' do
      specify { expect(subject).to respond_to(:guid)}
      specify do
        u = User.new id: nil
        expect(u.guid).to eq(u.id)
      end
      specify do
        u = User.new id: -1
        expect(u.guid).to eq(u.id)
      end
    end
  end
  context 'default values' do
    specify 'gender be 0' do
      u = create :user
      expect(u.gender).to be_nil
    end
  end
  context 'methods' do
    specify {expect(subject).to respond_to(:follow)}
    specify {expect(subject).to respond_to(:followers)}
    context 'login_with_email_or_username_and_password' do
      before(:each) do
        @pwd = 'password123'
        @confirmed_user = create :user, password: @pwd, password_confirmation: @pwd
        @unconfirmed_user = create :user, confirmed_at: nil, password: @pwd, password_confirmation: @pwd
      end
      it 'should return nil if the user is unconfirmed' do
        expect(User.login_with_email_or_username_and_password(@unconfirmed_user.email, @pwd)).not_to be_nil
      end
      it 'should return the token if the user is confirmed' do
        expect(User.login_with_email_or_username_and_password(@confirmed_user.email, @pwd)).to eq(@confirmed_user.get_authentication_token)
      end
    end
    context 'avatar_url' do
      context 'use_social_identity_avatar is set' do
        before(:each) do
          subject.use_social_identity_avatar = 'true'
        end
        context 'there is no social identity present' do
          it "should return its own avatar" do
            expect(subject).to receive(:non_social_identity_avatar)
            subject.avatar_url
          end
        end
        context 'social identity has no avatar_url' do
          it 'should return the non social identity avatar' do
            si = double('social_identity')
            allow(si).to receive(:avatar_url).and_return(nil)
            allow(subject).to receive(:social_identities).and_return([si])
            expect(subject).to receive(:non_social_identity_avatar)
            subject.avatar_url
          end
        end
        context 'there is social identity present' do
          it 'should return the first social identities avatar' do
            si = double('social_identity')
            allow(si).to receive(:avatar_url).and_return('x')
            expect(si).to receive(:avatar_url)
            allow(subject).to receive(:social_identities).and_return([si])
            expect(subject).not_to receive(:non_social_identity_avatar)
            subject.avatar_url
          end
        end
      end
      context 'use_social_identity_avatar is not set' do
        it "should return its own avatar" do
          expect(subject).to receive(:non_social_identity_avatar)
          subject.avatar_url
        end
      end
    end
    context 'non_social_identity_avatar' do
      context 'avatar is present' do
        it "should return the url value of the avatar" do
          avatar = double :avatar
          expect(avatar).to receive(:url)
          allow(subject).to receive(:avatar).and_return(avatar)
          subject.avatar = avatar
          subject.non_social_identity_avatar
        end
      end
      context 'avatar is not present' do
        context 'email is present' do
          it 'should return a gravatar image' do
            allow(subject).to receive(:email).and_return('boti@gmail.com')
            expect(subject.non_social_identity_avatar).to match(/www\.gravatar\.com/)
          end
        end
        context 'email is not present' do
          it "should return a silhouette" do
            allow(subject).to receive(:image_url) do |arg|
              arg
            end
            expect(subject.non_social_identity_avatar).to match(/profile_silhouette\.png/)
          end
        end
      end
    end
  end
end
