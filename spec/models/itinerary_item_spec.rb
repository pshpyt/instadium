# == Schema Information
#
# Table name: itinerary_items
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  product_id :integer
#  starttime  :datetime
#  endtime    :datetime
#  created_at :datetime
#  updated_at :datetime
#

require 'spec_helper'

describe ItineraryItem do
  context 'fields' do
    specify { expect(subject).to respond_to(:endtime)}
    specify { expect(subject).to respond_to(:starttime)}
  end
  context 'assotiations' do
    specify { expect(subject).to belong_to(:user)}
    specify { expect(subject).to belong_to(:product)}
  end
end
