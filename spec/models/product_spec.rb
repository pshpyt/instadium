# == Schema Information
#
# Table name: products
#
#  id                :integer          not null, primary key
#  brand             :string(255)
#  entity_path       :string(255)
#  market            :string(255)
#  market_rank       :integer
#  name              :string(255)
#  property_code     :string(255)
#  property_rank     :integer
#  category          :string(255)
#  created_at        :datetime
#  updated_at        :datetime
#  feed_id           :string(255)
#  expired           :boolean
#  coordinates       :hstore
#  mass_relevance    :hstore
#  short_description :text
#  details           :hstore
#  usage             :integer          default(0)
#

require 'spec_helper'

describe Product do
  context 'fields' do
    specify { expect(subject).to respond_to(:usage)}
    context 'property_rank' do
      specify {  expect(subject).to respond_to(:property_rank)}
      specify do
        create :product
        expect(Product.last.property_rank).to be_a_kind_of(Integer)
      end
    end
    context 'market_rank' do
      specify {  expect(subject).to respond_to(:market_rank)}
      specify do
        create :product
        expect(Product.last.market_rank).to be_a_kind_of(Integer)
      end
    end
  end
  context 'nested hash serialization' do
    specify 'details' do
      p = Product.new
      p.details = {a: {b: {c: :d}}}
      p.save
      expect(p.reload.details["a"]["b"]["c"]).to eq("d")
    end
  end
end
