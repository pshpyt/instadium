require 'spec_helper'


describe 'rails extension' do
  context 'string' do
    it "should extend string with int?" do
      expect('').to respond_to(:int?)
    end
    it "should extend string with float?" do
      expect('').to respond_to(:float?)
    end
  end
  context 'hash' do
    context 'numericalize values' do
      it "should have the method for the transformation" do
        expect({}).to respond_to(:numericalize_values)
      end
      it "should transform the values to numeric ones" do
        h = {a:'1',b:'1.3', c: {d:'1.2'}}
        expect(h.numericalize_values).to eq({a:1,b:1.3, c: {d:1.2}})
      end
      it "should transform the values to numeric ones only on first level" do
        h = {a:'1',b:'1.3', c: {d:'1.2'}}
        expect(h.numericalize_values(false)).to eq({a:1,b:1.3, c: {d:'1.2'}})
      end
    end
    context 'booleanize values' do
      it "should have the method for the transformation" do
        expect({}).to respond_to(:booleanize_values)
      end
      it "should transform the values to numeric ones" do
        h = {a:'True',b:'falsE', c: {cc: 'True'}}
        expect(h.booleanize_values).to eq({a:true,b:false, c:{cc: true}})
      end
      it "should transform the values to numeric ones only at first level" do
        h = {a:'True',b:'falsE', c: {cc: 'True'}}
        expect(h.booleanize_values(false)).to eq({a:true, b:false, c: {cc: 'True'}})
      end
    end
    context 'arrayize series' do
      it "should have the method for the transformation" do
        expect({}).to respond_to(:arrayize_series)
      end
      it "should transform to array where the keys form a mathematical series" do
        h = {a_2: :a, a_3: {c_1: :c, c_3: :c}, a_4: {b_1: :b, b_2: :b}, a_5: {d_1: :d, d_2: :d, d_4: :d}}
        expect(h.arrayize_series).to eq({a_2: :a, a_3: [:c, :c], a_4: [:b, :b], a_5: {d_1: :d, d_2: :d, d_4: :d}})
      end
    end
  end
end
