# == Schema Information
#
# Table name: messages
#
#  id         :integer          not null, primary key
#  message    :string(255)
#  group_id   :integer
#  media      :string(255)
#  timelimit  :integer
#  created_at :datetime
#  updated_at :datetime
#

require 'spec_helper'

describe Message do
  context 'fields' do
    specify { expect(subject).to respond_to(:message)}
    specify { expect(subject).to respond_to(:timelimit)}
    specify { expect(subject).to respond_to(:media)}
  end
  context 'assotiations' do
    specify { expect(subject).to belong_to(:group)}
  end
end
