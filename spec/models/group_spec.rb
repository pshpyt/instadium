# == Schema Information
#
# Table name: groups
#
#  id                :integer          not null, primary key
#  name              :string(255)
#  description       :text
#  users             :integer          is an Array
#  created_at        :datetime
#  updated_at        :datetime
#  unconfirmed_users :integer          is an Array
#

require 'spec_helper'

describe Group do
  context 'assotiations' do
    specify { expect(subject).to have_many(:messages)}
  end

  context 'fields' do
    specify { expect(subject).to respond_to(:id)}
    specify { expect(subject).to respond_to(:name)}
    specify { expect(subject).to respond_to(:description)}
    specify { expect(subject).to respond_to(:users)}
    specify { expect(subject).to respond_to(:unconfirmed_users)}
  end
end
