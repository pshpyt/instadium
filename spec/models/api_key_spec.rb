# == Schema Information
#
# Table name: api_keys
#
#  id         :integer          not null, primary key
#  token      :string(255)
#  expires_at :datetime
#  created_at :datetime
#  updated_at :datetime
#

require 'spec_helper'

describe ApiKey do
  context 'fields' do
    specify { expect(subject).to respond_to(:token)}
    specify { expect(subject).to respond_to(:expires_at)}
  end
end
