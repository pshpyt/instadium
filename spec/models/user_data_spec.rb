# == Schema Information
#
# Table name: user_data
#
#  id         :integer          not null, primary key
#  key        :string(255)
#  value      :hstore
#  created_at :datetime
#  updated_at :datetime
#

require 'spec_helper'

describe UserData do
  context 'nested hash serialization' do
    specify 'value' do
      p = UserData.new
      p.value = {a: {b: {c: :d}}}
      p.save
      expect(p.reload.value["a"]["b"]["c"]).to eq("d")
    end
  end
end
