require "spec_helper"

describe Admin::ResultsController do
  describe "routing" do

    it "routes to #index" do
      get("/admin/results").should route_to("admin/results#index")
    end

    it "routes to #new" do
      get("/admin/results/new").should route_to("admin/results#new")
    end

    it "routes to #show" do
      get("/admin/results/1").should route_to("admin/results#show", :id => "1")
    end

    it "routes to #edit" do
      get("/admin/results/1/edit").should route_to("admin/results#edit", :id => "1")
    end

    it "routes to #create" do
      post("/admin/results").should route_to("admin/results#create")
    end

    it "routes to #update" do
      put("/admin/results/1").should route_to("admin/results#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/admin/results/1").should route_to("admin/results#destroy", :id => "1")
    end

  end
end
