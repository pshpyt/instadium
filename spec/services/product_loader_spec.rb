require 'spec_helper'

describe Services::ProductLoader do
  specify 'should be able to load the data' do
    expect(subject).to respond_to(:load_data)
  end

  context 'create - expire - update' do
    let(:data) do
      attributes = attributes_for :product
      [attributes]
    end
    let(:product) do
      create :product, feed_id: 'old_one', expired: false
    end
    specify "it should create a new product if the product with the provided feed_id doesn't exists" do
      -> {
        Services::ProductLoader.new(data).load_data
      }.should change(Product, :count).by(1)
    end
    specify "it should update the product which is in the feed" do
      old_product = product
      updatable_product = create :product, feed_id: data.first[:feed_id], brand: 'should_be_updated', expired: true
      -> {
        Services::ProductLoader.new(data).load_data
      }.should change(Product, :count).by(0)
      expect(old_product.reload.expired).to be_false
      expect(updatable_product.reload.brand).to eq(data.first[:brand])
      expect(updatable_product.reload.expired).to be_false
    end

  end
end
