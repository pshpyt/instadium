# encoding: utf-8

require 'spec_helper'

describe Services::TransformTotalrewardsFeed do
  let(:event_feed) do
    feed = <<-JSON
{
  "@timestamp": "12-02-2014   15:29:20 PM",
  "event": [
      {
       "rank": "1",
       "title": "Absinthe",
       "brand": "InStadium",
       "type": "variety",
       "propertyRank": "7",
       "learnMoreURL": "http://www.InStadiumpalace.com/shows/absinthe.html",
       "marketRank": "1",
       "floor": "0",
       "additionalInfo": "",
       "category": "variety",
       "massRelevanceStreamID": "http://tweetriver.com/InStadiumMR/13-masterlasvegas-allhashtags.json",
       "latitude": "36.115454",
       "propCode": "clv",
       "longitude": "-115.174013",
       "displayMobileApp": "true",
       "massRelevanceHashTags": "#AbsintheVegas",
       "market": "lvm",
       "body": "&lt;p&gt;Absinthe is the critically-acclaimed acro-cabaret variety show that has been hailed by Las Vegas Sun as &ldquo;the most inventive and daring show to open on the Strip in years!&rdquo; Named the &ldquo;Best New Show&rdquo; on the Strip by both Vegas SEVEN and Las Vegas Weekly, ABSINTHE at InStadium Palace is the fantastical blend of carnival and spectacle, featuring wild and outlandish acts in a theatre-in-the-round presentation suitable for all seasons. Audiences are treated to a night of imagination and excess with performances that amaze and inspire. The cast of eccentrics evoke thrills and chills as they perform amazing feats of virtuosity, strength, balance and danger within mere feet of the audience surrounding the intimate stage. Hosted by the foul-mouthed Gazillionaire, along with his raucous assistant Penny Pibbets, ABSINTHE delivers jaw-dropping acts, raunchy comedy fun mixed with old world burlesque audiences have come to know and love.&lt;/p&gt;",
       "endDate": "2014-06-29T15:57:00.000-07:00",
       "displayDirectory": "true",
       "getTicketsURL": "http://www.ticketmaster.com/artist/802082?camefrom=CFC_InStadiumENT_CLV_Absinthe",
       "phoneNumber": "(800) 745-3000",
       "shortDescription": "A surreal experience for adults, ABSINTHE features wild and outlandish acts in a theatre-in-the-round presentation. Audiences are treated to a night of imagination and excess with performances that amaze and inspire.",
       "startDate": "2014-01-01T09:19:00.000-08:00",
       "priceRange": "Starting From $99 &lt;br&gt;(Plus Tax &amp; Fees)",
       "venue": "The Roman Plaza",
       "displayWeb": "true",
       "id": "clv_absinthe",
       "entityPath": "absinthe",
       "mainImageResource": "image",
       "mainImageUrl": "/content/dam/clv/Shows/Absinthe/InStadium-Las-Vegas-Shows-Absinthe-1.jpeg",
       "dateranges":       {
          "daterange_1":
            { "startDate": "2014-01-01T09:19:00.000-08:00",
              "performanceTimes": "7:30 PM,9:30 PM",
              "endDate": "2014-03-10T09:19:00.000-07:00"
            },
          "daterange_2":  
            {
              "startDate": "2014-03-12T15:57:00.000-07:00",
              "endDate": "2014-06-29T15:57:00.000-07:00"
            }
        }
      }]
}
JSON
      JSON.parse(feed)
  end

  let(:event_feed_with_name) do
    feed = <<-JSON
{
  "@timestamp": "12-02-2014   15:29:20 PM",
  "event": [
    {
       "rank": "1",
       "title": "Absinthe",
       "name": "Something",
       "brand": "InStadium",
       "type": "variety",
       "propertyRank": "7",
       "learnMoreURL": "http://www.InStadiumpalace.com/shows/absinthe.html",
       "marketRank": "1",
       "floor": "0",
       "additionalInfo": "",
       "category": "variety",
       "massRelevanceStreamID": "http://tweetriver.com/InStadiumMR/13-masterlasvegas-allhashtags.json",
       "latitude": "36.115454",
       "propCode": "clv",
       "longitude": "-115.174013",
       "displayMobileApp": "true",
       "massRelevanceHashTags": "#AbsintheVegas",
       "market": "lvm",
       "body": "&lt;p&gt;Absinthe is the critically-acclaimed acro-cabaret variety show that has been hailed by Las Vegas Sun as &ldquo;the most inventive and daring show to open on the Strip in years!&rdquo; Named the &ldquo;Best New Show&rdquo; on the Strip by both Vegas SEVEN and Las Vegas Weekly, ABSINTHE at InStadium Palace is the fantastical blend of carnival and spectacle, featuring wild and outlandish acts in a theatre-in-the-round presentation suitable for all seasons. Audiences are treated to a night of imagination and excess with performances that amaze and inspire. The cast of eccentrics evoke thrills and chills as they perform amazing feats of virtuosity, strength, balance and danger within mere feet of the audience surrounding the intimate stage. Hosted by the foul-mouthed Gazillionaire, along with his raucous assistant Penny Pibbets, ABSINTHE delivers jaw-dropping acts, raunchy comedy fun mixed with old world burlesque audiences have come to know and love.&lt;/p&gt;",
       "endDate": "2014-06-29T15:57:00.000-07:00",
       "displayDirectory": "true",
       "getTicketsURL": "http://www.ticketmaster.com/artist/802082?camefrom=CFC_InStadiumENT_CLV_Absinthe",
       "phoneNumber": "(800) 745-3000",
       "shortDescription": "A surreal experience for adults, ABSINTHE features wild and outlandish acts in a theatre-in-the-round presentation. Audiences are treated to a night of imagination and excess with performances that amaze and inspire.",
       "startDate": "2014-01-01T09:19:00.000-08:00",
       "priceRange": "Starting From $99 &lt;br&gt;(Plus Tax &amp; Fees)",
       "venue": "The Roman Plaza",
       "displayWeb": "true",
       "id": "clv_absinthe",
       "entityPath": "absinthe",
       "mainImageResource": "image",
       "mainImageUrl": "/content/dam/clv/Shows/Absinthe/InStadium-Las-Vegas-Shows-Absinthe-1.jpeg",
       "dateranges":       {
          "daterange_1":
            { "startDate": "2014-01-01T09:19:00.000-08:00",
              "performanceTimes": "7:30 PM,9:30 PM",
              "endDate": "2014-03-10T09:19:00.000-07:00"
            },
          "daterange_2":  
            {
              "startDate": "2014-03-12T15:57:00.000-07:00",
              "endDate": "2014-06-29T15:57:00.000-07:00"
            }
        }
      }]
}
JSON
      JSON.parse(feed)
  end

  let(:todo_feed) do
    feed = <<-JSON
{
  "@timestamp": "12-02-2014   16:50:07 PM",
  "thingstodo": [
        {
      "body": "<p>highly acclaimed sin city comedy will now call planet hollywood resort  casino home. combining world_class comedy with sinfully sexy burlesque, this stand_up comedy show will delight guests at the sin city theatre on the mezz level nightly at 9 p.m.<br><br>we are excited to bring our unique brand of comedy entertainment to planet hollywood, said sin city theater founder john padon. i've been a comedian for over twenty years; i knew i wanted to develop a venue different from the others, which i believe we have created with sin city comedy.<br><br>two time winner of the coveted best of las vegas award, sin city comedy will feature a rotating lineup of today's top comedians, all recognizable from appearances on comedy central, vh1, hbo, the late show with david letterman, the tonight show with jay leno, conan, the late late show and more.<br><br>the comedy show's burlesque twist dates back to the 1840s, when comedy and burlesque went hand and hand. unlike any comedy show around, sin city comedy will tickle funny bones and more.</p><p></p>",
      "propCode": "rlv",
      "longitude": "-115.18723",
      "name": " Rio Locals",
      "brand": "rio",
      "displayMobileApp": "true",
      "type": "promotions",
      "massRelevanceHashTags": "#RioLocals",
      "propertyRank": "3",
      "market": "lvm",
      "learnMoreURL": "/content/property/rio-all-suite-hotel-and-casino/index/things-to-do/rio-locals",
      "marketRank": "1",
      "floor": "0",
      "displayDirectory": "true",
      "attire": "Casual",
      "massRelevanceStreamID": "http://tweetriver.com/InStadiumMR/13-masterlasvegas-allhashtags.json",
      "learnMoreText": "LEARN MORE",
      "shortDescription": "Being a local has it's rewards! Text LOCALS to 227466 to learn more.",
      "latitude": "36.116384",
      "genericType": "promotions",
      "displayWeb": "true",
      "id": "rlv_riolocals",
      "entityPath": "local_deals",
      "mainImageResource": "image",
      "mainImageUrl": "/content/dam/rlv/Property/Exterior/Rio-All-Suites Hotel & Casino-Property-Exterior-1.jpg"
    }]}
JSON
    JSON.parse(feed)
  end

  let(:restaurant_feed) do
    feed = <<-JSON
{
  "@timestamp": "12-02-2014   17:12:21 PM",
  "restaurant": [
        {
      "name": "All-American Bar and Grille",
      "brand": "rio",
      "openTableLink": "http://www.opentable.com/all-american-bar-and-grille-rio-all-suite-hotel-and-casino",
      "averagecheck": "$15 to $25",
      "propertyRank": "7",
      "learnMoreURL": "http://www.riolasvegas.com/restaurants/all-american-bar-and-grille.html",
      "marketRank": "1",
      "floor": "0",
      "attire": "Casual",
      "massRelevanceStreamID": "http://tweetriver.com/InStadiumMR/13-masterlasvegas-allhashtags.json",
      "category": "casual",
      "latitude": "36.11719",
      "propCode": "rlv",
      "longitude": "-115.188325",
      "openTableRID": "95293",
      "displayMobileApp": "true",
      "phone": "(702) 777-7767",
      "market": "lvm",
      "massRelevanceHashTags": "#AllAmericanBarandGrille",
      "displayDirectory": "true",
      "expenseLevel": "$$",
      "dinnerMenu": "/content/dam/rlv/Dining/Casual/all american bar and grille/All American Bar and Grille Dinner Menu.pdf",
      "shortDescription": "This casual dining Las Vegas restaurant serves in-house &ldquo;Rio Dry Aged&rdquo; steaks and seafood cooked over a mesquite grill, plus salads and sandwiches.",
      "cuisine": "American",
      "displayWeb": "true",
      "id": "rlv_allamericanbarandgrille",
      "entityPath": "all_american_bar_and",
      "mainImageResource": "image",
      "logoImageResource": "logo",
      "mainImageUrl": "/content/dam/rlv/Dining/Casual/all american bar and grille/Rio-All-Suites Hotel & Casino-Dining-Casual-all-american bar and grille-5.jpg",
      "logoImageUrl": "/content/dam/rlv/Dining/Casual/all american bar and grille/Rio-All-Suite-Hotel-&-Casino-Dining-Casual-All-American-Bar-&-Grille-1.jpg",
      "facts": [      {
        "day": "Daily",
        "time": "11:00AM - 6:00AM"
      }],
      "hours":       [
                {
          "day": "Sun &amp; Mon",
          "time": "11:00AM - 6:00AM"
        },
                {
          "day": "Tue",
          "time": "11:00AM - 11:00PM"
        },
                {
          "day": "Wed-Sat",
          "time": "11:00AM - 6:00AM"
        }
      ]
    }
    ]}
JSON
    JSON.parse(feed)
  end

  context 'general fields' do
    specify "should contain the right brand" do
      field = :brand
      transformed = Services::TransformTotalrewardsFeed.new(event_feed).transform('event')
      expect(transformed.first[field]).to eq event_feed['event'].first[field.to_s]
    end
    specify "should contain the right lattitude" do
      field = :latitude
      transformed = Services::TransformTotalrewardsFeed.new(event_feed).transform('event')
      expect(transformed.first[:coordinates][field]).to eq event_feed['event'].first[field.to_s].to_f
    end
    specify "should contain the right longitude" do
      field = :longitude
      transformed = Services::TransformTotalrewardsFeed.new(event_feed).transform('event')
      expect(transformed.first[:coordinates][field]).to eq event_feed['event'].first[field.to_s].to_f
    end
    specify "entity_path transformation" do
      field = :entity_path
      transformed = Services::TransformTotalrewardsFeed.new(event_feed).transform('event')
      expect(transformed.first[field]).to eq event_feed['event'].first['entityPath']
    end
    specify "market transformation" do
      field = :market
      transformed = Services::TransformTotalrewardsFeed.new(event_feed).transform('event')
      expect(transformed.first[field]).to eq event_feed['event'].first[field.to_s]
    end
    specify "market_rank transformation" do
      field = :market_rank
      transformed = Services::TransformTotalrewardsFeed.new(event_feed).transform('event')
      expect(transformed.first[field]).to eq event_feed['event'].first['marketRank'].to_i
    end
    specify "name transformation" do
      field = :name
      transformed = Services::TransformTotalrewardsFeed.new(event_feed_with_name).transform('event')
      expect(transformed.first[field]).to eq event_feed_with_name['event'].first[field.to_s]
    end
    specify "date_ranges transformation" do
      transformed = Services::TransformTotalrewardsFeed.new(event_feed_with_name).transform('event')
      expect(transformed.first[:details]['dateranges']).to be_a_kind_of(Array)
    end
    specify "name be taken from title if name is empty" do
      field = :name
      transformed = Services::TransformTotalrewardsFeed.new(event_feed).transform('event')
      expect(transformed.first[field]).to eq event_feed['event'].first['title']
    end
    specify "prop_code transformation" do
      field = :property_code
      transformed = Services::TransformTotalrewardsFeed.new(event_feed).transform('event')
      expect(transformed.first[field]).to eq event_feed['event'].first['propCode']
    end
    specify "property_rank transformation" do
      field = :property_rank
      transformed = Services::TransformTotalrewardsFeed.new(event_feed).transform('event')
      expect(transformed.first[field]).to eq event_feed['event'].first['propertyRank'].to_i
    end
    specify "feed_id transformation" do
      field = :feed_id
      transformed = Services::TransformTotalrewardsFeed.new(event_feed).transform('event')
      expect(transformed.first[field]).to eq event_feed['event'].first['id']
    end
    specify "hash tag and stream id must be in the mass_relevance" do
      transformed = Services::TransformTotalrewardsFeed.new(event_feed).transform('event')
      expect(transformed.first[:mass_relevance][:stream_id]).to eq event_feed['event'].first['massRelevanceStreamID']
      expect(transformed.first[:mass_relevance][:hash_tags]).to eq event_feed['event'].first['massRelevanceHashTags']
    end
    specify "short description transformation" do
      transformed = Services::TransformTotalrewardsFeed.new(event_feed).transform('event')
      expect(transformed.first[:short_description]).to eq event_feed['event'].first['shortDescription']
    end
  end

  context 'category' do
    context 'todo_feed' do
      specify 'type should be transformed into category' do
        transformed = Services::TransformTotalrewardsFeed.new(todo_feed).transform('thingstodo')
        expect(transformed.first[:category]).to eq todo_feed['thingstodo'].first['type']
      end
    end
    context 'event_feed' do
      specify "category should be 'event'" do
        transformed = Services::TransformTotalrewardsFeed.new(event_feed).transform('event')
        expect(transformed.first[:category]).to eq 'event'
      end
    end
    context 'restaurant_feed' do
      specify "category should be 'restaurant'" do
        transformed = Services::TransformTotalrewardsFeed.new(restaurant_feed).transform('restaurant')
        expect(transformed.first[:category]).to eq 'restaurant'
      end
    end
  end

  context 'details' do
    specify 'should be a hash' do
      transformed = Services::TransformTotalrewardsFeed.new(todo_feed).transform('thingstodo')
      expect(transformed.first[:details]).to be_kind_of Hash
    end
    specify 'should have values' do
      transformed = Services::TransformTotalrewardsFeed.new(todo_feed).transform('thingstodo')
      expect(transformed.first[:details].values).to be_present
    end
    context 'hours' do
      let(:feed1) do
        feed = <<-JSON
    {
      "@timestamp": "12-02-2014   15:29:20 PM",
      "event": [
          {
           "brand": "InStadium",
           "latitude": "36.115454",
           "longitude": "-115.174013",
           "entityPath": "absinthe",
           "market": "lvm",
           "marketRank": "1",
           "title": "Absinthe",
           "propertyRank": "7",
           "propCode": "clv",
           "massRelevanceStreamID": "http://tweetriver.com/InStadiumMR/13-masterlasvegas-allhashtags.json",
           "massRelevanceHashTags": "#AbsintheVegas",
           "shortDescription": "A surreal experience for adults, ABSINTHE features wild and outlandish acts in a theatre-in-the-round presentation. Audiences are treated to a night of imagination and excess with performances that amaze and inspire.",
           "id": "clv_absinthe",
            "hours": [
              {
                "day": "Bar",
                "time": " "
              },
              {
                "day": "Daily",
                "time": "5:00PM - 3:00AM"
              },
              {
                "day": "Lounge",
                "time": " "
              },
              {
                "day": "Sun - Thurs",
                "time": "8:00PM - 3:00AM"
              },
              {
                "day": "Fri-Sat",
                "time": "11:00PM - 3:00AM"
              }
            ]

          }]
    }
    JSON
          JSON.parse(feed)
      end
      let(:feed2) do
        feed = <<-JSON
    {
      "@timestamp": "12-02-2014   15:29:20 PM",
      "event": [
          {
           "brand": "InStadium",
           "latitude": "36.115454",
           "longitude": "-115.174013",
           "entityPath": "absinthe",
           "market": "lvm",
           "marketRank": "1",
           "title": "Absinthe",
           "propertyRank": "7",
           "propCode": "clv",
           "massRelevanceStreamID": "http://tweetriver.com/InStadiumMR/13-masterlasvegas-allhashtags.json",
           "massRelevanceHashTags": "#AbsintheVegas",
           "shortDescription": "A surreal experience for adults, ABSINTHE features wild and outlandish acts in a theatre-in-the-round presentation. Audiences are treated to a night of imagination and excess with performances that amaze and inspire.",
           "id": "clv_absinthe",
        "hours":       [
        ["Restaurant"],
                {
          "day": "Sun-Thu   ",
          "time": "11:00AM - 10:00PM"
        },
                {
          "day": "Fri &amp; Sat ",
          "time": "11:30AM - 12:00AM"
        },
        ["Bar"],
                {
          "day": "Sun &amp; Mon ",
          "time": "11:30AM - 12:00AM"
        },
                {
          "day": "Tue-Thu ",
          "time": "11:30AM - 1:00AM"
        },
                {
          "day": "Fri-Sat ",
          "time": "11:30AM - 4:00AM"
        }
      ]
          }]
    }
    JSON
          JSON.parse(feed)
      end
      let(:feed3) do
        feed = <<-JSON
    {
      "@timestamp": "12-02-2014   15:29:20 PM",
      "event": [
          {
           "brand": "InStadium",
           "latitude": "36.115454",
           "longitude": "-115.174013",
           "entityPath": "absinthe",
           "market": "lvm",
           "marketRank": "1",
           "title": "Absinthe",
           "propertyRank": "7",
           "propCode": "clv",
           "massRelevanceStreamID": "http://tweetriver.com/InStadiumMR/13-masterlasvegas-allhashtags.json",
           "massRelevanceHashTags": "#AbsintheVegas",
           "shortDescription": "A surreal experience for adults, ABSINTHE features wild and outlandish acts in a theatre-in-the-round presentation. Audiences are treated to a night of imagination and excess with performances that amaze and inspire.",
           "id": "clv_absinthe",
        "hours":       [
        "Restaurant",
                {
          "day": "Sun-Thu   ",
          "time": "11:00AM - 10:00PM"
        },
                {
          "day": "Fri &amp; Sat ",
          "time": "11:30AM - 12:00AM"
        },
        "Bar",
                {
          "day": "Sun &amp; Mon ",
          "time": "11:30AM - 12:00AM"
        },
                {
          "day": "Tue-Thu ",
          "time": "11:30AM - 1:00AM"
        },
                {
          "day": "Fri-Sat ",
          "time": "11:30AM - 4:00AM"
        }
      ]
          }]
    }
    JSON
          JSON.parse(feed)
      end
      let(:closed1) do
        feed = <<-JSON
    {
      "@timestamp": "12-02-2014   15:29:20 PM",
      "event": [
          {
           "brand": "InStadium",
           "latitude": "36.115454",
           "longitude": "-115.174013",
           "entityPath": "absinthe",
           "market": "lvm",
           "marketRank": "1",
           "title": "Absinthe",
           "propertyRank": "7",
           "propCode": "clv",
           "massRelevanceStreamID": "http://tweetriver.com/InStadiumMR/13-masterlasvegas-allhashtags.json",
           "massRelevanceHashTags": "#AbsintheVegas",
           "shortDescription": "A surreal experience for adults, ABSINTHE features wild and outlandish acts in a theatre-in-the-round presentation. Audiences are treated to a night of imagination and excess with performances that amaze and inspire.",
           "id": "clv_absinthe",
            "hours": [{
              "day": "CLOSED FOR THE SEASON",
              "time": " "
            }]
          }]
    }
    JSON
          JSON.parse(feed)
      end
      let(:closed2) do
        feed = <<-JSON
    {
      "@timestamp": "12-02-2014   15:29:20 PM",
      "event": [
          {
           "brand": "InStadium",
           "latitude": "36.115454",
           "longitude": "-115.174013",
           "entityPath": "absinthe",
           "market": "lvm",
           "marketRank": "1",
           "title": "Absinthe",
           "propertyRank": "7",
           "propCode": "clv",
           "massRelevanceStreamID": "http://tweetriver.com/InStadiumMR/13-masterlasvegas-allhashtags.json",
           "massRelevanceHashTags": "#AbsintheVegas",
           "shortDescription": "A surreal experience for adults, ABSINTHE features wild and outlandish acts in a theatre-in-the-round presentation. Audiences are treated to a night of imagination and excess with performances that amaze and inspire.",
           "id": "clv_absinthe",
            "hours": [{
              "day": " ",
              "time": "CLOSED FOR THE SEASON"
            }]
          }]
    }
    JSON
          JSON.parse(feed)
      end
      let(:regular1) do
        feed = <<-JSON
    {
      "@timestamp": "12-02-2014   15:29:20 PM",
      "event": [
          {
           "brand": "InStadium",
           "latitude": "36.115454",
           "longitude": "-115.174013",
           "entityPath": "absinthe",
           "market": "lvm",
           "marketRank": "1",
           "title": "Absinthe",
           "propertyRank": "7",
           "propCode": "clv",
           "massRelevanceStreamID": "http://tweetriver.com/InStadiumMR/13-masterlasvegas-allhashtags.json",
           "massRelevanceHashTags": "#AbsintheVegas",
           "shortDescription": "A surreal experience for adults, ABSINTHE features wild and outlandish acts in a theatre-in-the-round presentation. Audiences are treated to a night of imagination and excess with performances that amaze and inspire.",
           "id": "clv_absinthe",
            "hours": [{
              "day": "Mon-Wed",
              "time": "12:00PM - 10:00PM"
            },{
              "day": "Thu-Sun",
              "time": "12:00PM - 12:00AM"
            }]
          }]
    }
    JSON
          JSON.parse(feed)
      end
      let(:regular2) do
        feed = <<-JSON
    {
      "@timestamp": "12-02-2014   15:29:20 PM",
      "event": [
          {
           "brand": "InStadium",
           "latitude": "36.115454",
           "longitude": "-115.174013",
           "entityPath": "absinthe",
           "market": "lvm",
           "marketRank": "1",
           "title": "Absinthe",
           "propertyRank": "7",
           "propCode": "clv",
           "massRelevanceStreamID": "http://tweetriver.com/InStadiumMR/13-masterlasvegas-allhashtags.json",
           "massRelevanceHashTags": "#AbsintheVegas",
           "shortDescription": "A surreal experience for adults, ABSINTHE features wild and outlandish acts in a theatre-in-the-round presentation. Audiences are treated to a night of imagination and excess with performances that amaze and inspire.",
           "id": "clv_absinthe",
            "hours": [{
              "day": "Thu-Sun",
              "time": "Open at 1:00AM"
            }]
          }]
    }
    JSON
          JSON.parse(feed)
      end
      let(:missing_hours) do
        feed = <<-JSON
    {
      "@timestamp": "12-02-2014   15:29:20 PM",
      "event": [
          {
           "brand": "InStadium",
           "latitude": "36.115454",
           "longitude": "-115.174013",
           "entityPath": "absinthe",
           "market": "lvm",
           "marketRank": "1",
           "title": "Absinthe",
           "propertyRank": "7",
           "propCode": "clv",
           "massRelevanceStreamID": "http://tweetriver.com/InStadiumMR/13-masterlasvegas-allhashtags.json",
           "massRelevanceHashTags": "#AbsintheVegas",
           "shortDescription": "A surreal experience for adults, ABSINTHE features wild and outlandish acts in a theatre-in-the-round presentation. Audiences are treated to a night of imagination and excess with performances that amaze and inspire.",
           "id": "clv_absinthe"
          }]
    }
    JSON
          JSON.parse(feed)
      end

      let(:expected1) do
        {"hours"=> [{
            "name"=> "Bar",
            "hours"=> [{
              "day"=> "Daily",
              "time"=> "5:00PM - 3:00AM"
              }]
          },{
            "name"=> "Lounge",
            "hours"=> [{
                "day"=> "Sun - Thurs",
                "time"=> "8:00PM - 3:00AM"
              }, {
                "day"=> "Fri-Sat",
                "time"=> "11:00PM - 3:00AM"
            }]
          }]
        }
      end
      let(:expected2) do
        {
          "hours" => [{
                'name' => "Restaurant",
                'hours' =>[{
                  "day" => "Sun-Thu   ",
                  "time" => "11:00AM - 10:00PM"
                },{
                  "day" => "Fri &amp; Sat ",
                  "time" => "11:30AM - 12:00AM"
                }]},
                  {
                  'name' => "Bar",
                  'hours' => [
                    {
                      "day" => "Sun &amp; Mon ",
                      "time" => "11:30AM - 12:00AM"
                    },{
                      "day" => "Tue-Thu ",
                      "time" => "11:30AM - 1:00AM"
                    },{
                      "day" => "Fri-Sat ",
                      "time" => "11:30AM - 4:00AM"
                    }]}
              ]
        }
      end
      let(:expected3) { expected2 }
      let(:expected4) do
        {
          "hours" => [{
            'name' => 'Closed for the season',
            'hours' => [{'day' => 'closed', 'time' => 'closed'}]
          }]
        }
      end
      let(:expected5) do
        {
          "hours" => [{
            "name" => 'Open Hours',
            "hours" => [{
              "day" => "Mon-Wed",
              "time" => "12:00PM - 10:00PM"
            },{
              "day" => "Thu-Sun",
              "time" => "12:00PM - 12:00AM"
            }]
          }]
        }
      end
      let(:expected6) do
        {
          "hours" => [{
            "name" => "Open Hours",
            'hours' => [{
                "day" => "Thu-Sun",
                "time" => "Open at 1:00AM"
              }]
          }]
        }
      end
      specify 'it should detect sections of venue and partition the time accordingly' do
        transformed = Services::TransformTotalrewardsFeed.new(feed1).transform('event')
        expect(transformed.first[:details]).to eq(expected1)
      end
      specify 'it should detect sections of venue and partition the time accordingly' do
        transformed = Services::TransformTotalrewardsFeed.new(feed2).transform('event')
        expect(transformed.first[:details]).to eq(expected2)
      end
      specify 'it should detect sections of venue and partition the time accordingly' do
        transformed = Services::TransformTotalrewardsFeed.new(feed3).transform('event')
        expect(transformed.first[:details]).to eq(expected3)
      end
      specify 'it should detect the closed for season time format' do
        transformed = Services::TransformTotalrewardsFeed.new(closed1).transform('event')
        expect(transformed.first[:details]).to eq(expected4)
      end
      specify 'it should detect the closed for season time format' do
        transformed = Services::TransformTotalrewardsFeed.new(closed2).transform('event')
        expect(transformed.first[:details]).to eq(expected4)
      end
      specify 'it should detect the closed for season time format' do
        transformed = Services::TransformTotalrewardsFeed.new(regular1).transform('event')
        expect(transformed.first[:details]).to eq(expected5)
      end
      specify 'it should detect the closed for season time format' do
        transformed = Services::TransformTotalrewardsFeed.new(regular2).transform('event')
        expect(transformed.first[:details]).to eq(expected6)
      end
      specify 'it should detect the if hours are missing' do
        transformed = Services::TransformTotalrewardsFeed.new(missing_hours).transform('event')
        expect(transformed.first[:details]['hours']).to be_blank
      end
      specify 'it should remove styling tags' do
        transformed = Services::TransformTotalrewardsFeed.new(todo_feed).transform('thingstodo')
        expect(transformed.first[:details]['body']).to eq("highly acclaimed sin city comedy will now call planet hollywood resort  casino home. combining world_class comedy with sinfully sexy burlesque, this stand_up comedy show will delight guests at the sin city theatre on the mezz level nightly at 9 p.m.we are excited to bring our unique brand of comedy entertainment to planet hollywood, said sin city theater founder john padon. i've been a comedian for over twenty years; i knew i wanted to develop a venue different from the others, which i believe we have created with sin city comedy.two time winner of the coveted best of las vegas award, sin city comedy will feature a rotating lineup of today's top comedians, all recognizable from appearances on comedy central, vh1, hbo, the late show with david letterman, the tonight show with jay leno, conan, the late late show and more.the comedy show's burlesque twist dates back to the 1840s, when comedy and burlesque went hand and hand. unlike any comedy show around, sin city comedy will tickle funny bones and more.")
      end
    end
  end
end
