require 'spec_helper'

describe CreateItineraryService do
  context 'invalid parameters' do
    specify 'it should throw ArgumentError' do
      expect { subject.execute }.to raise_error(ArgumentError)
    end
  end
  context 'valid parameters' do
    let(:user) { create :user }
    let(:product) { create :product }
    let(:start_time) { 1.day.ago }
    let(:end_time) { 2.days.from_now }
    subject(:subject) { CreateItineraryService.new user, product, start_time, end_time }
    specify 'it should create an itinerary item' do
      expect { subject.execute }.to change{ItineraryItem.count}.by(1)
    end
    specify 'it should set the right attributes for the new itinerary item' do
      subject.execute
      item = ItineraryItem.last
      expect(item.user).to eq(user)
      expect(item.product).to eq(product)
      expect(item.starttime).to be_within(1.minute).of(start_time)
      expect(item.endtime).to be_within(1.minute).of(end_time)
    end
    specify 'it should increment the product usage' do
      expect{subject.execute}.to change{product.usage}.by(1)
    end
  end
end
