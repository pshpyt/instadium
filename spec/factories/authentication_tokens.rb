# == Schema Information
#
# Table name: authentication_tokens
#
#  id         :integer          not null, primary key
#  token      :string(255)
#  user_id    :integer
#  expires_at :datetime
#  created_at :datetime
#  updated_at :datetime
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :authentication_token do
    user
    expires_at { Clock.now + 30.days }
  end
end
