# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :trivia_config do
    url "MyString"
    presented_image "MyString"
    ad_image "MyString"
    share_message "MyString"
  end
end
