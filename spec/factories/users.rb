# == Schema Information
#
# Table name: users
#
#  id                         :integer          not null, primary key
#  email                      :string(255)      default(""), not null
#  encrypted_password         :string(255)      default(""), not null
#  reset_password_token       :string(255)
#  reset_password_sent_at     :datetime
#  remember_created_at        :datetime
#  sign_in_count              :integer          default(0), not null
#  current_sign_in_at         :datetime
#  last_sign_in_at            :datetime
#  current_sign_in_ip         :string(255)
#  last_sign_in_ip            :string(255)
#  created_at                 :datetime
#  updated_at                 :datetime
#  name                       :string(255)
#  username                   :string(255)
#  full_name                  :string(255)
#  first_name                 :string(255)
#  last_name                  :string(255)
#  confirmation_token         :string(255)
#  confirmed_at               :datetime
#  confirmation_sent_at       :datetime
#  unconfirmed_email          :datetime
#  latitude                   :string(255)
#  longitude                  :string(255)
#  avatar                     :string(255)
#  use_social_identity_avatar :boolean          default(FALSE)
#  gender                     :string(255)
#  bio                        :text
#  location_last_update_time  :datetime
#  disabled                   :boolean
#  zip                        :string(255)
#  dob                        :datetime
#

# Read about factories at https://github.com/thoughtbot/factory_girl
include ActionDispatch::TestProcess

FactoryGirl.define do
  factory :user, aliases: [:recipient] do
    sequence(:username) {|n| "user-#{n}" }
    sequence(:first_name) {|n| "#{n}thSteven" }
    sequence(:last_name) {|n| "#{n}thJobs" }
    sequence(:email) {|n| "user-#{n}@example.org" }
    sequence(:password) {|n| "password-#{n}"}
    sequence(:confirmed_at) {|n| Time.now}
    factory :user_with_avatar do
      avatar { fixture_file_upload("spec/fixtures/images/test_avatar.jpg", "image/jpeg") }
    end

   # trait :admin do
   #   role "admin"
   # end
    trait :admin do
      after(:create) {|user| user.add_role(:admin)}
    end
  end
end
