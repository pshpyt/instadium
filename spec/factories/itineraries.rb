# == Schema Information
#
# Table name: itineraries
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  product_id :integer
#  starttime  :datetime
#  endtime    :datetime
#  created_at :datetime
#  updated_at :datetime
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :itinerary do
    user
    product
    starttime "2014-05-08 14:19:28"
    endtime "2014-05-08 14:19:28"
  end
end
