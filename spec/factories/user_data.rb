# == Schema Information
#
# Table name: user_data
#
#  id         :integer          not null, primary key
#  key        :string(255)
#  value      :hstore
#  created_at :datetime
#  updated_at :datetime
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user_datum, :class => 'UserData' do
    key "MyString"
    value ""
  end
end
