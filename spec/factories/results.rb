# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :result do
    ip "MyString"
    url "MyString"
    right_answers "MyString"
    started_at "2014-07-05 23:08:03"
    finished_at "2014-07-05 23:08:03"
    score 1
  end
end
