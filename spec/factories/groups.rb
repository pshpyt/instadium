# == Schema Information
#
# Table name: groups
#
#  id                :integer          not null, primary key
#  name              :string(255)
#  description       :text
#  users             :integer          is an Array
#  created_at        :datetime
#  updated_at        :datetime
#  unconfirmed_users :integer          is an Array
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :group do
    name "Group"
    description "Test Group"
    users []
    unconfirmed_users []
  end
end
