# == Schema Information
#
# Table name: products
#
#  id                :integer          not null, primary key
#  brand             :string(255)
#  entity_path       :string(255)
#  market            :string(255)
#  market_rank       :integer
#  name              :string(255)
#  property_code     :string(255)
#  property_rank     :integer
#  category          :string(255)
#  created_at        :datetime
#  updated_at        :datetime
#  feed_id           :string(255)
#  expired           :boolean
#  coordinates       :hstore
#  mass_relevance    :hstore
#  short_description :text
#  details           :hstore
#  usage             :integer          default(0)
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :product do
    brand         'brand'
    entity_path   'entityPath'
    market        'market'
    market_rank   'marketRank'
    name          'name'
    category      'category'
    property_code 'propCode'
    property_rank 'propertyRank'
    sequence(:feed_id) {|x| "#{x}"}
  end
end
