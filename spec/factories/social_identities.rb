# == Schema Information
#
# Table name: social_identities
#
#  id                :integer          not null, primary key
#  user_id           :integer          not null
#  provider          :string(255)
#  uid               :string(255)
#  provider_username :string(255)
#  token             :string(255)
#  secret            :string(255)
#  last_verified_at  :datetime
#  expires_at        :datetime
#  created_at        :datetime
#  updated_at        :datetime
#  avatar_url        :string(255)
#

FactoryGirl.define do
  factory :social_identity do
    user
    provider          'facebook'
    uid               '123234'
    provider_username '123123'
    token             '345456'
    secret            '456567'
    last_verified_at  Time.now
    expires_at        1.hours.from_now
    created_at        Time.now
    updated_at        Time.now
    avatar_url        ''
  end
end
