# == Schema Information
#
# Table name: messages
#
#  id         :integer          not null, primary key
#  message    :string(255)
#  group_id   :integer
#  media      :string(255)
#  timelimit  :integer
#  created_at :datetime
#  updated_at :datetime
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :message do
    message "MyString"
    group nil
    media "MyString"
    timelimit 1
  end
end
