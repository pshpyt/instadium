require 'spec_helper'

describe '/api/itinerary' do

  let(:api_key) { ApiKey.create }
  let(:developer_header){{'Authorization' => api_key.token}}
  let(:user) do
    create :user
  end
  let(:token) { create :authentication_token, user: user }
  let(:product) { create :product }

  specify{expect(InStadium::ItineraryApi).not_to be_nil}

  context 'creating itinerary' do
    let(:params) { {token: token.token, product_id: product.id, start_time: 1.day.from_now, end_time: 2.days.from_now} }
    context 'without developer key' do
      it "should be an unauthorized call" do
        post "/api/itinerary", params
        response.status.should == 401
      end
    end
    context 'with developer key' do
      context 'with invalid parameters' do
        context 'invalid token' do
          specify 'should return 401 if token is missing' do
            post "/api/itinerary", params.except(:token), developer_header
            response.status.should == 401
          end
          specify 'should return 401 if token is invalid' do
            post "/api/itinerary", params.merge(token: 'asdasd'), developer_header
            response.status.should == 401
          end
        end
        context 'invalid product_id' do
          specify 'should return 500 if product_id is missing' do
            post "/api/itinerary", params.except(:product_id), developer_header
            response.status.should == 500
          end
          specify 'should return 404 if product_id is invalid' do
            post "/api/itinerary", params.merge(product_id: 'adsd'), developer_header
            response.status.should == 404
          end
        end
      end
      context 'with valid parameters' do
        specify 'should return 201' do
          post "/api/itinerary", params, developer_header
          response.status.should == 201
        end
        specify 'should increase the itinerary count' do
          expect do
            post "/api/itinerary", params, developer_header
          end.to change{ItineraryItem.count}.by(1)
        end
      end
    end
  end

  context 'deleting itinerary' do
    let(:params) { {token: token.token, product_id: product.id} }
    context 'without developer key' do
      it "should be an unauthorized call" do
        delete "/api/itinerary", params
        response.status.should == 401
      end
    end
    context 'with developer key' do
      context 'with invalid parameters' do
        context 'invalid token' do
          specify 'should return 401 if token is missing' do
            delete "/api/itinerary", params.except(:token), developer_header
            response.status.should == 401
          end
          specify 'should return 401 if token is invalid' do
            delete "/api/itinerary", params.merge(token: 'asdasd'), developer_header
            response.status.should == 401
          end
        end
        context 'invalid product_id' do
          specify 'should return 500 if product_id is missing' do
            delete "/api/itinerary", params.except(:product_id), developer_header
            response.status.should == 500
          end
          specify 'should return 404 if product_id is invalid' do
            delete "/api/itinerary", params.merge(product_id: 'adsd'), developer_header
            response.status.should == 404
          end
        end
      end
      context 'with valid parameters' do
        before(:each) do
          itinerary = user.itinerary_items.create product: product, starttime: 1.days.from_now, endtime: 2.days.from_now
        end
        specify 'should return 200 if product_id is valid' do
          delete "/api/itinerary", params, developer_header
          response.status.should == 200
        end
        specify 'should return 200 if product_id is valid' do
          expect do
            delete "/api/itinerary", params, developer_header
          end.to change{ItineraryItem.count}.by(-1)
        end
      end
    end
  end

  context 'querying itineraries' do
    before(:each) do
      user.itinerary_items.create product: product, starttime: 1.days.from_now, endtime: 2.days.from_now
      user.itinerary_items.create product: another_product, starttime: 1.days.from_now, endtime: 2.days.from_now
      another_user.itinerary_items.create product: product, starttime: 1.days.from_now, endtime: 2.days.from_now
      outher_user.itinerary_items.create product: product, starttime: 1.days.from_now, endtime: 2.days.from_now
    end

    let(:another_product) do
      create :product, feed_id: 'another'
    end

    let(:another_user) do
      create(:user)
    end

    let(:outher_user) do
      create(:user)
    end

    let(:outher_users_token) do
      create(:authentication_token, user: outher_user)
    end

    let(:group) do
      create :group, users: [user.id, another_user.id]
    end

    let(:params) { {token: token.token, users: [user.id, another_user.id].to_s} }

    context 'without developer key' do
      it "should be an unauthorized call" do
        get "/api/itinerary", params
        response.status.should == 401
      end
    end
    context 'with developer key' do
      context 'with invalid parameters' do
        context 'invalid token' do
          specify 'should return 401 if token is missing' do
            get "/api/itinerary", params.except(:token), developer_header
            response.status.should == 401
          end
          specify 'should return 401 if token is invalid' do
            get "/api/itinerary", params.merge(token: 'asdasd'), developer_header
            response.status.should == 401
          end
        end
        context 'invalid users' do
          specify 'should return 500 if users are missing' do
            get "/api/itinerary", params.except(:users), developer_header
            response.status.should == 500
          end
        end
      end
      context 'with valid parameters' do
        specify 'should return 200' do
          get "/api/itinerary", params, developer_header
          response.status.should == 200
        end
        specify 'should return the right structure' do
          get "/api/itinerary", params, developer_header
          parsed_json = JSON.parse(response.body)
          expect(parsed_json).to be_an(Array)
          expect(parsed_json.size).to eq(2)
          expect(parsed_json[0]).to be_a(Hash)
          expect(parsed_json[0]['user_id'].to_i).to eq(user.id)
          expect(parsed_json[0]['products']).to be_an(Array)
          expect(parsed_json[0]['products'][0]).to be_a(Hash)
          expect(parsed_json[0]['products'][0]['product_id']).to eq(product.id)
          expect(parsed_json[0]['products'][1]['product_id']).to eq(another_product.id)
        end
      end
    end
  end
end
