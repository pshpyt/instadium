require 'spec_helper'

describe 'api/ping' do
  describe 'GET /api/ping' do
    before(:each) do
      @api_key = ApiKey.create
      @developer_header = {'Authorization' => @api_key.token}
    end

    it "should return a 200 if the API is available" do
      get "/api/ping_dev", nil, @developer_header
      response.should be_ok
    end
    it "should return a 401 without developer key" do
      get "/api/ping_dev"
      expect(response.status).to eql(401)
    end
  end
end