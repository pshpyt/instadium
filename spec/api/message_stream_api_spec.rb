require 'spec_helper'

describe '/api/messages' do
  let(:api_key) { ApiKey.create }
  let(:developer_header){{'Authorization' => api_key.token}}
  let(:user) do
    create :user
  end
  let(:another_user) do
    create :user
  end
  let(:outher_user) do
    create :user
  end
  let(:token) { create :authentication_token, user: user }
  let(:group) do
    create :group, users: [user.id, another_user.id]
  end
  let(:outer_group) do
    create :group, users: [outher_user.id]
  end

  specify{expect(InStadium::MessagesApi).not_to be_nil}

  context 'Post a message' do
    def call_endpoint *params
      post "/api/messages", *params
    end

    let(:params) { {token: token.token, group_id: group.id, message: 'Hello world!'} }
    let(:extended_params) do
      #TODO figure out the attachment
      params.merge timelimit: 2.days.from_now
    end

    context 'without developer key' do
      it "should be an unauthorized call" do
        call_endpoint params
        expect(response.status).to eq(401)
      end
    end
    context 'with developer key' do
      context 'with invalid parameters' do
        context 'invalid token' do
          specify 'should return 401 if token is missing' do
            call_endpoint params.except(:token), developer_header
            expect(response.status).to eq(401)
          end
          specify 'should return 401 if token is invalid' do
            call_endpoint params.merge(token: 'asdasd'), developer_header
            expect(response.status).to eq(401)
          end
        end
        context 'invalid groupid' do
          specify 'should return 500 if groupid is missing' do
            call_endpoint params.except(:group_id), developer_header
            expect(response.status).to eq(500)
          end
          specify 'should return 401 if group_id identifies a group the user is not part of' do
            call_endpoint params.merge(group_id: outer_group.id), developer_header
            expect(response.status).to eq(401)
          end
        end
        context 'invalid message' do
          specify 'should return 500 if message is missing' do
            call_endpoint params.except(:message), developer_header
            expect(response.status).to eq(500)
          end
        end
      end
      context 'valid parameters' do
        specify 'status should be 201' do
          call_endpoint params, developer_header
          expect(response.status).to eq(201)
        end
        specify 'The message should be created' do
          expect do
            call_endpoint params, developer_header
          end.to change{group.messages.count}.by(1)
        end
      end
    end
  end

  context 'Retrieve messages' do
    def call_endpoint *params
      get "/api/messages", *params
    end

    let(:params) { {token: token.token, group_id: group.id} }
    let(:extended_params) do
      params.merge count: 1, offset: 2
    end

    context 'without developer key' do
      it "should be an unauthorized call" do
        call_endpoint params
        expect(response.status).to eq(401)
      end
    end
    context 'with developer key' do
      context 'with invalid parameters' do
        context 'invalid token' do
          specify 'should return 401 if token is missing' do
            call_endpoint params.except(:token), developer_header
            expect(response.status).to eq(401)
          end
          specify 'should return 401 if token is invalid' do
            call_endpoint params.merge(token: 'asdasd'), developer_header
            expect(response.status).to eq(401)
          end
        end
        context 'invalid groupid' do
          specify 'should return 500 if groupid is missing' do
            call_endpoint params.except(:group_id), developer_header
            expect(response.status).to eq(500)
          end
          specify 'should return 401 if group_id identifies a group the user is not part of' do
            call_endpoint params.merge(group_id: outer_group.id), developer_header
            expect(response.status).to eq(401)
          end
        end
      end
      context 'valid parameters' do
        before(:each) do
          group.messages.create message: 'Hw'
          group.messages.create message: 'Hello World'
          group.messages.create message: 'Hi Gentlemans'
        end
        specify 'status should be 202' do
          call_endpoint params, developer_header
          expect(response.status).to eq(200)
        end
        specify 'Two messages should be retrieved' do
          call_endpoint params, developer_header
          parsed_json = JSON.parse(response.body)
          expect(parsed_json).to be_an(Array)
          expect(parsed_json.size).to eq(3)
          expect(parsed_json[0]).to be_a(Hash)
          expect(parsed_json[0]['message']).to eq('Hw')
          expect(parsed_json[1]['message']).to eq('Hello World')
        end
        specify 'The last message should be retrieved' do
          call_endpoint extended_params, developer_header
          parsed_json = JSON.parse(response.body)
          expect(parsed_json).to be_an(Array)
          expect(parsed_json.size).to eq(1)
          expect(parsed_json[0]['message']).to eq('Hi Gentlemans')
        end
      end
    end
  end

end
