require 'spec_helper'

describe '/api/users' do
  let(:api_key) { ApiKey.create }
  let(:developer_header) { {'Authorization' => api_key.token} }
  let(:user) { User.create!(email: 'sm@gmail.com', password: SecureRandom.hex(8), full_name: SecureRandom.hex(8), username: SecureRandom.hex(8), confirmed_at: Time.now) }

  context 'without developer key' do
    it "should be a denied call" do
      get "/api/users/#{user.id}"
      response.status.should == 401
    end
  end

  context 'POST /api/users/:id/resend_confirmation' do
    context 'non existing user' do
      it 'returns a 404 response' do
        post "/api/users/1/resend_confirmation", nil, developer_header
        response.status.should == 404
      end
    end
    context 'already confirmed user' do
      it 'returns a 404 response' do
        u = create :user, confirmed_at: Time.now
        post "/api/users/#{u.id}/resend_confirmation", nil, developer_header
        response.status.should == 403
      end
    end
    context 'unconfirmed user' do
      it 'returns a 200 response' do
        # u = create :user, confirmed_at: nil
        # expect_any_instance_of(User).to receive(:send_confirmation_instructions)
        # post "/api/users/#{u.id}/resend_confirmation", nil, developer_header
        # response.status.should == 200
      end
    end
  end

  context 'GET /api/users/:id' do
    context "for a user id that doesn't correspond to a user" do
      it "returns a 404 response" do
        get "/api/users/1001", nil, developer_header
        response.status.should == 404
      end
    end

    context "for a user id that corresponds to a user" do
      before do
        @email = "#{SecureRandom.hex(8)}@example.org"
        user = User.create!(email: @email, password: SecureRandom.hex(8), full_name: SecureRandom.hex(8), username: SecureRandom.hex(8), confirmed_at: Time.now)
      end

      it "returns a 200 response and the appropriate user data" do
        get "/api/users/#{user.id}", nil, developer_header
        response.should be_ok
        parsed_json = JSON.parse(response.body)
        api_should_be_a_detailed_user(parsed_json, user)
      end

      context "when authenticated" do
        before do
          @email = "#{SecureRandom.hex(8)}@example.org"
          user = User.create!(email: @email, password: SecureRandom.hex(8), username: SecureRandom.hex(8), confirmed_at: Time.now)
          @token = AuthenticationToken.create_token_for_user(user)
        end

        it "returns a 200 response and the appropriate user data" do
          get "/api/users/#{user.guid}", { 'token' => @token.token }, developer_header
          response.should be_ok
          parsed_json = JSON.parse(response.body)
          api_should_be_a_detailed_user(parsed_json, user)
        end
      end
    end
  end
end