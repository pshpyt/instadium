require 'spec_helper'

describe '/api/userlocation' do
  let(:api_key) { ApiKey.create }
  let(:developer_header){{'Authorization' => api_key.token}}
  let(:user) do
    create :user
  end
  let(:token) { create :authentication_token, user: user }

  specify{expect(InStadium::UserLocationApi).not_to be_nil}

  context 'update location' do
    def call_endpoint *params
      post "/api/userlocation", *params
    end

    let(:new_latitude) { '1.4' }
    let(:new_longitude) { '1.2' }
    let(:params) { {token: token.token, longitude: new_longitude, latitude: new_latitude} }

    context 'without developer key' do
      it "should be an unauthorized call" do
        call_endpoint params
        expect(response.status).to eq(401)
      end
    end
    context 'with developer key' do
      context 'with invalid parameters' do
        context 'invalid token' do
          specify 'should return 401 if token is missing' do
            call_endpoint params.except(:token), developer_header
            expect(response.status).to eq(401)
          end
          specify 'should return 401 if token is invalid' do
            call_endpoint params.merge(token: 'asdasd'), developer_header
            expect(response.status).to eq(401)
          end
        end
        context 'invalid coordinates' do
          specify 'should return 500 if longitude is missing' do
            call_endpoint params.except(:longitude), developer_header
          end
          specify 'should return 500 if latitude is missing' do
            call_endpoint params.except(:latitude), developer_header
          end
        end
      end
      context 'valid parameters' do
        specify 'status should be 201' do
          call_endpoint params, developer_header
          expect(response.status).to eq(201)
        end
        specify 'The longitude should be updated' do
          call_endpoint params, developer_header
          expect(user.reload.longitude).to eq(new_longitude)
        end
        specify 'The latitude should be updated' do
          call_endpoint params, developer_header
          expect(user.reload.latitude).to eq(new_latitude)
        end
        specify 'The location last update time should be changed' do
          expect do
            call_endpoint params, developer_header
          end.to change{user.reload.location_last_update_time}
        end
      end
    end
  end
end