require 'spec_helper'

describe '/api/products' do
  let(:api_key) { ApiKey.create }
  let(:developer_header) { {'Authorization' => api_key.token} }

  def api_call *params
    get "/api/feed/products", *params
  end

  context 'without developer key' do
    specify "should be a denied call" do
      api_call
      response.status.should == 401
    end
  end

  context 'with developer key' do
    before(:each) do
      @pr1 = create :product
      @pr2 = create :product
    end
    specify "should be a successfull call" do
      api_call nil, developer_header
      response.status.should == 200
      parsed = JSON.parse(response.body)
      expect(parsed).to be_an(Array)
      expect(parsed.count).to eq(2)
    end
    specify "should filter the products by id" do
      params = {product_ids: [@pr1.id].to_s}
      api_call params, developer_header
      response.status.should == 200
      parsed = JSON.parse(response.body)
      expect(parsed).to be_an(Array)
      expect(parsed.count).to eq(1)
    end
  end

end