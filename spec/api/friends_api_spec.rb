require 'spec_helper'

describe '/api/friends' do
  let(:api_key) { ApiKey.create }
  let(:developer_header){ {'Authorization' => api_key.token} }
  let(:user) do
    create :user
  end
  let(:another_email) { 'another_email@gmail.com' }
  let(:another_user) do
    create :user, email: another_email
  end
  let(:facebook_user) do
    create :user
  end
  let(:token) { create :authentication_token, user: user }

  specify{expect(InStadium::FriendsApi).not_to be_nil}

  context 'retrieving friends' do
    def call_endpoint *params
      get "/api/friends", *params
    end

    let(:params) { {token: token.token} }
    let(:extended_params) { params.merge emails: ['nonfriend@gmail.com', another_email].to_s }

    context 'without developer key' do
      it "should be an unauthorized call" do
        call_endpoint params
        response.status.should == 401
      end
    end
    context 'with developer key' do
      context 'with invalid parameters' do
        context 'invalid token' do
          specify 'should return 401 if token is missing' do
            call_endpoint params.except(:token), developer_header
            response.status.should == 401
          end
          specify 'should return 401 if token is invalid' do
            call_endpoint params.merge(token: 'asdasd'), developer_header
            response.status.should == 401
          end
        end
      end
      context 'with valid parameters' do
        before(:each) do
          allow_any_instance_of(GetGoogleFriends).to receive(:execute).and_return([])
        end
        context 'querying just the social friends' do
          specify 'should return 200' do
            call_endpoint params, developer_header
            expect(response.status).to eq(200)
          end
          specify 'should return the facebook friends' do
            app_id, app_secret = GetFacebookEntry.new.execute
            test_users = Koala::Facebook::TestUsers.new(:app_id => app_id, :secret => app_secret)

            facebook_user.follow user
            user.follow facebook_user
            si = create :social_identity, user: facebook_user, uid: test_users.list[1]['id']

            token = test_users.list[0]['access_token']
            create :social_identity, user: user, token: token
            call_endpoint params, developer_header
            parsed_json = JSON.parse(response.body)
            expect(parsed_json).to be_an(Array)
            expect(parsed_json.size).to eq(1)
            expect(parsed_json[0]).to be_a(Hash)
            expect(parsed_json[0]['user_id']).not_to be_nil
            expect(parsed_json[0]['avatar_url']).not_to be_nil
            expect(parsed_json[0]['first_name']).not_to be_nil
            expect(parsed_json[0]['last_name']).not_to be_nil
            expect(parsed_json[0]['provider']).not_to be_nil
          end
        end
        context 'querying the social friends + checking friends by email' do
          specify 'should check the additional friends too' do
            allow_any_instance_of(GetFacebookFriends).to receive(:execute).and_return([])
            another_user.follow user
            user.follow another_user
            call_endpoint extended_params, developer_header
            parsed_json = JSON.parse(response.body)
            expect(parsed_json).to be_an(Array)
            expect(parsed_json.size).to eq(1)
            expect(parsed_json[0]).to be_a(Hash)
            expect(parsed_json[0]['first_name']).to eq(another_user.first_name)
            expect(parsed_json[0]['last_name']).to eq(another_user.last_name)
          end
        end
      end
    end
  end
end
