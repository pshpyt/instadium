require 'spec_helper'

describe '/api/user_data' do
  before(:each) do
    @api_key = ApiKey.create
    @developer_header = {'Authorization' => @api_key.token}
    @ud = UserData.create key: 'asd'
    @params = {key: @ud.key}
  end

  context 'without developer key' do
    it "should be a denied call" do
      get "/api/user_data", @params
      response.status.should == 401
    end
  end

  context 'with developer key' do
    it "should be a successfull call" do
      get "/api/user_data", @params, @developer_header
      response.status.should == 200
    end
  end

end