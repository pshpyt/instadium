require 'spec_helper'

describe '/api/popular_products' do

  let(:api_key) { ApiKey.create }
  let(:developer_header){{'Authorization' => api_key.token}}
  let(:user) do
    create :user
  end
  let(:token) { create :authentication_token, user: user }

  specify{expect(InStadium::PopularProductsApi).not_to be_nil}

  context 'querying' do
    def call_endpoint *params
      get "/api/popular_products", *params
    end

    context 'without developer key' do
      specify "should be an unauthorized call" do
        call_endpoint
        expect(response.status).to eq(401)
      end
    end
    context 'with developer key' do
      before(:each) do
        create :product, usage: 2
        create :product, usage: 1
      end
      specify "should be return 200" do
        call_endpoint nil, developer_header
        expect(response.status).to eq(200)
      end
      specify "should return the products in descending usage order" do
        call_endpoint nil, developer_header
        parsed_json = JSON.parse(response.body)
        expect(parsed_json).to be_an(Array)
        expect(parsed_json.count).to eq(2)
        expect(parsed_json[0]['usage']).to eq(2)
        expect(parsed_json[1]['usage']).to eq(1)
      end
    end
  end
end
