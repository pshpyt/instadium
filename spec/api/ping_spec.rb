require 'spec_helper'

describe 'api/ping' do
  describe 'GET /api/ping' do
    it "should return a 200 if the API is available" do
      get "/api/ping"
      response.should be_ok
    end
  end
end