require 'spec_helper'

describe '/api/me' do
  before(:each) do
    @api_key = ApiKey.create
    @developer_header = {'Authorization' => @api_key.token}
  end

  context "me endpoints" do
    let(:user) do
      create(:user)
    end
    let(:token) { create(:authentication_token, user: user) }

    before :each do
      @request = { token: token.token }
    end

    context 'without developer key' do
      it "should be a successfull call" do
        get "/api/me", @request
        response.status.should == 401
      end
    end

    context "me" do
      it "should return 401 without token" do
        get "/api/me", nil, @developer_header
        response.status.should == 401
      end

      it "should be successful" do
        get "/api/me", @request, @developer_header
        response.status.should == 200
      end

      it "should have user entity" do
        get "/api/me", @request, @developer_header
        parsed_json = JSON.parse(response.body)
        api_should_be_a_me_user(parsed_json, user)
      end

      context 'with friends' do
        before(:each) do
          @invited = create :user
          user.follow @invited
          
          @mutual_friend = create :user
          user.follow @mutual_friend
          @mutual_friend.follow user
          
          @they_invited_me = create :user
          @they_invited_me.follow user
        end
        it "should contains the friends" do
          get "/api/me", @request, @developer_header
          parsed_json = JSON.parse(response.body)
          expect(parsed_json["friends"]).to be_present
          expect(parsed_json["friends"]["mutual"]).to eq([@mutual_friend.id])
          expect(parsed_json["friends"]["invited"]).to eq([@invited.id])
          expect(parsed_json["friends"]["invitations"]).to eq([@they_invited_me.id])
        end
      end
    end

    context 'friend' do
      let(:friend) { create :user }
      context 'specifying an id' do
        it "should return 404 if friend cannot be found" do
          post "/api/me/friend", @request.merge!(friend: 100001), @developer_header
          response.status.should == 404
        end
        it "should return 201 if friend is found" do
          post "/api/me/friend", @request.merge!(friend: friend.id), @developer_header
          response.status.should == 201
        end
        it "should make the friend relationship" do
          post "/api/me/friend", @request.merge!(friend: friend.id), @developer_header
          expect(user.follows_by_type('User').first.followable).to eq(friend)
        end
      end
      context 'specifying an email address' do
        context "user with the email doesn't exists" do
          let(:email){'new_users_email@gmail.com'}
          it "should be a valid request" do
            post "/api/me/friend", @request.merge!(friend: email), @developer_header
            response.status.should == 201
          end
          it "should create a new user" do
            expect do
              post "/api/me/friend", @request.merge!(friend: email), @developer_header
            end.to change{User.count}.by(1)
          end
          it "should not confirm the new user" do
            post "/api/me/friend", @request.merge!(friend: email), @developer_header
            expect(User.last.confirmed?).to be_true
          end
          it "should follow the new user" do
            post "/api/me/friend", @request.merge!(friend: email), @developer_header
            expect(user.following_by_type('User')).to include(User.last)
          end
          it 'should send an email' do
            # ActionMailer::Base.deliveries.last.to.should == [email]
          end
        end
        context "user with the email exists" do
          before do
            @invited = create :user
          end
          it "should be a successfull request" do
            post "/api/me/friend", @request.merge!(friend: @invited.email), @developer_header
            response.status.should == 201
          end
          it "should return 201" do
            post "/api/me/friend", @request.merge!(friend: @invited.email), @developer_header
            expect(user.following_by_type('User')).to include(User.last)
          end
        end
      end
    end

    context 'unfriend' do
      let(:friend) { create :user }
      it "should return 404 if friend can not be found" do
        delete "/api/me/unfriend", @request.merge!(friend_id: 100001), @developer_header
        response.status.should == 404
      end
      context 'existing friend' do
        before(:each) do
          user.follow friend
          friend.follow user
        end
        it "should return 200 if friend is found" do
          delete "/api/me/unfriend", @request.merge!(friend_id: friend.id), @developer_header
          response.status.should == 200
        end
        it "should make the friend relationship" do
          delete "/api/me/unfriend", @request.merge!(friend_id: friend.id), @developer_header
          expect(user.follows_by_type('User')).to be_empty
        end
      end
    end

    context "avatar" do
      let(:image) { Rack::Test::UploadedFile.new(test_image_path('test_avatar.jpg')) }

      it "should return 401 without token" do
        post "/api/me/avatar", nil, @developer_header
        response.status.should == 401
      end

      it "should return 200" do
        post "/api/me/avatar", {token: token.token, :use_social_identity_avatar => false, :avatar => image}, @developer_header
        response.status.should == 200
      end
    end

    context "dob and zip" do
      it "should return 401 without dob" do
        post "/api/me/set_dob", nil, @developer_header
        response.status.should == 401
      end

      it "should be successful" do
        post "/api/me/set_dob", @request.merge!(dob: "1974/12/07"), @developer_header
        response.status.should == 200
      end

      it "should return 401 without zip" do
        post "/api/me/set_zip", nil, @developer_header
        response.status.should == 401
      end

      it "should be successful" do
        post "/api/me/set_zip", @request.merge!(zip: "01742"), @developer_header
        response.status.should == 200
      end
    end

    context "password change" do
      it "should return 401 without token" do
        post "/api/me/set_password", nil, @developer_header
        response.status.should == 401
      end
      it "should be successfull" do
        post "/api/me/set_password", @request.merge!(new_password: "new_password"), @developer_header
        response.status.should == 200
      end
    end

    context 'update' do
      let(:user) { create(:user) }
      let(:token) { create(:authentication_token, user: user) }

      context 'invalid token' do
        it "should return 404" do
          post "/api/me/update", {token: 'non_existent'}, @developer_header
          response.status.should == 401
        end
      end
      context 'valid token' do
        it "should return 200" do
          new_zip = '005352'
          post "/api/me/update", {token: token.token, zip: new_zip}, @developer_header
          response.status.should == 200
          u = AuthenticationToken.find_user_by_token_value token.token
          expect(u.zip).to eq(new_zip)
        end
      end
    end
  end
end