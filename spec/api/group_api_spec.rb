require 'spec_helper'

describe '/api/group' do
  let(:api_key) { ApiKey.create }
  let(:developer_header) { {'Authorization' => api_key.token} }

  specify{expect(InStadium::GroupApi).not_to be_nil}

  let(:user) { create(:user) }
  let(:token) { create(:authentication_token, user: user) }

  context 'without developer key' do
    let(:params) { {token: token.token} }
    it "should be a successfull call" do
      get "/api/groups", params
      response.status.should == 401
    end
  end

  context 'group creation' do
    let(:mate1) { create :user }
    let(:mate2) { create :user }
    let(:params) do
      {name: 'group name', description: 'group description', token: token.token, user_ids: [mate1.id, mate2.id].to_s }
    end
    def api_call *params
      post "/api/groups", *params
    end
    context 'unauthenticated' do
      specify "should return 401 without token" do
        api_call nil, developer_header
        response.status.should == 401
      end
    end
    context 'authenticated' do
      context 'every param is provided' do
        specify "the request should be a valid one" do
          api_call params, developer_header
          response.status.should == 201
        end
        specify "should create a new group" do
          expect do
            api_call params, developer_header
          end.to change{Group.count}.by(1)
        end
        specify "should return the right format" do
          api_call params, developer_header
          parsed_json = JSON.parse(response.body)
          expect(parsed_json['name']).to eql(params[:name])
          expect(parsed_json['description']).to eql(params[:description])
          expect(parsed_json['members']).to be_an(Array)
          expect(parsed_json['pending_members']).to be_an(Array)
        end
        specify "should set the caller as user" do
          api_call params, developer_header
          expect(Group.last.users).to include(user.id)
        end
        specify "shouldn't set the caller as unconfirmed_user" do
          api_call params, developer_header
          expect(Group.last.unconfirmed_users).not_to include(user.id)
        end
        specify "should set the group attributes" do
          api_call params, developer_header
          expect(Group.last.name).to eq(params[:name])
          expect(Group.last.description).to eq(params[:description])
        end
        specify "should set the unconfirmed users" do
          api_call params, developer_header
          expect(Group.last.unconfirmed_users).to include(mate1.id, mate2.id)
        end
        specify "should not set the unconfirmed users duplicated" do
          api_call params.merge({user_ids: [mate1.id, mate2.id, mate1.id, mate1.id].to_s}), developer_header
          expect(Group.last.unconfirmed_users.sort).to eq([mate1.id, mate2.id].sort)
        end
      end
    end
  end

  context 'checking group request' do
    let(:group) { create :group, unconfirmed_users: [user.id] }
    let(:params) { { token: token.token } }
    def api_call *params
      get "/api/groups/check_request", *params
    end
    context 'unauthenticated' do
      specify "should return 401 without token" do
        api_call nil, developer_header
        response.status.should == 401
      end
    end
    context 'authenticated' do
      context 'every param is provided' do
        specify 'Should return 200' do
          api_call params, developer_header
          response.status.should == 200
        end
        specify "List the group id's into which the user is invited into" do
          g = group
          api_call params, developer_header
          result = JSON.parse(response.body)
          expect(result).to be_an(Array)
          expect(result.count).to eq(1)
          expect(result[0]['id']).to eq(group.id)
        end
      end
    end
  end

  context 'accepting group request' do
    let(:group) { create :group, unconfirmed_users: [user.id] }
    let(:params) do
      { token: token.token, id: group.id }
    end
    def api_call *params
      post "/api/groups/#{params.first[:id]}/accept_request", *params
    end
    context 'unauthenticated' do
      specify "should return 401 without token" do
        api_call params.except(:token), developer_header
        response.status.should == 401
      end
    end
    context 'authenticated' do
      context 'every param is provided' do
        specify "Should return 201" do
          api_call params, developer_header
          response.status.should == 201
        end
        specify "Move the user from unconfirmed to confirmed" do
          api_call params, developer_header
          expect(Group.last.unconfirmed_users).not_to include(user.id)
          expect(Group.last.users).to include(user.id)
        end
      end
    end
  end

  context 'query group data' do
    context 'unauthenticated' do
      specify "should return 401 without token" do
        get "/api/groups", nil, developer_header
        expect(response.status).to eq(401)
      end
    end
    context 'authenticated' do
      let(:unconfirmed) { create :user }
      let(:group) {create :group, users: [user.id], unconfirmed_users: [unconfirmed.id]}
      let(:params) { {token: token.token, ids: [group.id].to_s} }
      specify 'should be a successfull call' do
        get "/api/groups", params, developer_header
        expect(response.status).to eq(200)
      end
      specify 'should return the group' do
        get "/api/groups", params, developer_header
        parsed_json = JSON.parse(response.body)
        expect(parsed_json).to be_an(Array)
        expect(parsed_json.size).to eq(1)
      end
    end
  end

  context 'remove members from groups' do
    let(:mygroup) { create :group, name: 'jeijj', description: 'descr', users: [user.id] }
    let(:othersgroup) { create :group, name: 'jeijj', description: 'descr', users: [1001] }
    let(:unconfirmed_group) { create :group, name: 'jeijj', description: 'descr', users: [1001], unconfirmed_users: [user.id] }
    let(:object_user) { create :user }
    let(:params) { {token: token.token, user_ids: [object_user.id].to_s, id: mygroup.id} }

    def api_call *params
      delete "/api/groups/#{params.first[:id]}/users", *params
    end

    context 'unauthenticated' do
      specify "should return 401 without token" do
        api_call params.except(:token), developer_header
        expect(response.status).to eq(401)
      end
    end
    context 'authenticated' do
      context 'the user is not part of the group' do
        specify "should not be authorized" do
          api_call params.merge( id: othersgroup.id ), developer_header
          expect(response.status).to eq(401)
        end
      end
      context 'the user is not part of unconfirmed_users group' do
        specify "should not be authorized" do
          api_call params.merge( id: othersgroup.id ), developer_header
          expect(response.status).to eq(401)
        end
      end
      context 'the user is part of the group' do
        context 'invalid params' do
          specify "should be an invalid request if the object user's id are missing" do
            api_call params.except(:user_ids), developer_header
            expect(response.status).to eq(500)
          end
        end
        context 'valid params' do
          specify "should not change the users if the object user's id are invalid" do
            p = params
            expect do
              api_call p.merge(user_ids: [1001].to_s), developer_header
            end.not_to change{Group.last.users}
          end
          specify "should not change the unconfirmed users if the object user's id are invalid" do
            p = params
            expect do
              api_call p.merge(user_ids: [1001].to_s), developer_header
            end.not_to change{Group.last.unconfirmed_users}
          end
          specify "should be a valid request" do
            api_call params, developer_header
            expect(response.status).to eq(200)
          end
          specify "should remove the target user id from the users of the group" do
            api_call params, developer_header
            parsed_json = JSON.parse(response.body)
            expect(parsed_json["users"]).not_to include(object_user.id)
          end
          specify "should remove the target user id from the users of the group" do
            api_call params, developer_header
            parsed_json = JSON.parse(response.body)
            expect(parsed_json["users"]).not_to include(object_user.id)
          end
          specify "should remove the target user id from the unconfirmed_users of the group" do
            api_call params, developer_header
            parsed_json = JSON.parse(response.body)
            expect(parsed_json["users"]).not_to include(object_user.id)
          end
        end
      end
    end
  end

  context 'add members to groups' do
    let(:mygroup) { create :group, name: 'jeijj', description: 'descr', users: [user.id] }
    let(:othersgroup) { create :group, name: 'jeijj', description: 'descr', users: [1001], unconfirmed_users: [2000] }
    let(:my_other_group) { create :group, name: 'jeijj', description: 'descr', users: [user.id, object_user2.id], unconfirmed_users: [object_user.id] }
    let(:object_user) { create :user }
    let(:object_user2) { create :user }
    let(:params) { {token: token.token, user_ids: [object_user.id, object_user2.id].to_s, id: mygroup.id} }

    def api_call *params
      post "/api/groups/#{params.first[:id]}/users", *params
    end

    context 'unauthenticated' do
      specify "should return 401 without token" do
        api_call params.except(:token), developer_header
        expect(response.status).to eq(401)
      end
    end
    context 'authenticated' do
      context 'the user is not part of the group' do
        specify "should not be authorized" do
          api_call params.merge( id: othersgroup.id ), developer_header
          expect(response.status).to eq(401)
        end
      end
      context 'the user is part of the group' do
        context 'invalid params' do
          specify "should be an invalid request if the group is missing" do
            api_call params.merge(id: 1001), developer_header
            expect(response.status).to eq(400)
          end
        end
        context 'valid params' do
          specify "should be an invalid request if any of the object users ids are missing" do
            api_call params.except(:user_ids), developer_header
            expect(response.status).to eq(500)
          end
          specify "should be a valid request" do
            api_call params, developer_header
            expect(response.status).to eq(201)
          end
          specify "should add all the target user id to the users of the group" do
            api_call params, developer_header
            parsed_json = JSON.parse(response.body)
            expect(parsed_json["unconfirmed_users"]).to include(object_user.id)
            expect(parsed_json["unconfirmed_users"]).to include(object_user2.id)
          end
          specify "should not add the users which are not in the system" do
            api_call params.merge(user_ids: [object_user.id, object_user2.id, 1001].to_s), developer_header
            parsed_json = JSON.parse(response.body)
            expect(parsed_json["unconfirmed_users"]).not_to include(1000)
          end
          specify "should not add the a user twice to the unconfirmed ones" do
            api_call params.merge( id: my_other_group.id, user_ids: [object_user.id].to_s), developer_header
            parsed_json = JSON.parse(response.body)
            expect(parsed_json["unconfirmed_users"]).to eq([object_user.id])
          end
          specify "should not add the user as unconfirmed if the user is added already and is confirmed" do
            api_call params.merge( id: my_other_group.id, user_ids: [object_user.id, object_user2.id].to_s), developer_header
            parsed_json = JSON.parse(response.body)
            expect(parsed_json["unconfirmed_users"]).not_to include(object_user2.id)
            expect(parsed_json["unconfirmed_users"]).to include(object_user.id)
          end
          specify "should add the target user id to the group at db level" do
            api_call params, developer_header
            group = Group.find mygroup.id
            expect(group.unconfirmed_users).to include(object_user.id)
          end
        end
      end
    end
  end
end
