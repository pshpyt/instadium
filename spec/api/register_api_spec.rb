require 'spec_helper'

describe '/api/register' do

  before(:each) do
    @api_key = ApiKey.create
    @developer_header = {'Authorization' => @api_key.token}
  end

  context 'without developer key' do
    before do
      @email = "#{SecureRandom.hex(8)}@example.org"
      @username = "#{SecureRandom.hex(8)}"

      @required_params = {
        :email => @email,
        :username => @username,
        :password => "testpass"
      }
    end

    it "should be a denied call" do
      post "/api/register/email", @required_params
      response.status.should == 401
    end
  end

  describe 'POST /api/register/email' do
    context "with missing params" do
      before do
        @email = "#{SecureRandom.hex(8)}@example.org"

        @required_params = {
          :email => @email,
          :password => "testpass"
        }
      end
      [:email, :password].each do |s|
        context "when the #{s} parameter is blank" do
          before do
            @required_params[s] = ""
          end

          it "should return a 400" do
            post "/api/register/email", @required_params, @developer_header
            response.status.should == 400
          end
        end

        context "when the #{s} parameter is nil" do
          before do
            @required_params[s] = nil
          end

          it "should return a 400" do
            post "/api/register/email", @required_params, @developer_header
            response.status.should == 400
          end
        end

      end
    end

    context "with minimal valid params" do
      before do
        @email = "#{SecureRandom.hex(8)}@example.org"
        @username = "#{SecureRandom.hex(8)}"

        @required_params = {
          :email => @email,
          :username => @username,
          :password => "testpass"
        }
      end

      it "should create a user" do
        lambda {
          post "/api/register/email", @required_params, @developer_header
        }.should change(User, :count).by(1)
      end

      it "should create an unconfirmed user" do
        post "/api/register/email", @required_params, @developer_header
      end

      it "should return a 201" do
        post "/api/register/email", @required_params, @developer_header
        response.status.should == 201
      end

      it "should create a unconfirmed user" do
        post "/api/register/email", @required_params, @developer_header
        user = User.last
        expect(user.confirmed?).to be_true
      end

      it "should return a token" do
        post "/api/register/email", @required_params, @developer_header
        parsed_json = JSON.parse(response.body)
        parsed_json["token"].should_not be_nil
      end

      it "should return the" do
        post "/api/register/email", @required_params, @developer_header
        parsed_json = JSON.parse(response.body)
        api_should_be_a_me_user(parsed_json["me"], User.last)
      end
    end

    context "with additional params" do
      before do
        @email = "#{SecureRandom.hex(8)}@example.org"
        @required_params = {
          :email => @email,
          :username => @username,
          :password => "testpass",
          :full_name => "Full Name",
          :bio => "I have lived a life"
        }
      end

      it "should fill the attributes" do
        post "/api/register/email", @required_params, @developer_header
        user = User.last
        user.bio.should == "I have lived a life"
        user.full_name.should == "Full Name"
      end
    end

    context "first and last name behavior" do
      before do
        @email = "#{SecureRandom.hex(8)}@example.org"
        @required_params = {
          :email => @email,
          :password => "testpass"
        }
        @first_name = "Ted"
        @last_name = "Mosby"
        @full_name = "Barney Stinson"
      end

      it "should set the full name to the first name if only the first name is provided" do
        @required_params[:first_name] = @first_name
        lambda {
          post "/api/register/email", @required_params, @developer_header
        }.should change(User, :count).by(1)
        response.status.should == 201

        user = User.last
        user.full_name.should == @first_name
        user.first_name.should == @first_name
      end

      it "should set the full name to the last name if only the last name is provided" do
        @required_params[:last_name] = @last_name
        lambda {
          post "/api/register/email", @required_params, @developer_header
        }.should change(User, :count).by(1)
        response.status.should == 201

        user = User.last
        user.full_name.should == @last_name
        user.last_name.should == @last_name
      end

      it "should set the full name to the combined first and last name if both are provided" do
        @required_params[:first_name] = @first_name
        @required_params[:last_name] = @last_name
        lambda {
          post "/api/register/email", @required_params, @developer_header
        }.should change(User, :count).by(1)
        response.status.should == 201

        user = User.last
        user.full_name.should == "#{@first_name} #{@last_name}"
        user.first_name.should == @first_name
        user.last_name.should == @last_name
      end

      it "should set the full name to the provided value if an explicit value is provided" do
        @required_params[:first_name] = @first_name
        @required_params[:last_name] = @last_name
        @required_params[:full_name] = @full_name
        lambda {
          post "/api/register/email", @required_params, @developer_header
        }.should change(User, :count).by(1)
        response.status.should == 201

        user = User.last
        user.full_name.should == @full_name
        user.first_name.should == @first_name
        user.last_name.should == @last_name
      end
    end

    context "avatar image" do
      before do
        @email = "#{SecureRandom.hex(8)}@example.org"
        @required_params = {
          :email => @email,
          :password => "testpass",
          :avatar => fixture_file_upload('images/test_avatar.jpg')
        }
      end

      it "should set the full name to the first name if only the first name is provided" do
        lambda {
          post "/api/register/email", @required_params, @developer_header
        }.should change(User, :count).by(1)
        response.status.should == 201

        user = User.last
        user.avatar.should_not be_nil
      end
    end

    context "with a duplicate email address" do
      before do
        @email = "#{SecureRandom.hex(8)}@example.org"
        @username = "#{SecureRandom.hex(8)}"
        @user = User.create!( email: @email, password: SecureRandom.hex(8), username: SecureRandom.hex(8), confirmed_at: Time.now)
        @required_params = {
          :email => @email,
          :username => @username,
          :password => "testpass"
        }
      end

      it "should return a 400, but return a status of false and not create a user" do
        post "/api/register/email", @required_params, @developer_header
        response.status.should == 400
        parsed_json = JSON.parse(response.body)
        parsed_json.should be_an_instance_of(Hash)
        parsed_json["error_type"].should_not be_nil
        parsed_json["error_type"].should == "account_exists"
      end
    end
  end

end
