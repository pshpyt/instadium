require 'spec_helper'

describe Presenters::DetailedUserPresenter do
  subject(:subject) { Presenters::DetailedUserPresenter }
  context 'class hierarchy' do
    specify{ expect(subject.ancestors).to include(Presenters::UserPresenter)}
  end
  context 'fields' do
    specify { expect(subject).to represent(:image_url) }
    specify { expect(subject).to represent(:cover_image_url) }
    specify { expect(subject).to represent(:email) }
    specify { expect(subject).to represent(:disabled) }
    specify { expect(subject).to represent(:zip) }
    specify { expect(subject).to represent(:dob) }
    specify { expect(subject).to represent(:created_at) }
    specify { expect(subject).to represent(:agree_on_email_marketing) }
    specify { expect(subject).to represent(:gender) }
    specify { expect(subject).to represent(:phone_number) }
    # specify { expect(subject).to represent(:friends) }
    # specify { expect(subject).to represent(:social_identities) }
  end
end
