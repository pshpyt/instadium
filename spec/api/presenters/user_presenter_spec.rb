require 'spec_helper'

describe Presenters::UserPresenter do
  describe 'fields' do
    subject(:subject) { Presenters::UserPresenter }
    specify { expect(subject).to represent(:id) }
    specify { expect(subject).to represent(:name) }
    specify { expect(subject).to represent(:username) }
    specify { expect(subject).to represent(:first_name) }
    specify { expect(subject).to represent(:last_name) }
    specify { expect(subject).to represent(:full_name) }
    # specify { expect(subject).to represent(:verified) }
    # it { should represent(:agree_on_email_marketing) }
    # it { should represent(:gender) }
    # it { should represent(:phone_number) }
  end
end
