require 'spec_helper'

describe Presenters::GroupPresenter do
  describe 'fields' do
    subject(:subject) { Presenters::GroupPresenter }
    specify { expect(subject).to represent(:name) }
    specify { expect(subject).to represent(:description) }
    specify { expect(subject).to represent(:id) }
  end
end
