require 'spec_helper'

describe Presenters::ProductPresenter do
  describe 'fields' do
    subject(:subject) { Presenters::ProductPresenter }
    it { should represent(:id) }
    it { should represent(:brand) }
    it { should represent(:entity_path) }
    it { should represent(:usage) }
    it { should represent(:market) }
    it { should represent(:market_rank) }
    it { should represent(:name) }
    it { should represent(:property_code) }
    it { should represent(:property_rank) }
    it { should represent(:category) }
    it { should represent(:created_at) }
    it { should represent(:updated_at) }
    it { should represent(:short_description) }
    it { should represent(:mass_relevance) }

    context 'special fields' do
      subject(:subject) { Presenters::ProductPresenter.new(product) }
      context 'coordinates' do
        context 'normal flow' do
          let(:latitude) {'12.23'}
          let(:longitude) {'23.32'}
          let(:product) { Product.create coordinates: {latitude: latitude, longitude: longitude} }
          specify{ expect(subject.as_json[:coordinates][:latitude]).to eq(latitude.to_f)}
          specify{ expect(subject.as_json[:coordinates][:longitude]).to eq(longitude.to_f)}
        end
        context 'special cases' do
          context 'latitude is nil' do
            let(:latitude) {nil}
            let(:longitude) {'23.32'}
            let(:product) { Product.create coordinates: {latitude: latitude, longitude: longitude} }
            specify{ expect(subject.as_json[:coordinates][:latitude]).to eq(0)}
            specify{ expect(subject.as_json[:coordinates][:longitude]).to eq(longitude.to_f)}
          end
          context 'coordinates nil' do
            let(:product) { Product.create coordinates: nil }
            specify{ expect(subject.as_json[:coordinates]).to eq({})}
          end
        end
      end

      context 'details' do
        let(:details_hash) do
          {dateranges: [{startDate:'12', endDate: '13'}]}
        end
        let(:expected_details) do
          {'dateranges' => [{'startDate' => '12', 'endDate' => '13'}]}
        end
        let(:product) { Product.create details: details_hash }
        specify do
          expect(subject.as_json[:details]).to eq(expected_details)
        end
        context 'details is nil' do
          let(:product) { Product.create details: nil }
          specify{ expect(subject.as_json[:details]).to eq({})}
        end
      end
    end

  end
end
