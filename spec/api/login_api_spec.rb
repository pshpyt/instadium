require 'spec_helper'

describe '/api/login' do
  
  let(:user) { FactoryGirl.create(:user, :admin) }
  before(:each) do
    @api_key = ApiKey.create
    @developer_header = {'Authorization' => @api_key.token}
  end

  describe 'GET /email' do
    before do
      @email = "#{SecureRandom.hex(8)}@example.org"
      @user = User.create!( email: @email, password: SecureRandom.hex(8), username: SecureRandom.hex(8), confirmed_at: Time.now )
      @params = {:login => @email}
    end

    context 'without developer key' do
      it "should be a successfull call" do
        get "/api/login/email", @params
        response.status.should == 401
      end
    end

    it "should return 404 if a no user exists with that email" do
      get "/api/login/email", @params.merge(login: "nouser@example.org"), @developer_header
      response.status.should == 404
    end

    it "should return 200 if a user exists with that email" do
      get "/api/login/email", @params, @developer_header
      response.should be_ok
    end
  end

  describe 'GET /forgot' do
    before do
      @email = "#{SecureRandom.hex(8)}@example.org"
      @user = User.create!( email: @email, password: SecureRandom.hex(8), username: SecureRandom.hex(8), confirmed_at: Time.now )
      @params = {:login => @email}
    end

    context 'without developer key' do
      it "should be a successfull call" do
        get "/api/login/forgot", @params
        response.status.should == 401
      end
    end

    it "should return 200 if a user exists with that email" do
      get "api/login/forgot", @params, @developer_header
      response.should be_ok
      ActionMailer::Base.deliveries.last.to.should == [@email]
    end

    it 'returns a 404 response if a no user exists with that email' do
      get "/api/login/forgot.json", @params.merge(login: "nouser@example.org"), @developer_header
      response.status.should == 404
    end
  end

  describe 'POST email' do
    before do
      @email = "#{SecureRandom.hex(8)}@example.org"
      @username = "#{SecureRandom.hex(8)}"
      @password = "Testt12345"
      @user = User.create!(:username => @username, :email => @email, :password => @password, username: @username, confirmed_at: Time.now)
      @params = { :login => @email, :password => @password}
    end

    context 'without developer key' do
      it "should be a successfull call" do
        post "/api/login/email", @params
        response.status.should == 401
      end
    end

    context "with an email and a correct password" do
      it "should succeed, and return the token as part of the response" do
        post "/api/login/email", @params, @developer_header
        response.should be_ok
        parsed_json = JSON.parse(response.body)
        parsed_json.should be_an_instance_of(Hash)
        parsed_json["token"].should_not be_nil
        api_should_be_a_me_user(parsed_json["me"], @user)
      end
    end

    context "with a username and a correct password" do
      it "should succeed, and return the token as part of the response" do
        post "/api/login/email", @params.merge( :login => @username), @developer_header
        response.should be_ok
        parsed_json = JSON.parse(response.body)
        parsed_json.should be_an_instance_of(Hash)
        parsed_json["token"].should_not be_nil
        api_should_be_a_me_user(parsed_json["me"], @user)
      end
    end

    context "with an email and an incorrect password" do
      it "should fail with a 401 and return an error message" do
        post "/api/login/email", @params.merge( :password => "invalid" ), @developer_header
        response.status.should == 401
        parsed_json = JSON.parse(response.body)
        parsed_json.should be_an_instance_of(Hash)
        parsed_json["error_type"] = "bad_password"
      end
    end

    context "with a username and an incorrect password" do
      it "should fail with a 401 and return an error message" do
        post "/api/login/email", @params.merge( :login => @username, :password => "invalid" ), @developer_header
        response.status.should == 401
        parsed_json = JSON.parse(response.body)
        parsed_json.should be_an_instance_of(Hash)
        parsed_json["error_type"] = "bad_password"
      end
    end

    context "with a non-existent login" do
      it "should fail with a 401 and return an error message" do
        post "/api/login/email", @params.merge( :login => "notreal", :password => "invalid" ), @developer_header
        response.status.should == 401
        parsed_json = JSON.parse(response.body)
        parsed_json.should be_an_instance_of(Hash)
        parsed_json["error_type"] = "no_user"
      end
    end
  end

  describe 'POST /facebook' do
    context 'without developer key' do
      before do
        @old_email = "#{SecureRandom.hex(8)}@example.org"
        @current_user = User.create!( email: @old_email, password: SecureRandom.hex(8), username: SecureRandom.hex(8) )
        @token = AuthenticationToken.create(:user => @current_user, expires_at: (Clock.now + 1.days))

        @uid = "test-fb-uid-#{SecureRandom.hex(4)}"
        @provider_username = "fb-username-#{SecureRandom.hex(4)}"
        @facebook_token = SecureRandom.hex(16)
        @email = "#{SecureRandom.hex(8)}@example.org"
        @facebook_params = {
          "id" => @uid,
          "username" => @provider_username,
          "name" => 'Not Real',
          "email" => @email
        }
        expect(Social::FacebookFacade).not_to receive(:get_identity_data)
      end
      it "should return a 401" do
        post "/api/login/facebook", { :facebook_token => @facebook_token, :token => @token.token }
        response.status.should == 401
      end
    end

    context 'When token is provided' do
      context 'When token matches another user' do
        before do
          @another_email = "#{SecureRandom.hex(8)}@example.org"
          @another_user = User.create!( email: @another_email, password: SecureRandom.hex(8), username: SecureRandom.hex(8) )
          @token_for_another_user = AuthenticationToken.create(:user => @another_user, expires_at: (Clock.now + 1.days))

          @uid = "test-fb-uid-#{SecureRandom.hex(4)}"
          @email = "#{SecureRandom.hex(8)}@example.org"
          @user = User.create!( email: @email, password: SecureRandom.hex(8), username: SecureRandom.hex(8) )
          @social_identity = SocialIdentity.create( :provider => 'facebook', :uid => @uid, :user => @user)

          @provider_username = "fb-username-#{SecureRandom.hex(4)}"
          @facebook_token = SecureRandom.hex(16)
          @facebook_params = {
            "id" => @uid,
            "username" => @provider_username,
            "name" => 'Not Real',
            "email" => @email
          }
          Social::FacebookFacade.should_receive(:get_identity_data).with(@facebook_token).and_return(@facebook_params)
        end
        it "should return a 401" do
          post "/api/login/facebook", { :facebook_token => @facebook_token, :token => @token_for_another_user.token }, @developer_header
          response.status.should == 401
        end
      end
      context 'When token matches the user' do
        before do
          @old_email = "#{SecureRandom.hex(8)}@example.org"
          @current_user = User.create!( email: @old_email, password: SecureRandom.hex(8), username: SecureRandom.hex(8), confirmed_at: Time.now )
          @token = AuthenticationToken.create(:user => @current_user, expires_at: (Clock.now + 1.days))

          @uid = "test-fb-uid-#{SecureRandom.hex(4)}"
          @provider_username = "fb-username-#{SecureRandom.hex(4)}"
          @facebook_token = SecureRandom.hex(16)
          @email = "#{SecureRandom.hex(8)}@example.org"
          @facebook_params = {
            "id" => @uid,
            "username" => @provider_username,
            "name" => 'Not Real',
            "email" => @email
          }
          Social::FacebookFacade.should_receive(:get_identity_data).with(@facebook_token).and_return(@facebook_params)
        end
        it "should return a 200" do
          post "/api/login/facebook", { :facebook_token => @facebook_token, :token => @token.token }, @developer_header
          response.status.should == 200
        end
        it "should update the users email" do
          post "/api/login/facebook", { :facebook_token => @facebook_token, :token => @token.token }, @developer_header
          @current_user.reload.email.should eq(@email)
        end
        it "should create a social identity to the user" do
          lambda {
            post "/api/login/facebook", { :facebook_token => @facebook_token, :token => @token.token }, @developer_header
          }.should change(@current_user.social_identities, :count).by(1)
          expect(@current_user.social_identities.first.avatar_url).to eq("http://graph.facebook.com/#{@uid}/picture")
        end
      end
    end
    context 'when token is not provided' do
      context "when Facebook rejects the OAuth token" do
        before do
          @token = SecureRandom.hex(16)
          Social::FacebookFacade.should_receive(:get_identity_data).with(@token).and_return(nil)
        end

        it "should fail with a 401" do
          post "/api/login/facebook", { :facebook_token => @token }, @developer_header
          response.status.should == 401
        end
      end

      context "when Facebook validates the OAuth token and a user exists with this uid" do
        before do
          @uid = "test-fb-uid-#{SecureRandom.hex(4)}"
          @provider_username = "fb-username-#{SecureRandom.hex(4)}"
          @name = "Not Real"
          @email = "#{SecureRandom.hex(8)}@example.org"
          @facebook_token = SecureRandom.hex(16)
          @facebook_params = {
            "id" => @uid,
            "username" => @provider_username,
            "name" => @name,
            "email" => @email
          }
          @user = User.create!( email: @email, password: SecureRandom.hex(8), username: SecureRandom.hex(8) )
          @social_identity = SocialIdentity.create( :provider => 'facebook', :uid => @uid, :user => @user)
          Social::FacebookFacade.should_receive(:get_identity_data).with(@facebook_token).and_return(@facebook_params)
        end

        it "should succeed and return an AuthenticationToken for the user" do
          post "/api/login/facebook", { :facebook_token => @facebook_token }, @developer_header
          response.should be_ok
          parsed_json = JSON.parse(response.body)
          parsed_json.should be_an_instance_of(Hash)
          parsed_json["token"].should_not be_nil
          api_should_be_a_me_user(parsed_json["me"], @user)
        end

        it "should not create a new user" do
          lambda {
            post "/api/login/facebook", { :facebook_token => @facebook_token }, @developer_header
          }.should_not change(User, :count)
        end

        context "when the request is authenticated as the user with this uid" do
          before do
            @token = AuthenticationToken.create(:user => @user, expires_at: (Clock.now + 1.days))
          end

          it "should update the existing user's Facebook token and not create a new user" do
            lambda {
              post "/api/login/facebook", { :facebook_token => @facebook_token, :token => @token.token }, @developer_header
            }.should_not change(User, :count)
            response.should be_ok
            @social_identity.reload.token.should == @facebook_token
          end
        end
      end
    end

    context "when Facebook validates the OAuth token and a user doesn't exist with this uid" do
      before do
        @uid = "test-fb-uid-#{SecureRandom.hex(4)}"
        @provider_username = "fb-username-#{SecureRandom.hex(4)}"
        @name = "Not Real"
        @email = "#{SecureRandom.hex(8)}@example.org"
        @facebook_token = SecureRandom.hex(16)
        @facebook_params = {
          "id" => @uid,
          "username" => @provider_username,
          "name" => @name,
          "email" => @email
        }
        Social::FacebookFacade.should_receive(:get_identity_data).with(@facebook_token).and_return(@facebook_params)
      end

      it "should succeed, use the FB data to initialize the user and return an AuthenticationToken for the user" do
        post "/api/login/facebook", { :facebook_token => @facebook_token }, @developer_header
        response.should be_ok
        user = User.last
        user.full_name.should == @name
        user.username.should == @provider_username
        user.use_social_identity_avatar.should be_true
        facebook_identity = user.facebook_identities.where(:uid => @uid).first
        facebook_identity.should_not be_nil
        facebook_identity.token.should == @facebook_token
      end

      it "should create a user" do
        lambda {
          post "/api/login/facebook", { :facebook_token => @facebook_token }, @developer_header
        }.should change(User, :count).by(1)
      end

      it "should confirm the created user" do
        post "/api/login/facebook", { :facebook_token => @facebook_token }, @developer_header
        user = User.last
        expect(user.confirmed?).to be_true
      end

  #   it "set the optional location data if present" do
  #     @facebook_params["location"] = { "name" => "Nowhere, UT"}
  #     post "/api/login/facebook", { :facebook_token => @facebook_token }, @developer_header
  #     response.should_not be_ok
  #     user = User.last
  #     user.display_location.should == "Nowhere, UT"
  #     parsed_json = JSON.parse(response.body)
  #     parsed_json.should be_an_instance_of(Hash)
  #     parsed_json["token"].should_not be_nil
  #     api_should_be_a_me_user(parsed_json["me"], user)
  #   end

      context "when the request is authenticated" do
        before do
          @email = "#{SecureRandom.hex(8)}@example.org"
          @current_user = User.create!( email: @email, password: SecureRandom.hex(8), username: SecureRandom.hex(8), confirmed_at: Time.now )
          @token = AuthenticationToken.create(:user => @current_user, expires_at: (Clock.now + 1.days))
        end

        it "should update the existing user's Facebook token and not create a new user" do
          @current_user.the_identity(:facebook).should be_nil
          lambda {
            post "/api/login/facebook", { :facebook_token => @facebook_token, :token => @token.token }, @developer_header
          }.should_not change(User, :count)
          response.should be_ok
          @current_user.reload
          @current_user.the_identity(:facebook).should_not be_nil
          @current_user.the_identity(:facebook).token.should == @facebook_token
          @current_user.the_identity(:facebook).uid.should == @uid
        end
      end
    end

    context "when Facebook validates the OAuth token and a user doesn't exist with this uid, but a user with a matching email does" do
      before do
        @uid = "test-fb-uid-#{SecureRandom.hex(4)}"
        @provider_username = "fb-username-#{SecureRandom.hex(4)}"
        @name = "Not Real"
        @email = "#{SecureRandom.hex(8)}@example.org"
        @token = SecureRandom.hex(16)
        @facebook_params = {
          "id" => @uid,
          "username" => @provider_username,
          "name" => @name,
          "email" => @email
        }
        @user = User.create( email: @email, full_name: "Other Name", username: "alreadyset-#{SecureRandom.hex(4)}", password: SecureRandom.hex(8), confirmed_at: Time.now)
        Social::FacebookFacade.should_receive(:get_identity_data).with(@token).and_return(@facebook_params)
      end

      it "should succeed, match up the user by email and return an AuthenticationToken for the user" do
        post "/api/login/facebook", { :facebook_token => @token }, @developer_header
        response.should be_ok

        parsed_json = JSON.parse(response.body)
        parsed_json.should be_an_instance_of(Hash)
        parsed_json["token"].should_not be_nil
        api_should_be_a_me_user(parsed_json["me"], @user)

        user = User.last
        user.should == @user
        user.full_name.should_not == @name
        user.username.should_not == @provider_username
        facebook_identity = @user.facebook_identities.where(:uid => @uid).first
        facebook_identity.should_not be_nil
        facebook_identity.token.should == @token
      end
    end
  end

  describe 'DELETE facebook' do
    context 'when the client is not authenticated' do
      it 'the response status will be unauthorized' do
        delete '/api/login/facebook', nil, @developer_header
        response.status.should == 401
      end
    end
    context 'when the client is authenticated' do
      before do
        @email = "#{SecureRandom.hex(8)}@example.org"
        @current_user = User.create!( email: @email, password: SecureRandom.hex(8), username: SecureRandom.hex(8), confirmed_at: Time.now )
        @token = AuthenticationToken.create!( :user => @current_user, expires_at: (Clock.now + 1.days))
      end

      context "user without a linked Facebook account" do
        it "returns a 404 response" do
          delete '/api/login/facebook', { :token => @token.token }, @developer_header
          response.status.should == 404
        end
      end

      context "when the user has a linked Facebook account" do
        before do
          @si = SocialIdentity.create!( provider: 'facebook', user: @current_user)
        end

        it "returns a 200 response and deletes the linked account" do
          @current_user.reload.the_identity(:facebook).should_not be_nil
          delete '/api/login/facebook', { :token => @token.token }, @developer_header
          response.status.should == 200
          @current_user.reload.the_identity(:facebook).should be_nil
        end
      end
    end
  end

  describe 'POST google' do
    context 'When token is provided' do
      context 'matches another user' do
        before do
          @another_email = "#{SecureRandom.hex(8)}@example.org"
          @another_user = User.create!( email: @another_email, password: SecureRandom.hex(8), username: SecureRandom.hex(8) )
          @token_for_another_user = AuthenticationToken.create(:user => @another_user, expires_at: (Clock.now + 1.days))

          @uid = "test-fb-uid-#{SecureRandom.hex(4)}"
          @email = "#{SecureRandom.hex(8)}@example.org"
          @user = User.create!( email: @email, password: SecureRandom.hex(8), username: SecureRandom.hex(8) )

          @google_params = {
            "id" => @uid,
            "username" => @provider_username,
            "name" => {"familyName" => @name, "givenName" => @name},
            "emails" => [{'value' => @email}]
          }
          @social_identity = SocialIdentity.create( :provider => 'google_oauth2', :uid => @uid, :user => @user)

          @provider_username = "fb-username-#{SecureRandom.hex(4)}"
          @google_oauth2_token = SecureRandom.hex(16)
          Social::GoogleFacade.should_receive(:get_identity_data).with(@google_oauth2_token).and_return(@google_params)
        end
        it "should return a 401" do
          post "/api/login/google", { :google_oauth2_token => @google_oauth2_token, :token => @token_for_another_user.token }, @developer_header
          response.status.should == 401
        end
      end
      context 'When token matches the user' do
        before do
          @old_email = "#{SecureRandom.hex(8)}@example.org"
          @current_user = User.create!( email: @old_email, password: SecureRandom.hex(8), username: SecureRandom.hex(8) )
          @token = AuthenticationToken.create(:user => @current_user, expires_at: (Clock.now + 1.days))

          @uid = "test-fb-uid-#{SecureRandom.hex(4)}"
          @provider_username = "fb-username-#{SecureRandom.hex(4)}"
          @google_oauth2_token = SecureRandom.hex(16)
          @email = "#{SecureRandom.hex(8)}@example.org"
          @google_params = {
            "id" => @uid,
            "username" => @provider_username,
            "name" => {"familyName" => @name, "givenName" => @name},
            "emails" => [{'value' => @email}]
          }
          Social::GoogleFacade.should_receive(:get_identity_data).with(@google_oauth2_token).and_return(@google_params)
        end
        it "should return a 200" do
          post "/api/login/google", { :google_oauth2_token => @google_oauth2_token, :token => @token.token }, @developer_header
          response.status.should == 200
        end
        it "should update the users email" do
          post "/api/login/google", { :google_oauth2_token => @google_oauth2_token, :token => @token.token }, @developer_header
          @current_user.reload.email.should eq(@email)
        end
        it "should link the social identity to the user" do
          lambda {
            post "/api/login/google", { :google_oauth2_token => @google_oauth2_token, :token => @token.token }, @developer_header
          }.should change(@current_user.social_identities, :count).by(1)
        end
      end
    end
    context 'when token is not provided' do
      context "when Google rejects the OAuth token" do
        before do
          @token = SecureRandom.hex(16)
          Social::GoogleFacade.should_receive(:get_identity_data).with(@token).and_return(nil)
        end

        it "should fail with a 401" do
          post "/api/login/google", { :google_oauth2_token => @token }, @developer_header
          response.status.should == 401
        end
      end

      context "when Google validates the OAuth token and a user exists with this uid" do
        before do
          @uid = "test-gg-uid-#{SecureRandom.hex(4)}"
          @provider_username = "gg-username-#{SecureRandom.hex(4)}"
          @name = "Not Real"
          @email = "#{SecureRandom.hex(8)}@example.org"
          @google_oauth2_token = SecureRandom.hex(16)
          @google_params = {
            "id" => @uid,
            "username" => @provider_username,
            "name" => {"familyName" => @name, "givenName" => @name},
            "emails" => [{'value' => @email}]
          }
          @user = User.create!( email: @email, password: SecureRandom.hex(8), username: SecureRandom.hex(8) )
          @social_identity = SocialIdentity.create( :provider => 'google_oauth2', :uid => @uid, :user => @user)
          Social::GoogleFacade.should_receive(:get_identity_data).with(@google_oauth2_token).and_return(@google_params)
        end

        it "should succeed and return an AuthenticationToken for the user" do
          post "/api/login/google", { :google_oauth2_token => @google_oauth2_token }, @developer_header
          response.should be_ok
          parsed_json = JSON.parse(response.body)
          parsed_json.should be_an_instance_of(Hash)
          parsed_json["token"].should_not be_nil
          api_should_be_a_me_user(parsed_json["me"], @user)
        end

        it "should not create a new user" do
          lambda {
            post "/api/login/google", { :google_oauth2_token => @google_oauth2_token }, @developer_header
          }.should_not change(User, :count)
        end

        context "when the request is authenticated as the user with this uid" do
          before do
            @token = AuthenticationToken.create(:user => @user, expires_at: (Clock.now + 1.days))
          end

          it "should update the existing user's token and not create a new user" do
            lambda {
              post "/api/login/google", { :google_oauth2_token => @google_oauth2_token, :token => @token.token }, @developer_header
            }.should_not change(User, :count)
            response.should be_ok
            @social_identity.reload.token.should == @google_oauth2_token
          end
        end
      end

      context "when Google validates the OAuth token and a user doesn't exist with this uid" do
        before do
          @uid = "test-gg-uid-#{SecureRandom.hex(4)}"
          @provider_username = "gg-username-#{SecureRandom.hex(4)}"
          @name = "Not Real"
          @email = "#{SecureRandom.hex(8)}@example.org"
          @google_oauth2_token = SecureRandom.hex(16)
          @google_params = {
            'id' => @uid,
            'username' => @provider_username,
            'name' => {'familyName' => @name, 'givenName' => @name},
            'emails' => [{'value' => @email}],
            'placesLived'=>[{'value' => 'Nowhere, UT'}]
          }
          Social::GoogleFacade.should_receive(:get_identity_data).with(@google_oauth2_token).and_return(@google_params)
        end

        it "should create a user" do
          lambda {
            post "/api/login/google", { :google_oauth2_token => @google_oauth2_token }, @developer_header
          }.should change(User, :count).by(1)
        end

        it "should confirm the created user" do
          post "/api/login/google", { :google_oauth2_token => @google_oauth2_token }, @developer_header
          user = User.last
          expect(user.confirmed?).to be_true
        end

        it "should succeed, use the Google data to initialize the user and return an AuthenticationToken for the user" do
          post "/api/login/google", { :google_oauth2_token => @google_oauth2_token }, @developer_header
          response.should be_ok

          user = User.last
          user.should_not be_nil
          user.full_name.should == "#{@name} #{@name}"
          user.username.should == @provider_username
          google_identity = user.google_oauth2_identities.where(:uid => @uid).first
          google_identity.should_not be_nil
          google_identity.token.should == @google_oauth2_token
        end

        it "set the optional location data if present" do
          @google_params["location"] = { "name" => "Nowhere, UT"}
          post "/api/login/google", { :google_oauth2_token => @google_oauth2_token }, @developer_header
          response.should be_ok

          user = User.last
          user.should_not be_nil
          user.full_name.should == "#{@name} #{@name}"
          user.username.should == @provider_username
          user.display_location.should == "Nowhere, UT"
          google_identity = user.google_oauth2_identities.where(:uid => @uid).first
          google_identity.should_not be_nil
          google_identity.token.should == @google_oauth2_token

          parsed_json = JSON.parse(response.body)
          parsed_json.should be_an_instance_of(Hash)
          parsed_json["token"].should_not be_nil
          api_should_be_a_me_user(parsed_json["me"], user)
        end

        context "when the request is authenticated" do
          before do
            @email = "#{SecureRandom.hex(8)}@example.org"
            @current_user = User.create!( email: @email, password: SecureRandom.hex(8), username: SecureRandom.hex(8) )
            @token = AuthenticationToken.create(:user => @current_user, expires_at: (Clock.now + 1.days))
          end

          it "should update the existing user's Facebook token and not create a new user" do
            @current_user.the_identity(:google_oauth2).should be_nil
            lambda {
              post "/api/login/google", { :google_oauth2_token => @google_oauth2_token, :token => @token.token }, @developer_header
            }.should_not change(User, :count)
            response.should be_ok
            @current_user.reload
            @current_user.the_identity(:google_oauth2).should_not be_nil
            @current_user.the_identity(:google_oauth2).token.should == @google_oauth2_token
            @current_user.the_identity(:google_oauth2).uid.should == @uid
          end
        end
      end

      context "when Google validates the OAuth token and a user doesn't exist with this uid, but a user with a matching email does" do
        before do
          @uid = "test-gg-uid-#{SecureRandom.hex(4)}"
          @provider_username = "gg-username-#{SecureRandom.hex(4)}"
          @name = "Not Real"
          @email = "#{SecureRandom.hex(8)}@example.org"
          @token = SecureRandom.hex(16)
          @google_params = {
            "id" => @uid,
            "username" => @provider_username,
            "name" => {"familyName" => @name, "givenName" => @name},
            "emails" => [{'value' => @email}]
          }
          @user = User.create( email: @email, full_name: "Other Name", username: "alreadyset-#{SecureRandom.hex(4)}", password: SecureRandom.hex(8))
          Social::GoogleFacade.should_receive(:get_identity_data).with(@token).and_return(@google_params)
        end

        it "should succeed, match up the user by email and return an AuthenticationToken for the user" do
          post "/api/login/google", { google_oauth2_token: @token, email: @user.email }, @developer_header
          response.should be_ok

          parsed_json = JSON.parse(response.body)
          parsed_json.should be_an_instance_of(Hash)
          parsed_json["token"].should_not be_nil
          api_should_be_a_me_user(parsed_json["me"], @user)

          user = User.last
          user.should == @user
          user.full_name.should_not == @name
          user.username.should_not == @provider_username
          google_identity = @user.google_oauth2_identities.where(:uid => @uid).first
          google_identity.should_not be_nil
          google_identity.token.should == @token
        end
      end
    end
  end

  describe 'DELETE google' do
    context 'when the client is not authenticated' do
      it 'the response status will be unauthorized' do
        delete '/api/login/google', nil, @developer_header
        response.status.should == 401
      end
    end
    context 'when the client is authenticated' do
      before do
        @email = "#{SecureRandom.hex(8)}@example.org"
        @current_user = User.create!( email: @email, password: SecureRandom.hex(8), username: SecureRandom.hex(8), confirmed_at: Time.now )
        @token = AuthenticationToken.create!( :user => @current_user, expires_at: (Clock.now + 1.days))
      end

      context "user without a linked Google account" do
        it "returns a 404 response" do
          delete '/api/login/google', { :token => @token.token }, @developer_header
          response.status.should == 404
        end
      end

      context "when the user has a linked Google account" do
        before do
          @si = SocialIdentity.create!( provider: 'google_oauth2', user: @current_user)
        end

        it "returns a 200 response and deletes the linked account" do
          @current_user.reload.the_identity(:google_oauth2).should_not be_nil
          delete '/api/login/google', { :token => @token.token }, @developer_header
          response.status.should == 200
          @current_user.reload.the_identity(:google_oauth2).should be_nil
        end
      end
    end
  end

  describe 'POST twitter' do
    context 'When token is provided' do
      context 'matches another user' do
        before do
          @token = SecureRandom.hex(49)
          @secret = SecureRandom.hex(41)

          @uid = "test-fb-uid-#{SecureRandom.hex(4)}"
          @provider_username = @uid
          @name = "Not Real"
          @email = "#{SecureRandom.hex(8)}@example.org"
          @params = {
            screen_name: @uid,
            time_zone: { name: @name}
          }
          @user = User.create!( email: @email, password: SecureRandom.hex(8), username: SecureRandom.hex(8) )
          @social_identity = SocialIdentity.create( :provider => 'twitter', :uid => @uid, :user => @user)
          Social::TwitterFacade.should_receive(:get_identity_data).with(@token, @secret).and_return(@params)

          @another_email = "#{SecureRandom.hex(8)}@example.org"
          @another_user = User.create!( email: @another_email, password: SecureRandom.hex(8), username: SecureRandom.hex(8) )
          @token_for_another_user = AuthenticationToken.create(:user => @another_user, expires_at: (Clock.now + 1.days))
        end
        it "should return a 401" do
          post "/api/login/twitter", { twitter_token: @token, twitter_secret: @secret, email: 'nm@nm.com', :token => @token_for_another_user.token }, @developer_header
          response.status.should == 401
        end
      end
      context 'When token matches the user' do
        before do
          @token = SecureRandom.hex(49)
          @secret = SecureRandom.hex(41)

          @uid = "test-fb-uid-#{SecureRandom.hex(4)}"
          @provider_username = @uid
          @name = "Not Real"
          @email = "#{SecureRandom.hex(8)}@example.org"
          @params = {
            screen_name: @uid,
            time_zone: { name: @name}
          }
          @new_email = 'nm@email.com'
          @user = User.create!( email: @email, password: SecureRandom.hex(8), username: SecureRandom.hex(8) )
          Social::TwitterFacade.should_receive(:get_identity_data).with(@token, @secret).and_return(@params)
          @api_token = AuthenticationToken.create(:user => @user, expires_at: (Clock.now + 1.days))
        end
        it "should return a 200" do
          post "/api/login/twitter", { twitter_token: @token, twitter_secret: @secret, email: @new_email, :token => @api_token.token }, @developer_header
          response.status.should == 200
        end
        it "should update the users email" do
          post "/api/login/twitter", { twitter_token: @token, twitter_secret: @secret, email: @new_email, :token => @api_token.token }, @developer_header
          @user.reload.email.should eq(@new_email)
        end
        it "should link the social identity to the user" do
          lambda {
            post "/api/login/twitter", { twitter_token: @token, twitter_secret: @secret, email: @new_email, :token => @api_token.token }, @developer_header
          }.should change(@user.social_identities, :count).by(1)
        end
      end
    end
    context 'when token is not provided' do
      before do
        @token = SecureRandom.hex(49)
        @secret = SecureRandom.hex(41)
      end
      context "when twitter rejects the OAuth token" do
        before do
          Social::TwitterFacade.should_receive(:get_identity_data).with(@token, @secret).and_return({})
        end
        it "should fail with a 401" do
          post "/api/login/twitter", { twitter_token: @token, twitter_secret: @secret, email: 'nm@nm.com' }, @developer_header
          response.status.should == 401
        end
      end

      context "when validates the OAuth token and a user exists with this uid" do
        before do
          @uid = "test-fb-uid-#{SecureRandom.hex(4)}"
          @provider_username = @uid
          @name = "Not Real"
          @email = "#{SecureRandom.hex(8)}@example.org"
          @params = {
            screen_name: @uid,
            time_zone: { name: @name}
          }
          @user = User.create!( email: @email, password: SecureRandom.hex(8), username: SecureRandom.hex(8) )
          @social_identity = SocialIdentity.create( :provider => 'twitter', :uid => @uid, :user => @user)
          Social::TwitterFacade.should_receive(:get_identity_data).with(@token, @secret).and_return(@params)
        end

        it "should succeed and return an AuthenticationToken for the user" do
          post "/api/login/twitter", { twitter_token: @token, twitter_secret: @secret, email: 'nm@nm.com' }, @developer_header
          response.should be_ok
          parsed_json = JSON.parse(response.body)
          parsed_json.should be_an_instance_of(Hash)
          parsed_json["token"].should_not be_nil
          api_should_be_a_me_user(parsed_json["me"], @user)
        end

        it "should not create a new user" do
          lambda {
            post "/api/login/twitter", { twitter_token: @token, twitter_secret: @secret, email: 'nm@nm.com' }, @developer_header
          }.should_not change(User, :count)
        end
          #This doesn't apply to twitter because it has token and secret bound together.
        # context "when the request is authenticated as the user with this uid" do
        #   before do
        #     @api_token = AuthenticationToken.create(:user => @user, expires_at: (Clock.now + 1.days))
        #   end
        #   it "should update the existing user's token and not create a new user" do
        #     lambda {
        #       post "/api/login/twitter", { twitter_token: @token, twitter_secret: @secret, email: 'nm@nm.com', token: @api_token }
        #     }.should_not change(User, :count)
        #     response.should be_ok
        #     @social_identity.reload.token.should == @token
        #   end
        # end
      end

      context "when validates the OAuth token and a user doesn't exist with this uid" do
        before do
          @uid = "test-fb-uid-#{SecureRandom.hex(4)}"
          @provider_username = @uid
          @name = "Not Real"
          @email = "#{SecureRandom.hex(8)}@example.org"
          @params = {
            screen_name: @uid,
            time_zone: { name: @name}
          }
          Social::TwitterFacade.should_receive(:get_identity_data).with(@token, @secret).and_return(@params)
        end

        it "should create a user" do
          lambda {
            post "/api/login/twitter", { twitter_token: @token, twitter_secret: @secret, email: @email }, @developer_header
          }.should change(User, :count).by(1)
        end

        it "must not confirm the created user" do
          post "/api/login/twitter", { twitter_token: @token, twitter_secret: @secret, email: @email }, @developer_header
          user = User.last
          expect(user.confirmed?).to be_true
        end

        it 'should send a confirmation email' do
          post "/api/login/twitter", { twitter_token: @token, twitter_secret: @secret, email: @email }, @developer_header
          # expect(ActionMailer::Base.deliveries.last.to.first).to eq(@email)
        end

        after(:each) do
          ActionMailer::Base.deliveries = []
        end
      end

      context "when validates the OAuth token and a user doesn't exist with this uid, but a user with a matching email does" do
        before do
          @uid = "test-fb-uid-#{SecureRandom.hex(4)}"
          @provider_username = @uid
          @name = "Not Real"
          @email = "#{SecureRandom.hex(8)}@example.org"
          @params = {
            screen_name: @uid,
            time_zone: { name: @name}
          }
          @user = User.create!( email: @email, password: SecureRandom.hex(8), username: SecureRandom.hex(8) )
          Social::TwitterFacade.should_receive(:get_identity_data).with(@token, @secret).and_return(@params)
        end

        it "should succeed, match up the user by email and return an AuthenticationToken for the user" do
          post "/api/login/twitter", { twitter_token: @token, twitter_secret: @secret, email: @email }, @developer_header
          response.should be_ok

          parsed_json = JSON.parse(response.body)
          parsed_json.should be_an_instance_of(Hash)
          parsed_json["token"].should_not be_nil
          api_should_be_a_me_user(parsed_json["me"], @user)

          user = User.last
          user.should == @user
          user.full_name.should_not == @name
          user.username.should_not == @provider_username
          identity = @user.twitter_identities.where(:uid => @uid).first
          identity.should_not be_nil
          identity.token.should == @token
        end
      end
    end
  end

  describe 'DELETE twitter' do
    context 'when the client is not authenticated' do
      it 'the response status will be unauthorized' do
        delete '/api/login/twitter', { :token => 'non existent' }, @developer_header
        response.status.should == 401
      end
    end
    context 'when the client is authenticated' do
      before do
        @email = "#{SecureRandom.hex(8)}@example.org"
        @current_user = User.create!( email: @email, password: SecureRandom.hex(8), username: SecureRandom.hex(8), confirmed_at: Time.now )
        @token = AuthenticationToken.create!( :user => @current_user, expires_at: (Clock.now + 1.days))
      end

      context "user without a linked account" do
        it "returns a 404 response" do
          delete '/api/login/facebook', { :token => @token.token }, @developer_header
          response.status.should == 404
        end
      end

      context "when the user has a linked account" do
        before do
          @si = SocialIdentity.create!( provider: 'twitter', user: @current_user)
        end

        it "returns a 200 response and deletes the linked account" do
          @current_user.reload.the_identity(:twitter).should_not be_nil
          delete '/api/login/twitter', { :token => @token.token }, @developer_header
          response.status.should == 200
          @current_user.reload.the_identity(:twitter).should be_nil
        end
      end
    end
  end
end
