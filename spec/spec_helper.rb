# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV["RAILS_ENV"] ||= 'test'
if ENV["RAILS_ENV"] == 'test' 
  unless ENV["SKIP_COV"]
    require 'simplecov'
    if ENV['CIRCLE_ARTIFACTS']
      dir = File.join("..", "..", "..", ENV['CIRCLE_ARTIFACTS'], "coverage")
      SimpleCov.coverage_dir(dir)
    end
    SimpleCov.start 'rails'
  end
end
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'factory_girl'
require 'capybara/rspec'
require 'capybara-screenshot/rspec'
require 'email_spec'
require 'rspec/autorun'
require 'shoulda/matchers'

def test_image_path(image_file_name)
  "#{Rails.root}/spec/fixtures/images/#{image_file_name}"
end

# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
Dir[Rails.root.join("spec/support/**/*.rb")].each {|f| require f}

RSpec.configure do |config|
  config.include FactoryGirl::Syntax::Methods
  config.include Devise::TestHelpers, :type => :controller
  config.include EmailSpec::Helpers
  config.include EmailSpec::Matchers

  # Use color in STDOUT
  config.color_enabled = true
  # Use color not only in STDOUT but also in pagers and files
  config.tty = true

  Capybara.javascript_driver = :webkit
  # ## Mock Framework
  #
  # If you prefer to use mocha, flexmock or RR, uncomment the appropriate line:
  #
  # config.mock_with :mocha
  # config.mock_with :flexmock
  # config.mock_with :rr

  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
  config.fixture_path = "#{::Rails.root}/spec/fixtures"

  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = false

  # If true, the base class of anonymous controllers will be inferred
  # automatically. This will be the default behavior in future versions of
  # rspec-rails.
  config.infer_base_class_for_anonymous_controllers = false

  # Run specs in random order to surface order dependencies. If you find an
  # order dependency and want to debug it, you can fix the order by providing
  # the seed, which is printed after each run.
  #     --seed 1234
  config.order = "random"
  
  config.before(:all) do
    DatabaseCleaner.strategy = :truncation
    DatabaseCleaner.orm = :active_record
    DatabaseCleaner.clean
  end
  
  config.before(:suite) do
    DatabaseCleaner.strategy = :truncation
  end
  config.before(:each) do
    DatabaseCleaner.start
  end
  config.after(:each) do
    DatabaseCleaner.clean
  end
end
