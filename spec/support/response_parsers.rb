def api_should_be_a_simple_user(parsed_json, user = nil, current_user = nil)
  parsed_json.should be_an_instance_of(Hash)
  parsed_json["id"].should_not be_nil
  if user.present?
    parsed_json["id"].should == user.guid
  end
end

def api_should_be_a_detailed_user(parsed_json, user = nil, current_user = nil)
  api_should_be_a_simple_user(parsed_json, user, current_user)
end

def api_should_be_a_me_user(parsed_json, user = nil)
  parsed_json.should be_an_instance_of(Hash)
  parsed_json["id"].should_not be_nil
  if user.present?
    parsed_json["id"].should == user.guid
  end
end